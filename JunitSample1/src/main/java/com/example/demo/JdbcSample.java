package com.example.demo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JdbcSample {

	@Autowired
	private DbCommon dbCommon;

	public void jdbcAccess() {

		// コネクション取得
		Connection conn = dbCommon.getConn();

		try {

			// SELECT文の実行
			String sql = " SELECT reserved_id, user_id, menu_id, starttime, endtime, del_flg FROM reserved_tbl ;";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rset = ps.executeQuery();
			

			// SELECT結果の受け取り
			while (rset.next()) {
				System.out.println("----------------");
				System.out.println(rset.getString("id"));
				System.out.println(rset.getString("name"));
			}

			// INSERT文の実行
//            sql = "INSERT INTO testtable (id, name) VALUES (2, 'ABC')";
//            ps = conn.prepareStatement(sql);
//            ps.executeUpdate();
//            conn.commit();
		} catch (SQLException se) {
			se.printStackTrace();
		}
	}

}
