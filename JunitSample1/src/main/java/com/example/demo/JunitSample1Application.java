package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@PropertySource("classpath:dbinfo.properties")
@SpringBootApplication
public class JunitSample1Application {

	public static void main(String[] args) {
		SpringApplication.run(JunitSample1Application.class, args);
	}

}
