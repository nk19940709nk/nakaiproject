package com.example.demo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.example.demo.model.TblUser;

@Mapper
public interface TestMapper {
    List<TblUser> selectTest();
    
    void insertTest(TblUser tblUser);
    
    void updateTest(TblUser tblUser);
}
