package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SampleController {
    
    @GetMapping(value="/")
    public String index(Model model) {
        return "index";
    }
    
    /**
     * 
     * @return
     */
    @PostMapping("/nullPointerException")
    public String nullPointerExceptionCtl(@RequestParam(required = false) String name){
        throw new NullPointerException("NullPointerException:" + name);
    }
}
