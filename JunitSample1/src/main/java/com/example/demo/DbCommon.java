package com.example.demo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * DBコモンクラス
 */
@Component
public class DbCommon {

    @Autowired
    Environment env;

    /**
     * 機能概要 コネクション取得用メソッド
     * 
     * @return コネクション
     */
    public Connection getConn() {
        Connection conn;
        // プロパティファイルからDB情報を取得
        String url = env.getProperty("db.url");
        String password = env.getProperty("db.password");
        String user = env.getProperty("db.user");

        try {
            // コネクション生成
            conn = DriverManager.getConnection(url, user, password);
            // 自動コミットOFF
            conn.setAutoCommit(false);
        } catch (SQLException se) {
            conn = null;
            se.printStackTrace();
        }
        return conn;
    }
}
