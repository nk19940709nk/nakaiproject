package com.example.demo.model;

import lombok.Data;

@Data
public class TblUser {

    private Integer userId;

    private String name;
    
    private Long testId;
}
