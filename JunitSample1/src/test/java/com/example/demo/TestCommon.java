package com.example.demo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

/**
 * JUnitコモンクラス
 *
 */
@SpringJUnitConfig
@SpringBootTest
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class })
public class TestCommon {

    @Autowired
    private JdbcTemplate jdbctemplate;

    /**
     * テーブルデータ削除メソッド
     * 
     * @param tableName テーブル名
     * @return クエリ実行結果
     */
    public Integer delete(String tableName) {
        String deleteSql = "DELETE FROM " + tableName;
        return jdbctemplate.update(deleteSql);
    }

    /**
     * 指定テーブルの全レコードを取得
     * 
     * @param tableName テーブル名
     * @return クエリ結果
     */
    public List<Map<String, Object>> selectAll(String tableName) {
        String selectSql = "SELECT * FROM " + tableName;
        return jdbctemplate.queryForList(selectSql);
    }

    /**
     * 指定テーブルの指定カラムの全レコードを取得
     * 
     * @param tableName テーブル名
     * @param columns   カラム
     * @return クエリ結果
     */
    public List<Map<String, Object>> selectId(String tableName, String[] columns) {
        String selectSql = "SELECT " + String.join(",", columns) + " FROM " + tableName;
        return jdbctemplate.queryForList(selectSql);
    }

    /**
     * データベース状態をCSV形式で出力
     * 
     * @param tableNames テーブル名一覧
     */
    public void outputCsv(String[] header, List<Map<String, Object>> recordList, String fileName) {

        // 出力ファイルパス
        String filePath = "c:/test/" + fileName + ".csv";
        File file = new File(filePath);
        try (PrintWriter writer = new PrintWriter(file)) {
            // ヘッダ出力
            writer.println(Arrays.stream(header).collect(Collectors.joining(",")));
            // データ出力
            for (Map<String, Object> item : recordList) {
                writer.println(
                        Arrays.stream(header).map(m -> Optional.ofNullable(String.valueOf(item.get(m))).orElse(""))
                                .collect(Collectors.joining(",")));
            }

            // 保存
            writer.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
