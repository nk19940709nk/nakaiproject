package com.example.demo;

import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;

import com.github.springtestdbunit.annotation.DatabaseSetup;

/**
 * JUnitテストクラス
 *
 */
public class JdbcTest extends TestCommon {

	@Autowired
	private JdbcSample jdbcTest;

	/** テストクラス実行時の最初に実行される */
	@BeforeAll
	static void beforeAll() {
		System.out.println("-------------#BeforeAll");
	}

	/** 各テストメソッド実行前に実行される */
	@BeforeEach
	void beforeEach() {
		System.out.println("-------------#BeforeEach");
	}

	/** 各テストメソッド実行後に実行される */
	@AfterEach
	void afterEach() {
		System.out.println("-------------#AferEach");
	}

	/** テストクラス実行後に実行される */
	@AfterAll
	static void afterAll() {
		System.out.println("-------------#AfterAll");
	}

	/**
	 * xmlファイルを読み込んでデータベースに登録するテスト
	 */
	@Test
	@DisplayName("TestCase1")
	@DatabaseSetup("testsample1_1.xml")
	public void testMethod1() {
		jdbcTest.jdbcAccess();
	}

	/**
	 * xlsファイルを読み込んでデータベースに登録するテスト
	 */
	@Test
	@DisplayName("TestCase1")
	@DatabaseSetup("./testdata/testsample1_1.xml")
	public void testMethod2() {
		jdbcTest.jdbcAccess();
	}

	/**
	 * CSV形式でデータを設定するテスト
	 * 
	 * @param str1 引数１
	 * @param str2 引数２
	 * @param i    引数３
	 */
	@ParameterizedTest
	@CsvSource({ "foo,bar,1", "'hoge,fuga','',2", "fizz,,3" })
	public void testMethod3(String str1, String str2, int i) {
		System.out.println("str1:" + str1);
		System.out.println("str2:" + str2);
		System.out.println("int:" + i);
	}

	/**
	 * CSVファイルの読み込みテスト
	 * 
	 * @param str1
	 * @param str2
	 * @param i
	 */
	@ParameterizedTest
	@CsvFileSource(resources = "./testdata/testsample2.csv")
	public void testMehod3(String str1, String str2, int i) {
		System.out.println("str1:" + str1);
		System.out.println("str2:" + str2);
		System.out.println("int:" + i);
	}

	/**
	 * SELECTクエリテスト
	 */
	@Test
	public void testMethod4() {
		// テーブルの全レコードを取得
		List<Map<String, Object>> testList = selectAll("testtable");
		for (Map<String, Object> map : testList) {
			System.out.println(map);
		}
		// テーブルのカラムを指定して全レコード取得
		String[] columns = { "id" };
		List<Map<String, Object>> testList2 = selectId("testtable", columns);
		for (Map<String, Object> map2 : testList2) {
			System.out.println(map2.get("id"));
		}
	}

	@Test
	@DatabaseSetup("./testdata/testsample1_1.xml")
	public void outputCsvTest() {
		String[] header = { "id", "name" };
		outputCsv(header, selectAll("testtable"), "testcase1_1");
	}

}
