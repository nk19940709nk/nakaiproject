package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.mapper.TestMapper;
import com.example.demo.model.TblUser;

@SpringBootTest
public class MapperTest {
    @Autowired
    TestMapper TestMapper;
    
    @Test
    public void testCase1() {
        var test = TestMapper.selectTest();
        System.out.println(test);
    }
    
    @Test
    public void testCase2() {
        var test = new TblUser();
        test.setUserId(3);
        test.setName("testName3");
        test.setTestId(3L);
        TestMapper.insertTest(test);
    }
    
    @Test
    public void testCase3() {
        var test = new TblUser();
        String testStr = "3";
        test.setUserId(2);
        test.setName("testName5");
        test.setTestId(Long.parseLong(testStr));
        TestMapper.updateTest(test);
    }

}
