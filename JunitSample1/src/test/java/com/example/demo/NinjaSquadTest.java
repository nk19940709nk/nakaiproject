package com.example.demo;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.PropertySource;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.Operations;
import com.ninja_squad.dbsetup.destination.Destination;
import com.ninja_squad.dbsetup.destination.DriverManagerDestination;
import com.ninja_squad.dbsetup.operation.Operation;

@SpringBootTest
@PropertySource(value = "classpath:application_test.properties", encoding = "UTF-8")
public class NinjaSquadTest {

    @Value("${spring.datasource.url}")
    public String url;

    @Value("${spring.datasource.username}")
    public String user;

    @Value("${spring.datasource.password}")
    public String password;

    public static final Operation DELETE_ALL = Operations.deleteAllFrom("tbl_user");
    public static final Operation INSERT =
            // テーブルの指定
            Operations.insertInto("tbl_user")
                    // 全てのレコードに同じ値をセットする場合
                    .withDefaultValue("testid", 1)
                    // 列の定義
                    .columns("userid", "name")
                    // 1つ目のレコード
                    .values(111, "わかばイカT")
                    // Operationオブジェクトを作成
                    .build();

    @Test
    public void ninjaTest() {
        Destination dest = new DriverManagerDestination(url, user, password);
        Operation ops = Operations.sequenceOf(DELETE_ALL, INSERT);
        DbSetup dbSetup = new DbSetup(dest, ops);
        dbSetup.launch();
    }

    private static Pattern patternDomainName;
    private Matcher matcher;
    private static final String DOMAIN__NAME__PATTERN = "([a-zA-Z0-9]([a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])?\\.)+[a-zA-Z]{2,6}";
    static {
        patternDomainName = Pattern.compile(DOMAIN__NAME__PATTERN);
    }

    @Test
    public void test1() {
        try {
            Elements elements = getOgp("https://haveagood.holiday/areas/55/spots/attraction");
            for (Element element : elements) {
                if (element.toString().contains("http")) {
                    System.out.println(element.attr("content"));
                }
            }
        } catch (IOException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }
    }

    @Test
    public void test2() {

        Set<String> result = getDataFromGoogle("福岡+遊び");
        for (String temp : result) {
            System.out.println(temp);
        }
    }

    public String getDomainName(String url) {

        String newUrl = url.replace("/url?q=", "");
        return newUrl.split("&")[0];

    }

    private Set<String> getDataFromGoogle(String query) {

        Set<String> result = new HashSet<String>();
        String request = "https://www.google.com/search?q=" + query + "&num=20";
        System.out.println("Sending request..." + request);

        try {

            // need http protocol, set this as a Google bot agent :)
            Document doc = Jsoup.connect(request)
                    .userAgent("Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)").timeout(5000)
                    .get();

            // get all links
            Elements links = doc.select("a[href]");
            for (Element link : links) {

                String temp = link.attr("href");
                if (temp.startsWith("/url?q=")) {
                    result.add(getDomainName(temp));
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public Elements getOgp(String url) throws IOException {
        Document document = Jsoup.connect(url).get();
        return document.select("meta[property~=og:image]");
    }
}
