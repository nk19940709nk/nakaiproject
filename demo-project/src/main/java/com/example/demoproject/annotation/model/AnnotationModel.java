package com.example.demoproject.annotation.model;

import com.example.demoproject.annotation.customAnnotation.Hiragana;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class AnnotationModel {

    @Hiragana
    private String name;
    @NotBlank
    private String age;
}
