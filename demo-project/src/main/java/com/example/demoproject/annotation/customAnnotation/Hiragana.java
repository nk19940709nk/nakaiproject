package com.example.demoproject.annotation.customAnnotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.example.demoproject.annotation.validator.HiraganaValidator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Documented
@Constraint(validatedBy = HiraganaValidator.class)
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Hiragana {
    String message() default "{validation.Hiragana.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
