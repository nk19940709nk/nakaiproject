package com.example.demoproject.annotation.validator;

import org.apache.commons.lang3.StringUtils;

import com.example.demoproject.annotation.customAnnotation.Hiragana;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class HiraganaValidator implements ConstraintValidator<Hiragana, String> {
    @Override
    public void initialize(Hiragana annotation) {
    }

    @Override
    public boolean isValid(String name, ConstraintValidatorContext context) {
        if (StringUtils.isEmpty(name)) {
            return true;
        }
        String hiraganaRegex = "^[ぁ-ゞ]+$";

        return name.matches(hiraganaRegex);
    }
}
