package com.example.demoproject.annotation.controller;

import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demoproject.annotation.model.AnnotationModel;

@RestController
public class AnnotationController {

    @PostMapping(path = "/fieldAnnotation", consumes = "application/json")
    public String fieldTest(@RequestBody @Validated AnnotationModel model, BindingResult result) {
        if (result.hasErrors()) {
            System.out.println("ERROR検知:" + result);
        }
        return "model";
    }

    @GetMapping("/test")
    public void test() {
        System.out.println("test");
    }
}
