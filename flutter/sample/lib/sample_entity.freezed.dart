// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'sample_entity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

SampleEntity _$SampleEntityFromJson(Map<String, dynamic> json) {
  return _SampleEntity.fromJson(json);
}

/// @nodoc
class _$SampleEntityTearOff {
  const _$SampleEntityTearOff();

  _SampleEntity call(String genre) {
    return _SampleEntity(
      genre,
    );
  }

  SampleEntity fromJson(Map<String, Object?> json) {
    return SampleEntity.fromJson(json);
  }
}

/// @nodoc
const $SampleEntity = _$SampleEntityTearOff();

/// @nodoc
mixin _$SampleEntity {
  String get genre => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $SampleEntityCopyWith<SampleEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SampleEntityCopyWith<$Res> {
  factory $SampleEntityCopyWith(
          SampleEntity value, $Res Function(SampleEntity) then) =
      _$SampleEntityCopyWithImpl<$Res>;
  $Res call({String genre});
}

/// @nodoc
class _$SampleEntityCopyWithImpl<$Res> implements $SampleEntityCopyWith<$Res> {
  _$SampleEntityCopyWithImpl(this._value, this._then);

  final SampleEntity _value;
  // ignore: unused_field
  final $Res Function(SampleEntity) _then;

  @override
  $Res call({
    Object? genre = freezed,
  }) {
    return _then(_value.copyWith(
      genre: genre == freezed
          ? _value.genre
          : genre // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class _$SampleEntityCopyWith<$Res>
    implements $SampleEntityCopyWith<$Res> {
  factory _$SampleEntityCopyWith(
          _SampleEntity value, $Res Function(_SampleEntity) then) =
      __$SampleEntityCopyWithImpl<$Res>;
  @override
  $Res call({String genre});
}

/// @nodoc
class __$SampleEntityCopyWithImpl<$Res> extends _$SampleEntityCopyWithImpl<$Res>
    implements _$SampleEntityCopyWith<$Res> {
  __$SampleEntityCopyWithImpl(
      _SampleEntity _value, $Res Function(_SampleEntity) _then)
      : super(_value, (v) => _then(v as _SampleEntity));

  @override
  _SampleEntity get _value => super._value as _SampleEntity;

  @override
  $Res call({
    Object? genre = freezed,
  }) {
    return _then(_SampleEntity(
      genre == freezed
          ? _value.genre
          : genre // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_SampleEntity with DiagnosticableTreeMixin implements _SampleEntity {
  const _$_SampleEntity(this.genre);

  factory _$_SampleEntity.fromJson(Map<String, dynamic> json) =>
      _$$_SampleEntityFromJson(json);

  @override
  final String genre;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'SampleEntity(genre: $genre)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'SampleEntity'))
      ..add(DiagnosticsProperty('genre', genre));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _SampleEntity &&
            const DeepCollectionEquality().equals(other.genre, genre));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(genre));

  @JsonKey(ignore: true)
  @override
  _$SampleEntityCopyWith<_SampleEntity> get copyWith =>
      __$SampleEntityCopyWithImpl<_SampleEntity>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_SampleEntityToJson(this);
  }
}

abstract class _SampleEntity implements SampleEntity {
  const factory _SampleEntity(String genre) = _$_SampleEntity;

  factory _SampleEntity.fromJson(Map<String, dynamic> json) =
      _$_SampleEntity.fromJson;

  @override
  String get genre;
  @override
  @JsonKey(ignore: true)
  _$SampleEntityCopyWith<_SampleEntity> get copyWith =>
      throw _privateConstructorUsedError;
}
