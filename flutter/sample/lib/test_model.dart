import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

@immutable
class TestModel {
  String? title;
  DateTime? createdAt;

  TestModel({required String title, required DateTime createdAt }) {
    this.title = title;
    this.createdAt = createdAt;
  }

  //Firebaseからデータを取得する際の変換処理
  static TestModel fromJson2(Map<String, dynamic> json) {
    return TestModel(
        title: json['title']! as String,
        createdAt: (json['createdAt']! as Timestamp).toDate() as DateTime
    );
  }
  TestModel.fromJson(Map<String, Object?> json) : this(
      title: json['title']! as String,
      createdAt: (json['createdAt']! as Timestamp).toDate() as DateTime);

  //DartのオブジェクトからFirebaseへ渡す際の変換処理
  Map<String, Object?> toJson() {
    Timestamp? deletedTimestamp;
    return {
      'title': title,
      'createdAt': Timestamp.fromDate(createdAt!), //DartのDateTimeからFirebaseのTimestampへ変換
    };
  }
}