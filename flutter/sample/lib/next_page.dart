import 'package:flutter/material.dart';

class NextPage extends StatelessWidget {
  NextPage(this.name);
  final String name;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('NextPage'),
      ),
      body: Container(
        height:double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            //Text(name),
            Icon(
              Icons.directions_bike,
              size: 200,
            ),
            Center(
              child: RaisedButton(
                child: Icon(
                  Icons.arrow_back_ios,
                ),
                onPressed: () {
                  Navigator.pop(context, 'ill be back!');
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}