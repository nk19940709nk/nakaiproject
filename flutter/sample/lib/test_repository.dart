
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:sample/test_model.dart';

class TestRepository {
  final schoolsManager = FirebaseFirestore.instance.collection('test');

  // 情報取得
  Future<List<QueryDocumentSnapshot<TestModel>>> getTests() async {
    final testRef = schoolsManager.withConverter<TestModel>(
        fromFirestore: (snapshot, _) => TestModel.fromJson2(snapshot.data()!),
        toFirestore: (testmodel, _) => testmodel.toJson());
    final snapshot = await testRef.get();
    return snapshot.docs;
  }

  // データ追加
  Future<String> insert(TestModel testmodel) async {
    final data = await schoolsManager.add(testmodel.toJson());
    return data.id;
  }
}