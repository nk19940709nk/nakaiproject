
import 'package:cloud_firestore/cloud_firestore.dart';

class Book{
  Book(DocumentSnapshot e){
    documentId = e.reference.id;
    title = e['title'];
    imageURL = e['imageURL'];
  }
  String? title;
  String? documentId;
  String? imageURL;
  void setTitle(String title) {
    this.title = title;
  }
}