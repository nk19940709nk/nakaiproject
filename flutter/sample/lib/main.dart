import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:sample/main_model.dart';
import 'package:sample/test_model.dart';
import 'package:sample/test_repository.dart';

import 'iPhone8SE9.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(
    const ProviderScope(child: MyApp()),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends ConsumerWidget  {
  @override
  Widget build(BuildContext context, WidgetRef ref) {

    final genres = ref.watch(genresStreamProvider);
    final testRepos = TestRepository();
    String? title;
    DateTime createAt = DateTime.now();
    return Scaffold(

      appBar: AppBar(title: const Text('Counter example')),
      body: Column(
        children: [
          genres.when(
            data: (genres) {
              return SizedBox(
                height: 300,
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                    itemCount: genres.length,
                    itemBuilder: (context, index) {
                      final genre = genres[index];
                      return Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Card(
                          color: index % 2 == 1 ? Colors.blue : Colors.green,
                          child: Container(
                            width: 300,
                            child: RaisedButton(
                              onPressed: () async {
                                final tests = await testRepos.getTests();
                                for (var test in tests) {
                                  print("ドキュメントID:" + test.id.toString());
                                  print("タイトル" + test.data().title!);
                                  print("作成日時:" + test.data().createdAt.toString());
                                }
                              },
                            ),
                          ),
                        ),
                      );
                    }
                ),
              );
            },
            error: (error, stack) => Text('Error: $error'),
            loading: () => const CircularProgressIndicator(),
          ),
          TextField(
            onChanged: (text) => title = text,
          ),
          RaisedButton(
            child: Text('登録'),
            onPressed: () async {
              final insertData = new TestModel(
                  title:title!,
                  createdAt: createAt,
              );
              //ドキュメントIDを取得
              final deletedDocumentId = await testRepos.insert(insertData);
              print("ドキュメントID:" + deletedDocumentId.toString());
            },
          )
        ],
      )
    );
  }
}

