import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sample/domain/book.dart';

class AddBookModel extends ChangeNotifier {
  String bookTitle = '';
  File? imageFile;

  Future selectImagePicker() async{
    final ImagePicker  picker = ImagePicker();
    final pickedFile = await picker.pickImage (source: ImageSource.gallery);
    imageFile = File(pickedFile!.path);
    notifyListeners();
  }

  Future addBookFirebase() async  {
    if(bookTitle.isEmpty) {
      throw('title is Empty');
    }

    final imageURL = await uploadImage();

    FirebaseFirestore.instance.collection('books').add({
      'title': bookTitle,
      'imageURL' : imageURL,
    });
  }

  Future editBookFirebase(Book book) async {
    final document = FirebaseFirestore.instance.collection('books').doc(book.documentId);
    final imageURL = await uploadImage();
    await document.update({
      'title': bookTitle,
      'updateAt':Timestamp.now(),
      'imageURL' : imageURL,
    });
  }

  Future<String> uploadImage() async {
    firebase_storage.TaskSnapshot snapshot = await firebase_storage.FirebaseStorage.instance
        .ref('books/$bookTitle')
        .putFile(imageFile!);
    return snapshot.ref.getDownloadURL();
  }
}