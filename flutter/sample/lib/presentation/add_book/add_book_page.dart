import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sample/domain/book.dart';

import 'add_book_model.dart';


class AddBookPage extends StatelessWidget {
  Book? book;

  AddBookPage({Key? key,this.book}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final bool isUpdate = book != null;
    final textEditController = TextEditingController();
    if(isUpdate) {
      textEditController.text = book!.title!;
    }

    return ChangeNotifierProvider<AddBookModel>(
      create: (_) => AddBookModel(),
      child: Scaffold(
        appBar:AppBar(
          title: Text(isUpdate ? 'EditBook': 'AddBook'),
        ),
        body: Consumer<AddBookModel>(builder: (context, model,child){
          return Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children:[
                SizedBox(
                  width: double.infinity,
                  height: 300,
                  child: InkWell(
                    onTap: () async{
                      // TODO Open the CameraRoll
                      await model.selectImagePicker();
                    },
                    child:model.imageFile != null ?
                    Image.file(model.imageFile!): Container(color: Colors.red),
                  ),
                ),
                TextField(
                  controller: textEditController,
                  onChanged: (text){
                    model.bookTitle = text;
                  },
                ),
                RaisedButton(
                  child: Text(isUpdate ? 'edit!':'add!'),
                  onPressed: () async{
                    // TODO firesotre日本を追加
                    if(isUpdate) {
                      updateBook(model, context);
                    } else{
                      await addBook(model, context);
                    }
                  },
                ),
              ],
            ),
          );
        },),
      ),
    );
  }

  Future addBook(AddBookModel model, BuildContext context) async {
    try {
      await model.addBookFirebase();
      await showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('SuccessAddBook!'),
              actions: <Widget> [
                FlatButton(
                  child: Text('OK'),
                  onPressed: (){
                    // TODO
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          }
      );
      Navigator.pop(context);
    } catch(e) {
      await showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(e.toString()),
              actions: <Widget> [
                FlatButton(
                  child: Text('OK'),
                  onPressed: (){
                    // TODO
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          }
      );
      Navigator.pop(context);
    }
  }
  Future updateBook(AddBookModel model, BuildContext context) async {
    try {
      await model.editBookFirebase(book!);
      await showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('SuccessEdit!'),
              actions: <Widget> [
                FlatButton(
                  child: Text('OK'),
                  onPressed: (){
                    // TODO
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          }
      );
      Navigator.pop(context);
    } catch(e) {
      await showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(e.toString()),
              actions: <Widget> [
                FlatButton(
                  child: Text('OK'),
                  onPressed: (){
                    // TODO
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          }
      );
      Navigator.pop(context);
    }
  }
}
