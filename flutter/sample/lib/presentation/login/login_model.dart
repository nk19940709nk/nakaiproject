import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:sample/domain/book.dart';

class LoginModel extends ChangeNotifier {
  String mail = '';
  String password = '';

  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future login() async  {
    // todo
    if(mail.isEmpty) {
      throw('please input email');
    } else {
      print(mail);
    }
    if(password.isEmpty) {
      throw ('please input password');
    } else {
      print(password);
    }
    final result = await _auth.signInWithEmailAndPassword(
      email: mail,
      password: password,
    );
    print(result.user!.uid);
  }
}