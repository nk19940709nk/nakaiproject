import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sample/domain/book.dart';
import 'package:sample/presentation/add_book/add_book_model.dart';
import 'package:sample/presentation/add_book/add_book_page.dart';

import 'book_list_model.dart';

class BookListPage extends StatelessWidget {
  const BookListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<BookListModel>(
      create: (_) => BookListModel()..fetchBooks(),
      child: Scaffold(
        appBar:
        AppBar(
          title: const Text('BookLists'),
        ),
        body: Consumer<BookListModel>(builder: (context, model,child){
          final List<Book> books = model.bookList;
          final listTiles = books.map(
                (book) => ListTile(
                  leading: Image.network(
                    book.imageURL!,
                    errorBuilder: (c, o, s) {
                      return const Icon(
                        Icons.error,
                        color: Colors.red,
                      );
                    },
                  ),
              title: Text(book.title!),
                  trailing: IconButton(
                    icon: const Icon(Icons.edit),
                    onPressed: () async{
                      // TODO
                      await Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => AddBookPage(book: book),
                          fullscreenDialog: true,
                        ),
                      );
                      model.fetchBooks();
                    },
                  ),
                  onLongPress: () async{
                    await showDialog(
                        context: context,
                        builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text('Do you delete ${book.title} ?'),
                        actions: <Widget> [
                          FlatButton(
                            child: Text('OK'),
                            onPressed: () async{
                              // TODO
                              Navigator.of(context).pop();
                              // 削除メソッドを呼び出し
                              await deleteBook(context, model, book);
                            },
                          ),
                        ],
                      );
                    }
                    );
                  },
            ),
          ).toList();
          return ListView(
            children:listTiles,
          );
        },),
        floatingActionButton: Consumer<BookListModel>(builder: (context, model,child) {
          return FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () async{
              // TODO
              await Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => AddBookPage(),
                  fullscreenDialog: true,
                ),
              );
              model.fetchBooks();
            },
          );
        }
        ),
      ),
    );
  }

  Future deleteBook(BuildContext context, BookListModel model, Book book) async {
    try {
      await model.deleteBookFirebase(book,context);
      // Navigator.pop(context);
      await model.fetchBooks();
    } catch(e) {
      testDialog(context, e.toString());
    }
  }

  Future testDialog(BuildContext context, String title) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            actions: <Widget> [
              FlatButton(
                child: Text('OK'),
                onPressed: (){
                  // TODO
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        }
    );
    // Navigator.pop(context);
  }
}
