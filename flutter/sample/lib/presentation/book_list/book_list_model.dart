
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:sample/domain/book.dart';

class BookListModel extends ChangeNotifier {
  List<Book> bookList = [];

  Future fetchBooks()  async {
    final docs = await FirebaseFirestore.instance
        .collection('books')
        .get();
    final books = docs.docs.map((doc) => Book(doc)).toList();
    this.bookList = books;
    notifyListeners();
  }

  Future deleteBookFirebase(Book book, BuildContext context) async {

    final document = FirebaseFirestore.instance.collection('books').doc(book.documentId);
    await document.delete();
    showDialog(
        context: context,
        builder: (BuildContext context2) {
          return AlertDialog(
            title: Text('SuccessDeleteBook!'),
            actions: <Widget> [
              FlatButton(
                child: Text('OK'),
                onPressed: (){
                  // TODO
                  Navigator.of(context2).pop();
                },
              ),
            ],
          );
        }
    );
  }
}