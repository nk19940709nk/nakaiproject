import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sample/domain/book.dart';
import 'package:sample/presentation/signup/signup_model.dart';


class SignupPage extends StatelessWidget {
  Book? book;

  SignupPage({Key? key,this.book}) : super(key: key);
  @override
  Widget build(BuildContext context) {

    final mailEditController = TextEditingController();
    final passwordEditController = TextEditingController();

    return ChangeNotifierProvider<SignupModel>(
      create: (_) => SignupModel(),
      child: Scaffold(
        appBar:AppBar(
          title: Text('Signup'),
        ),
        body: Consumer<SignupModel>(builder: (context, model, child){
          return Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children:[
                TextFormField(
                  decoration: InputDecoration(
                    hintText: 'mail@xxx.co.jp'
                  ),
                  controller: mailEditController,
                  onChanged: (text){
                    model.mail = text;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(
                    hintText: 'password',
                  ),
                  controller: passwordEditController,
                  onChanged: (text){
                    model.password = text;
                  },
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 24, 0, 0),
                  child: RaisedButton(
                    child: Text('signup!'),
                    onPressed: () async{
                      // TODO firesotre日本を追加
                      try {
                        await model.signup();
                      } catch (e) {
                        print(e.toString());
                        print('Faild!!!!!!!');
                      }
                    },
                  ),
                ),
              ],
            ),
          );
        },),
      ),
    );
  }
}
