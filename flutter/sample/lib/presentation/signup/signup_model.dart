import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:sample/domain/book.dart';

class SignupModel extends ChangeNotifier {
  String mail = '';
  String password = '';

  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future signup() async  {
    // todo
    if(mail.isEmpty) {
      throw('please input email');
    } else {
      print(mail);
    }
    if(password.isEmpty) {
      throw ('please input password');
    } else {
      print(password);
    }
    UserCredential user = await _auth.createUserWithEmailAndPassword(
        email: mail,
        password: password,
    );

    final email = user.user!.email;

    FirebaseFirestore.instance.collection('users').add({
      'email':email,
      'createAt':Timestamp.now(),
    });



  }
}