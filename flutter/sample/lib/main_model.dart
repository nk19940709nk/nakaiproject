

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:sample/sample_entity.dart';

final genresStreamProvider = StreamProvider<List<SampleEntity>>((ref) => MainModel().getStrem());

class MainModel {

  Stream<List<SampleEntity>> getStrem() {
    final collection = FirebaseFirestore.instance.collection('genres');
    // データ（Map型）を取得
    final stream = collection.snapshots().map(
      // CollectionのデータからItemクラスを生成する
          (e) => e.docs.map((e) => SampleEntity.fromJson(e.data())).toList(),
    );
    return stream;
  }
}