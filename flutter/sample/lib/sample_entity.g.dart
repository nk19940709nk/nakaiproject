// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sample_entity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_SampleEntity _$$_SampleEntityFromJson(Map<String, dynamic> json) =>
    _$_SampleEntity(
      json['genre'] as String,
    );

Map<String, dynamic> _$$_SampleEntityToJson(_$_SampleEntity instance) =>
    <String, dynamic>{
      'genre': instance.genre,
    };
