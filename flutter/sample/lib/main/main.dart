import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sample/next_page.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:sample/presentation/book_list/book_list_page.dart';
import 'package:sample/presentation/login/login_page.dart';
import 'package:sample/presentation/signup/signup_page.dart';

import 'main_model.dart';

void main() => runApp(MyApp());



class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Firebase.initializeApp(); // new
    return MaterialApp(
      title: 'FlutterTest',
      home: ChangeNotifierProvider<MainModel> (
        create: (_) => MainModel(),
        child:Scaffold(
          appBar: AppBar(
            title: Text('mainPage'),
          ),
          body: Consumer<MainModel>(builder: (context, model, child) {
            return Center(
              child: Column(
                children:[
                  RaisedButton(
                    child: Text('BookListView'),
                    onPressed:() {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => BookListPage()),
                      );
                    },
                  ),
                  RaisedButton(
                    child: Text('Regist Account'),
                    onPressed:() {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => SignupPage()),
                      );
                    },
                  ),
                  RaisedButton(
                    child: Text('Login'),
                    onPressed:() {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => LoginPage()),
                      );
                    },
                  ),
                ],
              ),
            );
          })
        ),
      ),
    );
  }
}
