import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
// パッケージのインポート
import 'package:flutter/foundation.dart';

// 自動生成されるコードを読み込む
part 'sample_entity.freezed.dart';
part 'sample_entity.g.dart'; // 追加


@freezed
class SampleEntity with _$SampleEntity {
  const factory SampleEntity(String genre) = _SampleEntity;
  factory SampleEntity.fromJson(Map<String, dynamic> json) => _$$_SampleEntityFromJson(json);
}