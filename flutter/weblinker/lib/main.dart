import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'main_model.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<MainModel>(
      create: (_) => MainModel(),
      child:
        Scaffold(
          appBar: AppBar(
            title:
              const Center(
                child: Text('testApp'),
              ),
          ),
          body:
            Consumer(
              builder: (context, model, child) {
                return Container(
                  height: 300,
                  width: double.infinity,
                  padding: const EdgeInsets.all(16.0),
                  child: const ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(20.0)),
                    child: WebView(
                      initialUrl: 'https://flutter.dev',
                    ),
                  ),
                );
              },
            ),
        ),
    );
  }
}