import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/view/add_page.dart';

import 'model/main_model.dart';

void main() async {
  // runAppの前にflutter機能を使用する場合に必要な処理
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Flutter Demo',
      home: MainPage(),
    );
  }
}

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<MainModel>(
      create: (_) =>MainModel()..getTodoListRealtime(),
      child:
      Scaffold(
        // ヘッダー
        appBar: AppBar(
          title: const Center(
            child: Text(
              'TODO Appllication',
            ),
          ),
          actions:[
            Consumer<MainModel>(
              builder: (context, model, child) {
                final isActive = model.checkDelete();
                return FlatButton(
                  onPressed:
                  isActive ? () async {
                    await model.deleteTodo();
                  } : null,
                  child: Text(
                    '完了',
                    style: TextStyle(
                      color: isActive ? Colors.white : Colors.white.withOpacity(0.4),
                    ),
                  ),
                );
                },
            ),
          ],
        ),
        body:
        Consumer<MainModel>(
          builder: (context, model, child) {
            final todoList = model.todoList;
            final listTiles = todoList.map(
                  (todo) =>
                  CheckboxListTile(
                    title: Text(todo.title!),
                    value: todo.delFlg,
                    onChanged: (value){
                      todo.delFlg = !todo.delFlg;
                      model.reload();
                    },
                  ),
            ).toList();

            return ListView(
              children: listTiles,
            );
          },
        ),
        // プラスボタン
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => AddPage(),
                fullscreenDialog: true,
              ),
            );
          },
          tooltip: 'Increment',
          child: Icon(Icons.add),
        ),
      ),
    );
  }
}