
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:todo_app/entity/todoEntity.dart';

class MainModel extends ChangeNotifier {
  List<TodoEntity> todoList = [];

  /**
   * FirebaseのtodoListコレクションからを取得する(非同期処理)
   */
  Future getTodoList() async {
    // FirebaseからtodoListコレクションオブジェクトを取得する
    final snapshot = await FirebaseFirestore.instance.collection('todoList').get();
    final docObject = snapshot.docs;
    final todoList = docObject.map((docObject) => TodoEntity(docObject)).toList();
    this.todoList = todoList;
    notifyListeners();
  }

  /**
   * リアルタイムにFirebaseのtodoListコレクションからを取得する(同期処理)
   */
  void getTodoListRealtime() {
    final snapshots = FirebaseFirestore.instance.collection('todoList').snapshots();
    snapshots.listen((snapshot) {
      final docObject = snapshot.docs;
      final todoList = docObject.map((docObject) => TodoEntity(docObject)).toList();
      todoList.sort((a,b) => a.createDate!.compareTo(b.createDate!));
      this.todoList = todoList;
      notifyListeners();
    });
  }

  /**
   * データ変更通知メソッド
   */
  void reload() {
    notifyListeners();
  }

  /**
   * チェック済みTODOコンテンツを削除する
   */
  Future deleteTodo() async{
    // todoListでtrueのものだけ抽出
    final checkedList = todoList.where((todo) => todo.delFlg).toList();
    // referencesのリストに変換
    final checkedRef = checkedList.map((todo) => todo.docRef).toList();
    final WriteBatch batch = FirebaseFirestore.instance.batch();

    // 対象コンテンツを削除
    for (var ref in checkedRef) {
      batch.delete(ref!);
    }

    // データベースにコミット
    return batch.commit();
  }

  /**
   * 完了ボタン表示・非表示判定
   */
  bool checkDelete() {
    // todoListでtrueのものだけ抽出
    final checkedList = todoList.where((todo) => todo.delFlg).toList();
    return checkedList.isNotEmpty;
  }

  Future removeSingle(uid) async {
    final collection = FirebaseFirestore.instance.collection('todoList');
    await collection.doc(uid).delete();
  }
}