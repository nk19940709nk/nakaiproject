
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_app/model/add_model.dart';

class AddPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<AddModel>(
      create: (_) => AddModel(),
      child:
      Scaffold(
        appBar: AppBar(
          title: const Center(
            child: Text(
              'TODO Appllication',
            ),
          ),
        ),
        body:
        Consumer<AddModel>(
          builder: (context, model, child)  {
            return Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  // テキスト入力欄
                  TextField(
                    decoration:InputDecoration(
                      hintText: '追加することを入力',
                    ),
                    onChanged: (text) {
                      model.todoText = text;
                    },
                  ),
                  // Pdding
                  SizedBox(
                    height: 16.0,
                  ),
                  // ボタン
                  RaisedButton(
                    child: Text('追加'),
                    onPressed: () async{
                      await model.addContent();
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}