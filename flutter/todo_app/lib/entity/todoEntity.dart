
import 'package:cloud_firestore/cloud_firestore.dart';

/**
 * TODOEntityクラス
 */
class TodoEntity {
  TodoEntity(DocumentSnapshot docSnapshot) {
    docRef = docSnapshot.reference;
    title = docSnapshot['title'];
    createDate = docSnapshot['createDate'].toDate();
  }

  String? title;
  DateTime? createDate;
  bool delFlg = false;
  DocumentReference? docRef;
}