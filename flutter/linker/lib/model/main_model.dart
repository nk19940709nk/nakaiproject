import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:linker/entity/weblink.dart';

class MainModel extends ChangeNotifier {

  List<WebLink> linkList = [];

  void readLinksRealtime() {
    final snapshots = FirebaseFirestore.instance.collection('weblinks').snapshots();
    snapshots.listen((snapshot) {
      final docObject = snapshot.docs;
      final weblinks = docObject.map((docObject) => WebLink(docObject)).toList();
      weblinks.sort((a,b) => a.rgtDt!.compareTo(b.rgtDt!));
      this.linkList = weblinks;
      notifyListeners();
    });
  }

  Future readLinks() async {
    final linkDocs = await FirebaseFirestore.instance.collection('weblinks').get();
    this.linkList = linkDocs.docs.map((linkDocs) => WebLink(linkDocs)).toList();
    notifyListeners();
  }

}