
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class LinkAddModel extends ChangeNotifier {
  String? title = '';

  Future addWebLink(String title, String linkUrl) async {
    await FirebaseFirestore.instance.collection('weblinks').add({
      'title': title,
      'linkUrl' : linkUrl,
      'rgtDt': Timestamp.now(),
    });
  }

}