import 'package:flutter/material.dart';
import 'package:linker/entity/weblink.dart';
import 'package:linker/model/link_add_model.dart';
import 'package:provider/provider.dart';

class LinkAddPage extends StatelessWidget {
  LinkAddPage({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {

    String? inputTitle;
    String? inputLinkUrl;

    return ChangeNotifierProvider<LinkAddModel>(
      create: (_) => LinkAddModel(),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(126, 140, 224,1.0),
          title: const Center(
            child: Text('リンク追加ページ'),
          ),
        ),
        body:Consumer<LinkAddModel>(
          builder: (context, model, child) {
            return Column(
              children:[
                TextField(
                  //controller: textEditController,
                  onChanged: (text) {
                    inputTitle = text;
                  },
                  decoration: InputDecoration(
                      hintText: 'タイトル'
                  ),
                ),
                TextField(
                  onChanged: (text) {
                    inputLinkUrl = text;
                  },
                  decoration: InputDecoration(
                      hintText: 'URL'
                  ),
                ),
                RaisedButton(
                  onPressed: () async {
                    // ここに処理を記述
                    await addLink(model, context, inputTitle!, inputLinkUrl!);
                  },
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  Future addLink(LinkAddModel model, BuildContext context, String title, String linkUrl) async {
    try {
      await model.addWebLink(title, linkUrl);
      await showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('SuccessAddLink!'),
              actions: <Widget> [
                FlatButton(
                  child: Text('OK'),
                  onPressed: (){
                    // TODO
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          }
      );
      Navigator.pop(context);
    } catch(e) {
      await showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(e.toString()),
              actions: <Widget> [
                FlatButton(
                  child: Text('OK'),
                  onPressed: (){
                    // TODO
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          }
      );
      Navigator.pop(context);
    }
  }

}