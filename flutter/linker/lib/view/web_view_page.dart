import 'package:flutter/material.dart';
import 'package:linker/entity/weblink.dart';
import 'package:linker/model/main_model.dart';
import 'package:linker/model/web_view_model.dart';
import 'package:provider/provider.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewPage extends StatelessWidget {
  WebLink? webLink;

  WebViewPage({Key? key, this.webLink}) : super(key: key);



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(
          child: Text(
            'ウェブ表示ページ',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
      body : WebView(
        initialUrl: this.webLink!.linkUrl!
      ),
    );
  }
}