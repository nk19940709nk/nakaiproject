import 'package:cloud_firestore/cloud_firestore.dart';

class WebLink {
  String? docId;
  String? linkUrl;
  String? title;
  Timestamp? rgtDt;


  WebLink(DocumentSnapshot snapshot) {
    docId = snapshot.id;
    linkUrl = snapshot['linkUrl'];
    title = snapshot['title'];
    rgtDt = snapshot['rgtDt'];
  }
}