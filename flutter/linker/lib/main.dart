import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:linker/model/main_model.dart';
import 'package:linker/view/link_add_page.dart';
import 'package:linker/view/web_view_page.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(); // new
  runApp(MyApp());

  // webviewページでリンクを削除できるようにする
  // アコーディオン形式にして備考欄を設ける
  // ページング機能をつける
  // 追加しているときにローディング形式にする
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  String? title;
  MyHomePage({Key? key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<MainModel>(
      create: (_) => MainModel()..readLinksRealtime(),
      child: Scaffold(
        appBar: AppBar(
          title: Center(child: Text(this.title!)),
          backgroundColor: Color.fromRGBO(126, 140, 224,1.0),
        ),
        body: Consumer<MainModel>(
          builder: (context, model, child) {
            final weblinkList = model.linkList;
            final listTiles = weblinkList.map(
                  (weblink) => Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Container(
                      height: 80,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black45,
                            spreadRadius: 1.0,
                            blurRadius: 1.0,
                            offset: Offset(1,1),
                          ),
                        ],
                        borderRadius: BorderRadius.circular(16.0),
                      ),
                      child: ListTile(
                title: Text(weblink.title!),
                trailing: IconButton(
                  onPressed: () {

                  },
                  icon: Icon(Icons.delete) ,
                ),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => WebViewPage(webLink: weblink),
                    ),
                  );
                },
              ),
                    ),
                  ),
            ).toList();
            return Container(
              color: Color.fromRGBO(126, 140, 224,1.0),
              child: ListView(
                children: listTiles,
              ),
            );
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => LinkAddPage(),
                fullscreenDialog: true,
              ),
            );
          },
          tooltip: 'increment',
          child: const Icon(Icons.add),
        ),
      ),
    );
  }
}
