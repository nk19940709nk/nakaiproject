package com.example.sirialize.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.sirialize.model.Test;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class TestController {

    ObjectMapper mapper = new ObjectMapper();

    @GetMapping("/test")
    public void test() throws Exception {
        String json = "{\"prop\":\"testvalueA\"}";

        Test test = mapper.readValue(json, Test.class);

        System.out.println(test.getProp());
    }
}