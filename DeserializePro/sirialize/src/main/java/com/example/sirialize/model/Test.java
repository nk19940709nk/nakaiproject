package com.example.sirialize.model;

import java.util.Arrays;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Test {

    private Prop prop;

    @RequiredArgsConstructor
    public enum Prop {
        TEST_A("testvalueA"),
        TEST_B("testvalueB");

        private final String value;

        @JsonCreator
        public static Prop from(String value) {
            return Arrays.stream(values())
                    .filter(type -> type.value.equals(value))
                    .findFirst()
                    .orElseThrow(() -> new IllegalArgumentException("想定外の値が返却されました。。：" + value));
        }

        public String getValue() {
            return value;
        }
    }
}
