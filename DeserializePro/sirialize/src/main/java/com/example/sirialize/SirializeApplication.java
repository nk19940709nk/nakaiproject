package com.example.sirialize;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SirializeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SirializeApplication.class, args);
	}

}
