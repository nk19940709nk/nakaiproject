/**
 * セレクトボックス選択値リクエストクラス
 */
export class SearchRequest {

    /** 場所 */
    public placeVal?: number;

    /** ジャンル */
    public genreVal?: number;

    /** 天気 */
    public weatherVal?: number;
}