/**
 * 登録済みジャンルオブジェクトクラス
 */
 export class RegistedGenre {

    /** ジャンル名 */
    public genreId!: number;

    /** ジャンルアイコン */
    public genreName!: string;
}