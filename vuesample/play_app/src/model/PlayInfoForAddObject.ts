/**
 * あそびオブジェクトクラス
 */
export class PlayInfoForAddObject {

    /** あそび名称 */
    public playInfoName!: string;

    /** あそびサイトURL */
    public playInfoSiteUrl!: string;

    /** あそびイメージURL */
    public playInfoImageUrl!: string;

    /** あそびイメージURL */
    public playInfoSnsUrl!: string;

    /** あそび推奨天気 */
    public weatherId?: number;

    /** あそび場所 */
    public placeId?: number;

    /** あそびジャンル */
    public genreId?: number;

    /** あそび説明 */
    public description!: string;

    /** あそびタグ情報 */
    public playInfoTag!: string;
}