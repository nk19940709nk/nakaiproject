/**
 * トピック更新対象オブジェクトクラス
 */
export class TopicUpdateRequest {

    /** 対象ジャンル */
    public genreList?: number[];

}