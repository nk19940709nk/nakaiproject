/**
 * トピック情報オブジェクトクラス
 */
export class TopicObject {

    /** トピックID */
    public topicId?: number;

    /** トピックサイトタイトル */
    public topicSiteTitle?: string;

    /** トピックサイトURL */
    public topicSiteUrl?: string;

    /** トピックOGPURL */
    public topicOgpUrl?: string;

    /** ジャンルアイコン */
    public genreIcon?: string;
}