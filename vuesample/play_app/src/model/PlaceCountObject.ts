export class PlaceCountObject {

    /** 地名ID */
    public placeId!: number;

    /** 地名 */
    public placeName!: string;

    /** 地名カウント */
    public placeCount!: number;
}