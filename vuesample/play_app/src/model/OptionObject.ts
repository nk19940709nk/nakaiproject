
export class OptionObject {

    /** ラベル */
    public label!: string;

    /** 値 */
    public value!: number;
}