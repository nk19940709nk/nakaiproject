/**
 * ログインオブジェクトクラス
 */
export class LoginInfoObject {

    /** ユーザー名 */
    public username!: string;

    /** パスワード */
    public password!: string;
}