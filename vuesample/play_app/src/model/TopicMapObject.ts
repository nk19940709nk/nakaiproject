/**
 * トピック情報オブジェクトクラス
 */

import { TopicObject } from '@/model/TopicObject'
export class TopicMapObject {

    /** トピックID */
    public topicObjectList?: TopicObject[];

    /** ジャンルID */
    public genreId?: number;


}