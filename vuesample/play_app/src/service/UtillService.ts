import { ConditionsData } from '@/model/ConditionsData';
import { useStore } from "vuex";
import { ApiService } from "@/service/ApiService";
import { OptionObject } from '@/model/OptionObject';
import { PlaceCountObject } from '@/model/PlaceCountObject';
import { PlaceCountRequest } from '@/model/PlaceCountRequest';
import { RegistedGenre } from '@/model/RegistedGenre';


export class UtilService {

    // インスタンス
    public store = useStore();

    // 絞り込み条件取得
    public setConditions(): void {
        const apiService = new ApiService();

        const genreOptions: OptionObject[] = [];
        const weatherOptions: OptionObject[] = [];
        const placeOptions: OptionObject[] = [];

        apiService.getConditionsData().then((ret: ConditionsData) => {
            ret.weatherList?.forEach((weather) => {
                const option = new OptionObject();
                option.label = weather.weatherName;
                option.value = weather.weatherId;
                weatherOptions.push(option);
            })

            this.store.commit("setWeatherOptions", weatherOptions);
        });
        apiService.getPlaceCount(new PlaceCountRequest).then((ret: PlaceCountObject[]) => {

            ret.forEach((placeCountObject) => {
                const option = new OptionObject();
                option.value = placeCountObject.placeId;
                option.label = placeCountObject.placeName + "(" + placeCountObject.placeCount + ")";
                placeOptions.push(option);
            })

            this.store.commit("setPlaceOptions", placeOptions);
        });
        apiService.getRegistedGenre().then((ret: RegistedGenre[]) => {

            ret.forEach((registedGenre) => {
                const option = new OptionObject();
                option.value = registedGenre.genreId;
                option.label = registedGenre.genreName;
                genreOptions.push(option);
            })
            this.store.commit("setGenreOptions", genreOptions);
        });
    }

    // 絞り込み条件取得
    public updateConditions(request: PlaceCountRequest): void {
        const apiService = new ApiService();
        const placeOptions: OptionObject[] = [];
        apiService.getPlaceCount(request).then((ret: PlaceCountObject[]) => {

            ret.forEach((placeCountObject) => {
                const option = new OptionObject();
                option.value = placeCountObject.placeId;
                option.label = placeCountObject.placeName + "(" + placeCountObject.placeCount + ")";
                placeOptions.push(option);
            })

            this.store.commit("setPlaceOptions", placeOptions);
        });
    }


    public setConditionsAdd(): void {
        const apiService = new ApiService();

        const genreOptions: OptionObject[] = [];
        const weatherOptions: OptionObject[] = [];
        const placeOptions: OptionObject[] = [];

        apiService.getConditionsData().then((ret: ConditionsData) => {

            const allOption = new OptionObject();
            allOption.label = '該当なし';
            allOption.value = 0;

            placeOptions.push(allOption);
            weatherOptions.push(allOption);

            ret.genreList?.forEach((genre) => {
                const option = new OptionObject();
                option.label = genre.genreName;
                option.value = genre.genreId;
                genreOptions.push(option);
            })

            ret.placeList?.forEach((place) => {
                const option = new OptionObject();
                option.label = place.placeName;
                option.value = place.placeId;
                placeOptions.push(option);
            })

            ret.weatherList?.forEach((weather) => {
                const option = new OptionObject();
                option.label = weather.weatherName;
                option.value = weather.weatherId;
                weatherOptions.push(option);
            })

            this.store.commit("setGenreOptions", genreOptions);
            this.store.commit("setPlaceOptions", placeOptions);
            this.store.commit("setWeatherOptions", weatherOptions);
        });
    }

    // 空チェック
    public isEmpty(checkValue: string): boolean {
        if (checkValue == null || checkValue.length == 0) {
            return true;
        }
        return false;
    }
}