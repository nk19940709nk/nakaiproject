import axios from "axios";
import { TopicObject } from '@/model/TopicObject';
import { PlayInfoObject } from '@/model/PlayInfoObjec';
import { ConditionsData } from '@/model/ConditionsData';
import { SearchRequest } from '@/model/SerachRequest';
import { PlayInfoForAddObject } from '@/model/PlayInfoForAddObject'
import { GenreObject } from '@/model/GenreObject';
import { TopicUpdateRequest } from '@/model/TopicUpdateRequest';
import { LoginInfoObject } from '@/model/LoginInfoObject';
import { PlaceCountObject } from '@/model/PlaceCountObject';
import { PlaceCountRequest } from '@/model/PlaceCountRequest';
import { RegistedGenre } from '@/model/RegistedGenre';

/**
 * APIアクセスサービス
 */
export class ApiService {

    public rtnTopicObjects!: TopicObject[];
    public rtnTopicMap!: Map<string, TopicObject[]>;
    public rtnPlayInfo!: PlayInfoObject;
    public rtnPlayInfos!: PlayInfoObject[];
    public rtnPlayInfoMap!: Map<string, PlayInfoObject[]>;
    public rtnConditionsData!: ConditionsData;
    public rtnPlayInfoAddResult!: boolean;
    public rtnPlayInfoDeleteResult!: boolean;
    public rtnPlayInfoUpdateResult!: boolean;
    public rtnLoginResult!: boolean;
    public rtnPlaceCountObject!: PlaceCountObject[];
    public rtnRegistedGenre!: RegistedGenre[];

    /**
     * メニューリスト取得メソッド
     * @returns 
     */
    public async getTopicList(): Promise<Map<string, TopicObject[]>> {
        this.rtnTopicObjects = [];
        this.rtnTopicMap = new Map;
        await axios
            .get(process.env.VUE_APP_GET_TOPICLIST_URL)
            .then((response) => {
                for (const key in response.data) {
                    response.data[key].forEach((resData: TopicObject) => {
                        const data: TopicObject = {
                            topicId: resData["topicId"],
                            topicSiteTitle: resData["topicSiteTitle"],
                            topicSiteUrl: resData["topicSiteUrl"],
                            topicOgpUrl: resData["topicOgpUrl"],
                            genreIcon: resData["genreIcon"],

                        };
                        this.rtnTopicObjects.push(data);
                    });
                    this.rtnTopicMap.set(key, this.rtnTopicObjects);
                    this.rtnTopicObjects = [];
                }

            })
            .catch((error) => {
                console.log(error);
            });

        return this.rtnTopicMap;
    }

    /**
     * あそび情報取得メソッド
     * @returns 
     */
    public async getPlayInfo(request: SearchRequest): Promise<PlayInfoObject[]> {
        this.rtnPlayInfos = [];
        await axios
            .post(process.env.VUE_APP_GET_PLAYINFO_URL, {
                genreVal: request.genreVal,
                placeVal: request.placeVal,
                weatherVal: request.weatherVal
            })
            .then((response) => {
                response.data.forEach((resData: PlayInfoObject) => {
                    const data: PlayInfoObject = {
                        playInfoId: resData["playInfoId"],
                        playInfoName: resData["playInfoName"],
                        playInfoSiteUrl: resData["playInfoSiteUrl"],
                        playInfoOgpUrl: resData["playInfoOgpUrl"],
                        playInfoWeather: resData["playInfoWeather"],
                        playInfoPlace: resData["playInfoPlace"],
                        playInfoGenre: resData["playInfoGenre"],
                        playInfoSnsUrl: resData["playInfoSnsUrl"],
                        playInfoDescription: resData["playInfoDescription"].length > 60 ? resData["playInfoDescription"].slice(0, 60) + "…" : resData["playInfoDescription"],
                        genreName: resData["genreName"],
                        genreIcon: resData["genreIcon"],
                        playInfoTag: resData["playInfoTag"],

                    };
                    this.rtnPlayInfos.push(data);
                });
            })
            .catch((error) => {
                console.log(error);
            });
        return this.rtnPlayInfos;
    }


    /**
     * メニューリスト取得メソッド
     * @returns 
     */
    public async getPlayInfoAll(): Promise<Map<string, PlayInfoObject[]>> {
        this.rtnPlayInfos = [];
        this.rtnPlayInfoMap = new Map;
        await axios
            .get(process.env.VUE_APP_GET_PLAYINFO_ALL_URL)
            .then((response) => {
                for (const key in response.data) {
                    response.data[key].forEach((resData: PlayInfoObject) => {
                        console.log(resData);
                        const data: PlayInfoObject = {
                            playInfoId: resData["playInfoId"],
                            playInfoName: resData["playInfoName"],
                            playInfoSiteUrl: resData["playInfoSiteUrl"],
                            playInfoOgpUrl: resData["playInfoOgpUrl"],
                            playInfoWeather: resData["playInfoWeather"],
                            playInfoPlace: resData["playInfoPlace"],
                            playInfoGenre: resData["playInfoGenre"],
                            playInfoSnsUrl: resData["playInfoSnsUrl"],
                            playInfoDescription: resData["playInfoDescription"].length > 80 ? resData["playInfoDescription"].slice(0, 80) + "…" : resData["playInfoDescription"],
                            playInfoTag: resData["playInfoTag"],
                            genreName: resData["genreName"],
                            genreIcon: resData["genreIcon"],

                        };
                        this.rtnPlayInfos.push(data);
                    });
                    this.rtnPlayInfoMap.set(key, this.rtnPlayInfos);
                    this.rtnPlayInfos = [];
                }

            })
            .catch((error) => {
                console.log(error);
            });

        return this.rtnPlayInfoMap;
    }


    /**
     * セレクトボックスデータ取得メソッド
     * @returns 
     */
    public async getConditionsData(): Promise<ConditionsData> {
        await axios
            .get(process.env.VUE_APP_GET_CONDITIONS_URL)
            .then((response) => {
                this.rtnConditionsData = {
                    genreList: response.data["genreList"],
                    placeList: response.data["placeList"],
                    weatherList: response.data["weatherList"],
                };
            })
            .catch((error) => {
                console.log(error);
            });
        return this.rtnConditionsData;
    }

    /**
     * あそび情報追加
     * @param request 
     * @returns 
     */
    public async addPlayInfo(request: PlayInfoForAddObject): Promise<boolean> {
        this.rtnPlayInfoAddResult = false;
        await axios
            .post(process.env.VUE_APP_POST_PLAYINFO_ADD_URL, {
                playInfoName: request.playInfoName,
                playInfoSiteUrl: request.playInfoSiteUrl,
                playInfoImageUrl: request.playInfoImageUrl,
                playInfoSnsUrl: request.playInfoSnsUrl,
                playInfoWeather: request.weatherId,
                playInfoPlace: request.placeId,
                playInfoGenre: request.genreId,
                playInfoDescription: request.description,
                playInfoTag: request.playInfoTag
            })
            .then((response) => {
                this.rtnPlayInfoAddResult = response.data;
            })
            .catch((error) => {
                console.log(error);
            });
        return this.rtnPlayInfoAddResult;
    }

    /**
     * あそび情報削除
     * @param request 
     * @returns 
     */
    public async deletePlayInfo(request: number): Promise<boolean> {
        this.rtnPlayInfoDeleteResult = false;
        await axios
            .post(process.env.VUE_APP_POST_PLAYINFO_DELETE_URL, {
                playInfoId: request,
            })
            .then((response) => {
                this.rtnPlayInfoDeleteResult = response.data;
            })
            .catch((error) => {
                console.log(error);
            });
        return this.rtnPlayInfoDeleteResult;
    }

    /**
     * あそび情報更新
     * @param request 
     * @returns 
     */
    public async updatePlayInfo(request: PlayInfoObject): Promise<boolean> {
        this.rtnPlayInfoUpdateResult = false;
        await axios
            .post(process.env.VUE_APP_POST_PLAYINFO_UPDATE_URL, {
                playInfoId: request.playInfoId,
                playInfoName: request.playInfoName,
                playInfoSiteUrl: request.playInfoSiteUrl,
                playInfoOgpUrl: request.playInfoOgpUrl,
                playInfoSnsUrl: request.playInfoSnsUrl,
                playInfoWeather: request.playInfoWeather,
                playInfoPlace: request.playInfoPlace,
                playInfoGenre: request.playInfoGenre,
                playInfoDescription: request.playInfoDescription,
                playInfoTag: request.playInfoTag.split(",").join(","),
            })
            .then((response) => {
                this.rtnPlayInfoUpdateResult = response.data;
            })
            .catch((error) => {
                console.log(error);
            });
        return this.rtnPlayInfoUpdateResult;
    }

    /**
     * ジャンル追加
     * @param request 
     */
    public async addGenre(request: GenreObject) {
        await axios
            .post(process.env.VUE_APP_POST_GENRE_INSERT_URL, {
                genreName: request.genreName,
                genreIcon: request.genreIcon,
            })
            .catch((error) => {
                console.log(error);
            });
    }

    /**
     * トピック情報更新
     * @param request 
     */
    public async updateTopicInfo(request: TopicUpdateRequest) {
        this.rtnPlayInfoUpdateResult = false;
        await axios
            .post(process.env.VUE_APP_POST_TOPICUPDATE_URL, {
                genreList: request.genreList,
            })
            .catch((error) => {
                console.log(error);
            });
    }

    /**
     * ログインAPIアクセス
     * @param request 
     * @returns 
     */
    public async login(request: LoginInfoObject): Promise<boolean> {
        this.rtnLoginResult = false;
        await axios
            .post(process.env.VUE_APP_GET_LOGIN_URL, {
                username: request.username,
                password: request.password,
            })
            .then((response) => {
                this.rtnLoginResult = response.data;
            })
            .catch((error) => {
                console.log(error);
            });
        return this.rtnLoginResult;
    }


    /**
     * 地名カウント取得
     * @returns 
     */
    public async getPlaceCount(request: PlaceCountRequest): Promise<PlaceCountObject[]> {
        this.rtnPlaceCountObject = [];
        await axios
            .post(process.env.VUE_APP_POST_PLACECOUNT_URL, {
                genreId: request.genreId,
            })
            .then((response) => {
                response.data.forEach((resData: PlaceCountObject) => {
                    const data: PlaceCountObject = {
                        placeId: resData["placeId"],
                        placeName: resData["placeName"],
                        placeCount: resData["placeCount"]

                    };
                    this.rtnPlaceCountObject.push(data);
                });
            })
            .catch((error) => {
                console.log(error);
            });
        return this.rtnPlaceCountObject;
    }

    /**
     * 登録済みジャンル取得
     * @returns 
     */
    public async getRegistedGenre(): Promise<RegistedGenre[]> {
        this.rtnRegistedGenre = [];
        await axios
            .get(process.env.VUE_APP_GET_REGISTEDGENRE_URL)
            .then((response) => {
                response.data.forEach((resData: RegistedGenre) => {
                    const data: RegistedGenre = {
                        genreId: resData["genreId"],
                        genreName: resData["genreName"],

                    };
                    this.rtnRegistedGenre.push(data);
                });
            })
            .catch((error) => {
                console.log(error);
            });
        return this.rtnRegistedGenre;
    }


}