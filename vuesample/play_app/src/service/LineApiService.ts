import liff from "@line/liff";

export class LineApiService {

    // イニシャライザ
    initializeliff() {
        liff
            .init({
                liffId: process.env.VUE_APP_LINE_CHANNEL_ID,
            })
            .then(() => {
                this.getProfile();
                this.getIdToken();
            })
            .catch((err) => {
                console.log(err);
            });
    }
    /** プロフィール取得 */
    getProfile() {
        console.log(liff.getProfile());
    }

    /** IDトークン取得 */
    getIdToken() {
        console.log(liff.getIDToken());
    }

    /** ログアウト処理 */
    logout() {
        if (liff.isLoggedIn()) {
            liff.logout();
        }
    }
    /** Liffアプリを閉じる */
    closeWindow() {
        liff.closeWindow();
    }
}