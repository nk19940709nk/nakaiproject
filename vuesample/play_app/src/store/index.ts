import { createStore } from 'vuex';
import { PlayInfoObject } from '@/model/PlayInfoObjec';
import { OptionObject } from '@/model/OptionObject';
import { PlaceCountObject } from '@/model/PlaceCountObject';

export interface Store {
  playInfoObject?: PlayInfoObject,
  playInfoObjects?: PlayInfoObject[],
  updatePlayInfoObject?: PlayInfoObject,
  genreMap?: Map<number, string>,
  placeMap?: Map<number, string>,
  weatherMap?: Map<number, string>,
  genreOptions?: OptionObject[],
  weatherOptions?: OptionObject[],
  placeOptions?: OptionObject[],
  placeCountOptions?: OptionObject[]
}

export default createStore<Store>({
  state: {
  },
  getters: {
  },
  mutations: {
    setPlayInfo: (state: Store, playInfoObject: PlayInfoObject) => {
      state.playInfoObject = playInfoObject;
    },
    setPlayInfos: (state: Store, playInfoObjects: PlayInfoObject[]) => {
      state.playInfoObjects = playInfoObjects;
    },
    setUpdatePlayInfoObject: (state: Store, value: PlayInfoObject) => {
      state.updatePlayInfoObject = value;
    },
    setGenreMap: (state: Store, value: Map<number, string>) => {
      state.genreMap = value;
    },
    setPlaceMap: (state: Store, value: Map<number, string>) => {
      state.placeMap = value;
    },
    setWeatherMap: (state: Store, value: Map<number, string>) => {
      state.weatherMap = value;
    },
    setGenreOptions: (state: Store, value: OptionObject[]) => {
      state.genreOptions = value;
    },
    setWeatherOptions: (state: Store, value: OptionObject[]) => {
      state.weatherOptions = value;
    },
    setPlaceOptions: (state: Store, value: OptionObject[]) => {
      state.placeOptions = value;
    },
  },
  actions: {
  },
  modules: {
  }
})
