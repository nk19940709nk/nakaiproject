import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import TopicView from '@/views/TopicView.vue'
import SearchView from '@/views/SearchView.vue'
import SearchResultView from '@/views/SearchResultView.vue'
import LoginView from '@/views/LoginView.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/topic',
    name: 'topic',
    component: TopicView
  },
  {
    path: '/search',
    name: 'search',
    component: SearchView
  },
  {
    path: '/result',
    name: 'result',
    component: SearchResultView
  },
  {
    path: '/login',
    name: 'login',
    component: LoginView
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
