export class CalendarEventsObject {
    /** 名称 */
    title?: string;
    /** 開始時刻 */
    start?: string;
    /** 終了時刻 */
    end?: string;
    /** 表示色 */
    color?: string;
    // /** カテゴリー */
    // category?: string;
    // /** イベント詳細 */
    // details?: string;
}