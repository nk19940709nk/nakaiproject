export class CalendarViewObject {
    /** 表示用テキスト */
    viewText?: string;
    /**  所要時間 */
    menuTime?: string;
    /** 開始時刻 */
    startTime?: string;
    /** 終了時刻 */
    endTime?: string;
}