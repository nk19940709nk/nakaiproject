export class Profile {
    /** ユーザーID */
    userId?: string;
    /** 表示名 */
    displayName?: string;
    /** 画像URL */
    pictureUrl?: string;
    /** ステータス*/
    statusMessage?: string;
}