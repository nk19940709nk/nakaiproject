export class MenuObject {
    /** メニューID */
    menuId?: number;
    /** メニュー名称 */
    menuName?: string;
    /** 料金 */
    menuPrice?: number;
    /** メニュー詳細 */
    menuDetail?: string;
    /** 所要時間 */
    menuTime?: string
    /** 削除フラグ */
    delFlg?: boolean
}