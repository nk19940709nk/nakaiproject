import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import HomeView from '@/views/HomeView.vue';
import CalendarView from '@/views/CalendarView.vue';
import ConfirmView from '@/views/ConfirmView.vue';
import CompleteComponent from '@/views/CompleteView.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/calendar',
    name: 'calendar',
    component: CalendarView,
  },
  {
    path: '/confirm',
    name: 'confirm',
    component: ConfirmView,
  },
  {
    path: '/complete',
    name: 'complete',
    component: CompleteComponent,
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
