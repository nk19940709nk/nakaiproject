import liff from "@line/liff";
// import { useStore } from "vuex";

export class LineApiService {
    // /** Liffイニシャライザ */
    // initializeliff() {
    //     this.store.commit("setTokenId", "tsetsetse");
    //     liff
    //         .init({
    //             liffId: process.env.VUE_APP_LINE_CHANNEL_ID,
    //         })
    //         .then(() => {
    //             this.getProfile();
    //             this.getIdToken();
    //         })
    //         .catch((err) => {
    //             this.store.commit("setErrMsg", err);
    //         });
    // }

    // /** プロフィール取得 */
    // getProfile() {
    //     liff.getProfile().then((value) => {
    //         this.store.commit("setProfile", value);
    //     });
    // }

    // /** IDトークン取得 */
    // getIdToken() {
    //     this.store.commit("setTokenId", liff.getIDToken());
    // }

    /** ログアウト処理 */
    logout() {
        if (liff.isLoggedIn()) {
            liff.logout();
        }
    }
    /** Liffアプリを閉じる */
    closeWindow() {
        liff.closeWindow();
    }
}