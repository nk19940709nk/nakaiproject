import { CalendarViewObject } from '@/model/CalendarViewObject';
import { CalendarEventsObject } from '@/model/CalendarEventsObject';
import { MenuObject } from '@/model/MenuObject';
import axios from "axios";

export class CalendarService {
    public rtnCalendarEventsObject!: CalendarEventsObject[];

    public async getReservedList(checkMenuObj: MenuObject): Promise<CalendarEventsObject[]> {
        this.rtnCalendarEventsObject = [];
        // 予約データを取得
        await axios
            .post(process.env.VUE_APP_POST_CALENDARVIEW_URL, {
                menuTime: checkMenuObj.menuTime,
            })
            .then((response) => {
                response.data.forEach((element: CalendarViewObject) => {
                    if (element.viewText === "〇") {
                        const data: CalendarEventsObject = {
                            title: element["viewText"],
                            start: element["startTime"],
                            end: element["endTime"],
                            color: "#ff9f89",
                        };
                        this.rtnCalendarEventsObject.push(data);
                    }
                });

            })
            .catch((error) => {
                console.log(error);
            });

        return this.rtnCalendarEventsObject;
    }

}
