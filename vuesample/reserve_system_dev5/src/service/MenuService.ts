import axios from "axios";
import { MenuObject } from '@/model/MenuObject'

export class MenuService {

    public rtnMenuObjList!: MenuObject[];

    /**
     * メニューリスト取得メソッド
     * @returns 
     */
    public async getMenuList(): Promise<MenuObject[]> {
        this.rtnMenuObjList = [];
        await axios
            .get(process.env.VUE_APP_GET_MENULIST_URL)
            .then((response) => {
                response.data.forEach((resData: MenuObject) => {
                    const data: MenuObject = {
                        menuId: resData["menuId"],
                        menuName: resData["menuName"],
                        menuPrice: resData["menuPrice"],
                        menuDetail: resData["menuDetail"],
                        menuTime: resData["menuTime"],
                        delFlg: resData["delFlg"],
                    };
                    this.rtnMenuObjList.push(data);
                });
            })
            .catch((error) => {
                console.log(error);
            });
        return this.rtnMenuObjList;
    }
}
