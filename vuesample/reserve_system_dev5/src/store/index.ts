import { createStore } from 'vuex';
import { MenuObject } from '@/model/MenuObject';
import { Profile } from '@/model/Profile';
export interface Store {
  selectedMenu: MenuObject,
  selectedStartTime: Date | null,
  errMsg: string[],
  tokenId: string,
  profile: Profile,
}



export const store = createStore<Store>({
  state: {
    selectedMenu: {},
    selectedStartTime: new Date,
    errMsg: [],
    tokenId: "",
    profile: {},
  },
  getters: {
  },
  mutations: {
    setSelectedMenu: (state: Store, selectedMenu: MenuObject) => {
      state.selectedMenu = selectedMenu;
    },
    setSelectedStartTime: (state: Store, selectedTime: Date) => {
      state.selectedStartTime = selectedTime;
    },
    setErrMsg: (state: Store, err: string) => {
      state.errMsg.push(err);
    },
    setTokenId: (state: Store, tokenId: string) => {
      state.tokenId = tokenId;
    },
    setProfile: (state: Store, profile: Profile) => {
      state.profile = profile;
    },
  },
  actions: {
  },
  modules: {
  }
})
