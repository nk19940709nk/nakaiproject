import { createStore } from 'vuex';
import { PlayInfoObject } from '@/model/PlayInfoObjec';
import { OptionObject } from '@/model/OptionObject';

export interface Store {
  updatePlayInfoObject?: PlayInfoObject,
  genreOptions?: OptionObject[],
  weatherOptions?: OptionObject[],
  placeOptions?: OptionObject[],
}

export default createStore<Store>({
  state: {
  },
  getters: {
  },
  mutations: {

    setUpdatePlayInfoObject: (state: Store, value: PlayInfoObject) => {
      state.updatePlayInfoObject = value;
    },
    setGenreOptions: (state: Store, value: OptionObject[]) => {
      state.genreOptions = value;
    },
    setWeatherOptions: (state: Store, value: OptionObject[]) => {
      state.weatherOptions = value;
    },
    setPlaceOptions: (state: Store, value: OptionObject[]) => {
      state.placeOptions = value;
    },
  },
  actions: {
  },
  modules: {
  }
})
