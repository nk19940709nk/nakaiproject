import axios from "axios";
import { PlayInfoObject } from '@/model/PlayInfoObjec';
import { ConditionsData } from '@/model/ConditionsData';
import { PlayInfoForAddObject } from '@/model/PlayInfoForAddObject'
import { GenreObject } from '@/model/GenreObject';
import { TopicUpdateRequest } from '@/model/TopicUpdateRequest';
import { LoginInfoObject } from '@/model/LoginInfoObject';

/**
 * APIアクセスサービス
 */
export class ApiService {

    public rtnPlayInfos!: PlayInfoObject[];
    public rtnPlayInfoMap!: Map<string, PlayInfoObject[]>;
    public rtnConditionsData!: ConditionsData;
    public rtnPlayInfoAddResult!: boolean;
    public rtnPlayInfoDeleteResult!: boolean;
    public rtnPlayInfoUpdateResult!: boolean;
    public rtnLoginResult!: boolean;


    /**
     * メニューリスト取得メソッド
     * @returns 
     */
    public async getPlayInfoAll(): Promise<Map<string, PlayInfoObject[]>> {
        this.rtnPlayInfos = [];
        this.rtnPlayInfoMap = new Map;
        await axios
            .get(process.env.VUE_APP_GET_PLAYINFO_ALL_URL)
            .then((response) => {
                for (const key in response.data) {
                    response.data[key].forEach((resData: PlayInfoObject) => {
                        console.log(resData);
                        const data: PlayInfoObject = {
                            playInfoId: resData["playInfoId"],
                            playInfoName: resData["playInfoName"],
                            playInfoSiteUrl: resData["playInfoSiteUrl"],
                            playInfoOgpUrl: resData["playInfoOgpUrl"],
                            playInfoWeather: resData["playInfoWeather"],
                            playInfoPlace: resData["playInfoPlace"],
                            playInfoGenre: resData["playInfoGenre"],
                            playInfoSnsUrl: resData["playInfoSnsUrl"],
                            playInfoDescription: resData["playInfoDescription"].length > 80 ? resData["playInfoDescription"].slice(0, 80) + "…" : resData["playInfoDescription"],
                            playInfoTag: resData["playInfoTag"],
                            genreName: resData["genreName"],
                            genreIcon: resData["genreIcon"],

                        };
                        this.rtnPlayInfos.push(data);
                    });
                    this.rtnPlayInfoMap.set(key, this.rtnPlayInfos);
                    this.rtnPlayInfos = [];
                }

            })
            .catch((error) => {
                console.log(error);
            });

        return this.rtnPlayInfoMap;
    }


    /**
     * セレクトボックスデータ取得メソッド
     * @returns 
     */
    public async getConditionsData(): Promise<ConditionsData> {
        await axios
            .get(process.env.VUE_APP_GET_CONDITIONS_URL)
            .then((response) => {
                this.rtnConditionsData = {
                    genreList: response.data["genreList"],
                    placeList: response.data["placeList"],
                    weatherList: response.data["weatherList"],
                };
            })
            .catch((error) => {
                console.log(error);
            });
        return this.rtnConditionsData;
    }

    /**
     * あそび情報追加
     * @param request 
     * @returns 
     */
    public async addPlayInfo(request: PlayInfoForAddObject): Promise<boolean> {
        this.rtnPlayInfoAddResult = false;
        await axios
            .post(process.env.VUE_APP_POST_PLAYINFO_ADD_URL, {
                playInfoName: request.playInfoName,
                playInfoSiteUrl: request.playInfoSiteUrl,
                playInfoOgpUrl: request.playInfoImageUrl,
                playInfoSnsUrl: request.playInfoSnsUrl,
                playInfoWeather: request.weatherId,
                playInfoPlace: request.placeId,
                playInfoGenre: request.genreId,
                playInfoDescription: request.description,
                playInfoTag: request.playInfoTag
            })
            .then((response) => {
                this.rtnPlayInfoAddResult = response.data;
            })
            .catch((error) => {
                console.log(error);
            });
        return this.rtnPlayInfoAddResult;
    }

    /**
     * あそび情報削除
     * @param request 
     * @returns 
     */
    public async deletePlayInfo(request: number): Promise<boolean> {
        this.rtnPlayInfoDeleteResult = false;
        await axios
            .post(process.env.VUE_APP_POST_PLAYINFO_DELETE_URL, {
                playInfoId: request,
            })
            .then((response) => {
                this.rtnPlayInfoDeleteResult = response.data;
            })
            .catch((error) => {
                console.log(error);
            });
        return this.rtnPlayInfoDeleteResult;
    }

    /**
     * あそび情報更新
     * @param request 
     * @returns 
     */
    public async updatePlayInfo(request: PlayInfoObject): Promise<boolean> {
        this.rtnPlayInfoUpdateResult = false;
        await axios
            .post(process.env.VUE_APP_POST_PLAYINFO_UPDATE_URL, {
                playInfoId: request.playInfoId,
                playInfoName: request.playInfoName,
                playInfoSiteUrl: request.playInfoSiteUrl,
                playInfoOgpUrl: request.playInfoOgpUrl,
                playInfoSnsUrl: request.playInfoSnsUrl,
                playInfoWeather: request.playInfoWeather,
                playInfoPlace: request.playInfoPlace,
                playInfoGenre: request.playInfoGenre,
                playInfoDescription: request.playInfoDescription,
                playInfoTag: request.playInfoTag.split(",").join(","),
            })
            .then((response) => {
                this.rtnPlayInfoUpdateResult = response.data;
            })
            .catch((error) => {
                console.log(error);
            });
        return this.rtnPlayInfoUpdateResult;
    }

    /**
     * ジャンル追加
     * @param request 
     */
    public async addGenre(request: GenreObject): Promise<void> {
        await axios
            .post(process.env.VUE_APP_POST_GENRE_INSERT_URL, {
                genreName: request.genreName,
                genreIcon: request.genreIcon,
            })
            .catch((error) => {
                console.log(error);
            });
    }

    /**
     * トピック情報更新
     * @param request 
     */
    public async updateTopicInfo(request: TopicUpdateRequest): Promise<void> {
        this.rtnPlayInfoUpdateResult = false;
        await axios
            .post(process.env.VUE_APP_POST_TOPICUPDATE_URL, {
                genreList: request.genreList,
            })
            .catch((error) => {
                console.log(error);
            });
    }

    /**
     * ログインAPIアクセス
     * @param request 
     * @returns 
     */
    public async login(request: LoginInfoObject): Promise<boolean> {
        this.rtnLoginResult = false;
        await axios
            .post(process.env.VUE_APP_GET_LOGIN_URL, {
                username: request.username,
                password: request.password,
            })
            .then((response) => {
                this.rtnLoginResult = response.data;
            })
            .catch((error) => {
                console.log(error);
            });
        return this.rtnLoginResult;
    }

}