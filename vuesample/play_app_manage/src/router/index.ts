import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import ManageHomeView from '@/views/ManageHomeView.vue'
import ManageAddView from '@/views/ManageAddView.vue'
import ManageListView from '@/views/ManageListView.vue';
import ManageDetailView from '@/views/ManageDetailView.vue';
import ManageTopicUpdateView from '@/views/ManageTopicUpdateView.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'ManageHomeView',
    component: ManageHomeView
  },
  {
    path: '/manageadd',
    name: 'manageadd',
    component: ManageAddView,
  },
  {
    path: '/managelist',
    name: 'managelist',
    component: ManageListView,
  },
  {
    path: '/managedetail',
    name: 'managedetail',
    component: ManageDetailView,
  },
  {
    path: '/managetopicupdate',
    name: 'managetopicupdate',
    component: ManageTopicUpdateView,
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
