/**
 * セレクトボックス選択データクラス
 */
export class ConditionsData {

    /** ジャンルリスト */
    public genreList!: Genre[];

    /** 場所リスト */
    public placeList!: Place[];

    /** 天気情報リスト */
    public weatherList!: Weather[];
}

/** ジャンルクラス */
class Genre {
    public genreId!: number;

    public genreName!: string;
}
/** 場所クラス */
class Place {
    public placeId!: number;

    public placeName!: string;
}
/** 天気クラス */
class Weather {
    public weatherId!: number;

    public weatherName!: string;
}