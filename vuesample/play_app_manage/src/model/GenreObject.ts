/**
 * ジャンルオブジェクトクラス
 */
export class GenreObject {

    /** ジャンル名 */
    public genreName!: string;

    /** ジャンルアイコン */
    public genreIcon!: string;
}