/**
 * あそびオブジェクトクラス
 */
export class PlayInfoObject {

    /** あそび情報ID */
    public playInfoId!: number;

    /** あそび名称 */
    public playInfoName!: string;

    /** あそびサイトURL */
    public playInfoSiteUrl!: string;

    /** あそびOGPURL */
    public playInfoOgpUrl?: string;

    /** あそびSNSURL */
    public playInfoSnsUrl?: string;

    /** あそび推奨天気 */
    public playInfoWeather!: number;

    /** あそび場所 */
    public playInfoPlace!: number;

    /** あそびジャンル */
    public playInfoGenre!: number;

    /** あそび説明 */
    public playInfoDescription!: string;

    /** ジャンル名称 */
    public genreName?: string;

    /** ジャンルアイコン */
    public genreIcon?: string;

    /** あそびタグ情報 */
    public playInfoTag!: string;
}