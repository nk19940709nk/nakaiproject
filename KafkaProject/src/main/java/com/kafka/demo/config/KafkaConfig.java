package com.kafka.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@ConfigurationProperties(prefix="kafka")
@Data
public class KafkaConfig {
	private String bootstrapServers;
	private String groupId;
	private String topic;
}
