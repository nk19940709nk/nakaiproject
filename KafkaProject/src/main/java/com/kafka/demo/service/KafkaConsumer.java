package com.kafka.demo.service;

import java.nio.charset.StandardCharsets;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {
	@KafkaListener(topics = "${kafka.topic}")
	public void consume(ConsumerRecord<String, String> record) {
		System.out.println("consumeKey: " + record.key() + ", consumeValue: " + record.value());
		record.headers().forEach(header -> {
			System.out.println("headerKey : " + header.key());
			String headerValue = new String(header.value(), StandardCharsets.UTF_8);
			System.out.println("headerValue : " + headerValue);
		});
	}
}
