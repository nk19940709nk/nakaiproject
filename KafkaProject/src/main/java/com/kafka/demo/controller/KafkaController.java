package com.kafka.demo.controller;

import java.util.concurrent.CompletableFuture;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.kafka.demo.config.KafkaConfig;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class KafkaController {

	private final KafkaTemplate<String, String> kafkaTemplate;

	private final KafkaConfig kafkaConfig;

	@GetMapping("/send/{key}/{value}")
	public void producer(@PathVariable("key") String key, @PathVariable("value") String value) {
		kafkaTemplate.send(kafkaConfig.getTopic(), key, value);
	}

	@GetMapping("/send")
	public void asyncProducer() {
		CompletableFuture<SendResult<String, String>> future = kafkaTemplate.send(kafkaConfig.getTopic(), "testkey",
				"something");
		future.whenComplete((result, ex) -> {
			System.out.println(result);
		});
	}

}
