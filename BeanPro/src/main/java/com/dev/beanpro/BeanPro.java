package com.dev.beanpro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeanPro {
    public static void main(String[] args) {
        SpringApplication.run(BeanPro.class, args);
    }
}
