package com.dev.beanpro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TestController {

	@Autowired
	Environment env;
	@Value("${nakaitest.test}")
	private String test;

	@GetMapping("test")
	public void testCtr() {
		System.out.println(test);
		System.out.println(env);
	}

}
