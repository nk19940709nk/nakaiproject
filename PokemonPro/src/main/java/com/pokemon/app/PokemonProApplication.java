package com.pokemon.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class PokemonProApplication {

	public static void main(String[] args) {
		SpringApplication.run(PokemonProApplication.class, args);
	}

}
