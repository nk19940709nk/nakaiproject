package com.pokemon.app.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.pokemon.app.model.db.UnionDao;
import com.pokemon.app.model.db.request.UnionRequest;

@Mapper
public interface UnionMapper {

	void insertUnion(UnionRequest request);

	List<UnionDao> selectUnion(String username);

	UnionDao selectUnionByTicketId(Integer ticketId);

	List<UnionDao> selectUnionAll();

	Integer tempDeleteUnion();

	Integer deleteUnion();

	Integer countUnionByName(String unionName);

	Integer deleteUnionByTicketId(Integer ticketId);
}
