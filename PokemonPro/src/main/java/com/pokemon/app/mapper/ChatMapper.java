package com.pokemon.app.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.pokemon.app.model.db.ChatDao;
import com.pokemon.app.model.db.request.ChatRequest;

@Mapper
public interface ChatMapper {
	List<ChatDao> selectChatByTicketId(Integer ticketId);

	void insertChat(ChatRequest request);

	Integer tempDeleteChat();

	Integer deleteChat();

}
