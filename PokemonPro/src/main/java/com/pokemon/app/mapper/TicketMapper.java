package com.pokemon.app.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.pokemon.app.model.db.request.TicketRequest;

@Mapper
public interface TicketMapper {

	boolean insertTicket(TicketRequest request);

	Long nextTicketId();

	Integer tempDeleteTicket();

	Integer deleteTicket();

	Integer deleteTicketByTicketId(Integer ticketId);
}
