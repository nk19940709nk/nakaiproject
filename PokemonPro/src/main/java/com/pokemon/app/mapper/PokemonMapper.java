package com.pokemon.app.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.pokemon.app.model.db.PokemonDao;
import com.pokemon.app.model.db.PokemonEvoDao;
import com.pokemon.app.model.db.PokemonSkillDao;
import com.pokemon.app.model.db.PokemonTechDao;

@Mapper
public interface PokemonMapper {
	List<PokemonDao> selectPokemonAll();

	PokemonDao selectPokemonByPokemonId(Integer pokemonId);

	List<PokemonSkillDao> selectSkillByPokemonId(Integer pokemonId);

	List<PokemonEvoDao> selectEvoByPokemonId(Integer pokemonId);

	List<PokemonTechDao> selectTechByPokemonId(Integer pokemonId);

}
