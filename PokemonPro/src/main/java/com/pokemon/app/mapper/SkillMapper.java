package com.pokemon.app.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.pokemon.app.model.db.SkillDao;

@Mapper
public interface SkillMapper {

	List<SkillDao> selectSkillAll();
}
