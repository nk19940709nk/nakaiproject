package com.pokemon.app.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.pokemon.app.model.db.TechDao;

@Mapper
public interface TechMapper {

	List<TechDao> selectTechAll();

}
