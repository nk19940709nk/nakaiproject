package com.pokemon.app.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.pokemon.app.model.db.RaidDao;
import com.pokemon.app.model.db.request.RaidRequest;

@Mapper
public interface RaidMapper {

	void insertRaid(RaidRequest request);

	List<RaidDao> selectRaid(String username);

	RaidDao selectRaidByTicketId(Integer ticketId);

	List<RaidDao> selectRaidAll();

	Integer tempDeleteRaid();

	Integer deleteRaid();

	Integer countRaidByName(String raidName);

	Integer deleteRaidByTicketId(Integer ticketId);

}
