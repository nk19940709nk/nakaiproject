package com.pokemon.app.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.pokemon.app.model.db.ExchangeDao;
import com.pokemon.app.model.db.request.ExchangeRequest;

@Mapper
public interface ExchangeMapper {

	void insertExchange(ExchangeRequest request);

	List<ExchangeDao> selectExchange(String username);

	ExchangeDao selectExchangeByTicketId(Integer ticketId);

	List<ExchangeDao> selectExchangeAll();

	Integer tempDeleteExchange();

	Integer deleteExchange();

	Integer deleteExchangeByTicketId(Integer ticketId);

	Integer countExchangeByName(String pokemonName);
}
