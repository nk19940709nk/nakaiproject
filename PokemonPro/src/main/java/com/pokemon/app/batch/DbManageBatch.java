package com.pokemon.app.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.pokemon.app.service.DbAccessService;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class DbManageBatch {

	private final DbAccessService dbAccessService;
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	// cronは秒、分、時、日、月、曜日 を指定することが可能。曜日は（0,1,2,3,4,5,6,7が日～日を表す。※日は2つある）
//	@Scheduled(cron = "0 31 * * * *", zone = "Asia/Tokyo")
	public void deleteMatchingRecords() {
//		try {
//			Integer deleteChatCount = dbAccessService.tempDeleteChat();
//			Integer deleteExchangeCount = dbAccessService.tempDeleteExchange();
//			Integer deleteUnionCount = dbAccessService.tempDeleteUnion();
//			Integer deleteRaidCount = dbAccessService.tempDeleteRaid();
//			if (deleteChatCount > 0) {
//				logger.info(String.format("チャット削除件数:%d", deleteChatCount));
//				dbAccessService.deleteChat();
//			}
//			if (deleteExchangeCount > 0) {
//				logger.info(String.format("交換削除件数:%d", deleteExchangeCount));
//				dbAccessService.deleteExchange();
//			}
//			if (deleteUnionCount > 0) {
//				logger.info(String.format("ユニオン削除件数:%d", deleteUnionCount));
//				dbAccessService.deleteUnion();
//			}
//			if (deleteRaidCount > 0) {
//				logger.info(String.format("レイド削除件数:%d", deleteRaidCount));
//				dbAccessService.deleteRaid();
//			}
//		} catch (Exception e) {
//			logger.error(e.toString());
//		}
	}

//	@Scheduled(cron = "0 12 * * * *", zone = "Asia/Tokyo")
	public void deleteTicketRecords() {
//		try {
//			Integer deletedTicketCount = dbAccessService.deleteTicket();
//			if (deletedTicketCount > 0) {
//				logger.info(String.format("レイド削除件数:%d", deletedTicketCount));
//				dbAccessService.deleteTicket();
//			}
//		} catch (Exception e) {
//			logger.error(e.toString());
//		}
	}
}
