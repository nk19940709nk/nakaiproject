package com.pokemon.app.model.db;

import java.util.List;

import lombok.Data;

@Data
public class PokemonDao {
	private Integer pokemonId;
	private String pokemonName;
	private Integer pokemonStatusH;
	private Integer pokemonStatusA;
	private Integer pokemonStatusB;
	private Integer pokemonStatusC;
	private Integer pokemonStatusD;
	private Integer pokemonStatusS;
	private Integer pokemonStatusAll;
	private Integer pokemonEffort;
	private String mainType;
	private String subType;
	private List<PokemonSkillDao> pokemonSkillList;
	private List<PokemonTechDao> pokemonTechList;
	private List<PokemonEvoDao> pokemonEvoList;
}
