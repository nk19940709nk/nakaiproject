package com.pokemon.app.model.line;

import java.util.List;

import lombok.Data;

@Data
public class LineObjMessages {
	private List<LineObjMessageDetails> messages;
}
