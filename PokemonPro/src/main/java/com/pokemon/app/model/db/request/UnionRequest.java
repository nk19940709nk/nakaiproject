package com.pokemon.app.model.db.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class UnionRequest {

	@JsonProperty("createUser")
	private String createUser;
	@JsonProperty("unionName")
	private String unionName;
	@JsonProperty("explain")
	private String explain;
	@JsonProperty("ticketId")
	private Long ticketId;
}
