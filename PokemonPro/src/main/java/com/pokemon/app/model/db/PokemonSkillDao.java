package com.pokemon.app.model.db;

import lombok.Data;

@Data
public class PokemonSkillDao {
	private Integer pokemonId;
	private String skillName;
	private String skillExplain;
	private boolean skillDreamFlg;

}
