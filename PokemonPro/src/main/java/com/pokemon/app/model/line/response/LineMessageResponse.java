package com.pokemon.app.model.line.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class LineMessageResponse {

	@JsonProperty("destination")
	private String destination;

	@JsonProperty("events")
	private List<Events> events;

	@Data
	public static class Events {
		@JsonProperty("type")
		private String type;
		@JsonProperty("message")
		private Message message;
		@JsonProperty("webhookEventId")
		private String webhookEventId;
		@JsonProperty("deliveryContext")
		private DeliveryContext deliveryContext;
		@JsonProperty("timestamp")
		private Long timestamp;
		@JsonProperty("source")
		private Source source;
		@JsonProperty("replyToken")
		private String replyToken;
		@JsonProperty("mode")
		private String mode;

		@Data
		public static class Message {
			@JsonProperty("id")
			private String id;
			@JsonProperty("type")
			private String type;
			@JsonProperty("text")
			private String text;
		}

		@Data
		public static class DeliveryContext {
			@JsonProperty("isRedelivery")
			private boolean isRedelivery;
		}

		@Data
		public static class Source {
			@JsonProperty("type")
			private String type;
			@JsonProperty("userId")
			private String userId;
		}

	}
}
