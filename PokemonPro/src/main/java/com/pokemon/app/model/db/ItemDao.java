package com.pokemon.app.model.db;

import lombok.Data;

@Data
public class ItemDao {

	private Integer itemId;
	private String itemName;
	private String itemExplain;
}
