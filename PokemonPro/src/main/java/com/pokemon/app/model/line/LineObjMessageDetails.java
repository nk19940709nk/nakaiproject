package com.pokemon.app.model.line;

import lombok.Data;

@Data
public class LineObjMessageDetails {
	/** メッセージ種別 */
	private String type;
	/** メッセージ */
	private String text;
}
