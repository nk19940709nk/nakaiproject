package com.pokemon.app.model.db;

import lombok.Data;

@Data
public class SkillIdDao {

	private Integer skillId;
	private String pokemonName;
}
