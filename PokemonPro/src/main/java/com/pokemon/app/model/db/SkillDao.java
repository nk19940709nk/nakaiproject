package com.pokemon.app.model.db;

import lombok.Data;

@Data
public class SkillDao {
	private Integer skillId;
	private String skillName;
	private String skillExplain;
	private String pokemonName;
}
