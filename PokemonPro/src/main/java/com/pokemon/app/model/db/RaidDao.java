package com.pokemon.app.model.db;

import java.util.Date;

import lombok.Data;

@Data
public class RaidDao {
	private String raidName;
	private String raidLevel;
	private String explain;
	private Date limitDate;
	private Long ticketId;
	private String createUser;
}
