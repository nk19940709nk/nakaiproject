package com.pokemon.app.model.db;

import java.util.Date;

import lombok.Data;

@Data
public class UnionDao {
	private String unionName;
	private Date limitDate;
	private String explain;
	private Long ticketId;
	private String createUser;
}
