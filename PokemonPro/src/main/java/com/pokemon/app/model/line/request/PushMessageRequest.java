package com.pokemon.app.model.line.request;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class PushMessageRequest {
	private String to;
	private List<Message> messages;

	/**
	 * プッシュメッセージリクエストコンストラクタ
	 * 
	 * @param userId
	 * @param imageUrl
	 * @param titleText
	 * @param actionPath
	 */
	public PushMessageRequest(String userId, List<MessagePartContent> messagePartContentList) {
		MessageContent messageContent = new MessageContent(messagePartContentList);

		Message message = new Message() {
			{
				setContents(messageContent);
			}
		};
		List<Message> messageList = new ArrayList<>();
		messageList.add(message);

		this.setTo(userId);
		this.setMessages(messageList);

	}

	@Data
	public static class Message {
		private String type = "flex";
		private String altText = "ポケモン情報";
		private MessageContent contents;
	}

	@Data
	public static class MessageContent {
		private String type = "carousel";
		private List<MessagePartContent> contents = new ArrayList<>();

		public MessageContent(List<MessagePartContent> messagePartContentList) {
			this.setContents(messagePartContentList);
		}
	}

	@Data
	public static class MessagePartContent {
		private String type = "bubble";
		private Hero hero;
		private Body body;
		private Footer footer;

		public MessagePartContent(String imageUrl, String titleText, String actionPath, String buttonText) {

			Hero hero = new Hero() {
				{
					setUrl(imageUrl);
				}
			};

			List<BodyContent> bodyContentList = new ArrayList<>();
			BodyContent bodyContent = new BodyContent() {
				{
					setText(titleText);
				}
			};
			bodyContentList.add(bodyContent);
			Body body = new Body() {
				{
					setContents(bodyContentList);
				}
			};

			List<FooterContent> footerContentList = new ArrayList<>();
			FooterContentAction action = new FooterContentAction() {
				{
					setUri(actionPath);
					setLabel(buttonText);
				}
			};
			FooterContent footerContent = new FooterContent() {
				{
					setAction(action);
				}
			};
			footerContentList.add(footerContent);
			Footer footer = new Footer() {
				{
					setContents(footerContentList);
				}
			};

			this.setHero(hero);
			this.setBody(body);
			this.setFooter(footer);
		}

	}

	@Data
	public static class Hero {
		private String type = "image";
		private String url;
		private String size = "full";
		private String aspectRatio = "20:13";
		private String aspectMode = "cover";
	}

	@Data
	public static class Body {
		private String type = "box";
		private String layout = "vertical";
		private List<BodyContent> contents;

	}

	@Data
	public static class BodyContent {
		private String type = "text";
		private String text;
		private String weight = "bold";
		private String size = "xl";
	}

	@Data
	public static class Footer {
		private String type = "box";
		private String layout = "vertical";
		private String spacing = "sm";
		private List<FooterContent> contents;
	}

	@Data
	public static class FooterContent {
		private String type = "button";
		private String style = "primary";
		private String height = "sm";
		private FooterContentAction action;
	}

	@Data
	public static class FooterContentAction {
		private String type = "uri";
		private String label;
		private String uri;
	}

}
