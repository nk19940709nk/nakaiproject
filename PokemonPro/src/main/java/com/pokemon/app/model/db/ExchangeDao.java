package com.pokemon.app.model.db;

import java.util.Date;

import lombok.Data;

@Data
public class ExchangeDao {
	private String pokemonName;
	private Date limitDate;
	private boolean statusH;
	private boolean statusA;
	private boolean statusB;
	private boolean statusC;
	private boolean statusD;
	private boolean statusS;
	private String color;
	private String dream;
	private String explain;
	private Long ticketId;
	private String createUser;

}
