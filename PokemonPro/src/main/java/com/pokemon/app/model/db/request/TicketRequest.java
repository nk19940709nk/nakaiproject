package com.pokemon.app.model.db.request;

import java.util.Date;

import lombok.Data;

@Data
public class TicketRequest {

	private Long ticketId;
	private Date limitDate;
	private String createUser;
}
