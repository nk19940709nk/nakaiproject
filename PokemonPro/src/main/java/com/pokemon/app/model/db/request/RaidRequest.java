package com.pokemon.app.model.db.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class RaidRequest {

	@JsonProperty("createUser")
	private String createUser;
	@JsonProperty("raidName")
	private String raidName;
	@JsonProperty("raidLevel")
	private String raidLevel;
	@JsonProperty("explain")
	private String explain;
	@JsonProperty("ticketId")
	private Long ticketId;
}
