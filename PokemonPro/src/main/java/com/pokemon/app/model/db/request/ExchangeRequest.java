package com.pokemon.app.model.db.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ExchangeRequest {
	@JsonProperty("createUser")
	private String createUser;
	@JsonProperty("pokemonName")
	private String pokemonName;
	@JsonProperty("statusH")
	private Boolean statusH;
	@JsonProperty("statusA")
	private Boolean statusA;
	@JsonProperty("statusB")
	private Boolean statusB;
	@JsonProperty("statusC")
	private Boolean statusC;
	@JsonProperty("statusD")
	private Boolean statusD;
	@JsonProperty("statusS")
	private Boolean statusS;
	@JsonProperty("color")
	private String color;
	@JsonProperty("dream")
	private String dream;
	@JsonProperty("explain")
	private String explain;
	private Long ticketId;

}
