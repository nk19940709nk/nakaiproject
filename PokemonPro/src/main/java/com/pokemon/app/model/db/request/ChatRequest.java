package com.pokemon.app.model.db.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ChatRequest {

	@JsonProperty("chatUsername")
	private String chatUsername;
	@JsonProperty("chatContent")
	private String chatContent;
	@JsonProperty("chatGenre")
	private Integer chatGenre;
	@JsonProperty("ticketId")
	private Integer ticketId;
}
