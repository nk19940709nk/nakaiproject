package com.pokemon.app.model.db;

import lombok.Data;

@Data
public class PokemonEvoDao {
	private Integer pokemonId;
	private Integer pokemonEvoId;
	private String evoName;
	private String evoConditions;

}
