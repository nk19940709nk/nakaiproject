package com.pokemon.app.model.db;

import java.sql.Date;

import lombok.Data;

@Data
public class ChatDao {

	private Integer chatId;
	private String chatUsername;
	private String chatContent;
	private Date chatDate;
	private String chatGenreName;
}
