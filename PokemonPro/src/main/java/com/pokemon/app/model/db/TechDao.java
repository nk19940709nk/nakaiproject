package com.pokemon.app.model.db;

import lombok.Data;

@Data
public class TechDao {
	private Integer techId;
	private String techName;
	private String techExplain;
	private String pokemonTechMethod;
	private Integer techDamage;
	private Integer techHit;
	private Integer techPp;
	private String typeName;
	private String techGenreName;
}
