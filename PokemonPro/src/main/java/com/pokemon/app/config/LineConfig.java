package com.pokemon.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LineConfig {

	@Bean
	public LineBean lineBean() {
		return new LineBean();
	}
}
