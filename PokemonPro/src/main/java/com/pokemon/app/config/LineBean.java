package com.pokemon.app.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
@ConfigurationProperties(prefix = "line")
public class LineBean {

	private String channelAccessToken;
}
