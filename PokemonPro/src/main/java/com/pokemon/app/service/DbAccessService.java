package com.pokemon.app.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pokemon.app.mapper.ChatMapper;
import com.pokemon.app.mapper.ExchangeMapper;
import com.pokemon.app.mapper.ItemMapper;
import com.pokemon.app.mapper.PokemonMapper;
import com.pokemon.app.mapper.RaidMapper;
import com.pokemon.app.mapper.SkillMapper;
import com.pokemon.app.mapper.TechMapper;
import com.pokemon.app.mapper.TicketMapper;
import com.pokemon.app.mapper.UnionMapper;
import com.pokemon.app.model.db.ChatDao;
import com.pokemon.app.model.db.ExchangeDao;
import com.pokemon.app.model.db.ItemDao;
import com.pokemon.app.model.db.PokemonDao;
import com.pokemon.app.model.db.PokemonEvoDao;
import com.pokemon.app.model.db.PokemonSkillDao;
import com.pokemon.app.model.db.PokemonTechDao;
import com.pokemon.app.model.db.RaidDao;
import com.pokemon.app.model.db.SkillDao;
import com.pokemon.app.model.db.TechDao;
import com.pokemon.app.model.db.UnionDao;
import com.pokemon.app.model.db.request.ChatRequest;
import com.pokemon.app.model.db.request.ExchangeRequest;
import com.pokemon.app.model.db.request.RaidRequest;
import com.pokemon.app.model.db.request.TicketRequest;
import com.pokemon.app.model.db.request.UnionRequest;

import io.micrometer.common.util.StringUtils;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DbAccessService {

	private final PokemonMapper pokemonMapper;
	private final ItemMapper itemMapper;
	private final SkillMapper skillMapper;
	private final TechMapper techMapper;
	private final ChatMapper chatMapper;
	private final ExchangeMapper exchangeMapper;
	private final TicketMapper ticketMapper;
	private final UnionMapper unionMapper;
	private final RaidMapper raidMapper;

	/**
	 * ポケモン
	 */
	@Transactional(rollbackForClassName = { "Exception" })
	public List<PokemonDao> getPokemonAll() {
		return pokemonMapper.selectPokemonAll();
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public List<PokemonSkillDao> getSkillByPokemonId(Integer pokemonId) {
		return pokemonMapper.selectSkillByPokemonId(pokemonId);
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public List<PokemonEvoDao> getEvoByPokemonId(Integer pokemonId) {
		return pokemonMapper.selectEvoByPokemonId(pokemonId);
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public List<PokemonTechDao> getTechByPokemonId(Integer pokemonId) {
		return pokemonMapper.selectTechByPokemonId(pokemonId);
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public PokemonDao getPokemonByPokemonId(Integer pokemonId) {
		return pokemonMapper.selectPokemonByPokemonId(pokemonId);
	}

	/**
	 * わざ
	 */
	@Transactional(rollbackForClassName = { "Exception" })
	public List<TechDao> getTechAll() {
		return techMapper.selectTechAll();
	}

	/**
	 * とくせい
	 */
	@Transactional(rollbackForClassName = { "Exception" })
	public List<SkillDao> getSkillAll() {
		return skillMapper.selectSkillAll();
	}

	/**
	 * アイテム
	 */
	@Transactional(rollbackForClassName = { "Exception" })
	public List<ItemDao> getItemAll() {
		return itemMapper.selectItemAll();
	}

	/**
	 * チャット
	 */
	@Transactional(rollbackForClassName = { "Exception" })
	public List<ChatDao> getChatByTicketId(Integer ticketId) {
		List<ChatDao> chatDaoList = new ArrayList<>();
		if (ticketId != null) {
			chatDaoList = chatMapper.selectChatByTicketId(ticketId);
		}
		return chatDaoList;
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public void postChat(ChatRequest request) {
		if (StringUtils.isNotEmpty(request.getChatUsername()) && StringUtils.isNotEmpty(request.getChatContent())
				&& request.getChatGenre() != null && request.getTicketId() != null) {
			chatMapper.insertChat(request);
		}
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public Integer deleteChat() {
		return chatMapper.deleteChat();
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public Integer tempDeleteChat() {
		return chatMapper.tempDeleteChat();
	}

	/**
	 * チケット
	 */
	@Transactional(rollbackForClassName = { "Exception" })
	public boolean postTicket(TicketRequest request) {
		if (request.getTicketId() != null && request.getLimitDate() != null
				&& StringUtils.isNotEmpty(request.getCreateUser())) {
			return ticketMapper.insertTicket(request);
		} else {
			return false;
		}
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public Long getNextTicketId() {
		return ticketMapper.nextTicketId();
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public Integer tempDeleteTicket() {
		return ticketMapper.tempDeleteTicket();
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public Integer deleteTicket() {
		return ticketMapper.deleteTicket();
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public Integer deleteTicketByTicketId(Integer ticketId) {
		return ticketMapper.deleteTicketByTicketId(ticketId);
	}

	/**
	 * 交換
	 */
	@Transactional(rollbackForClassName = { "Exception" })
	public void postExchange(ExchangeRequest request) {
		exchangeMapper.insertExchange(request);
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public List<ExchangeDao> getExchangeByName(String username) {
		return exchangeMapper.selectExchange(username);
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public ExchangeDao getExchangeByTicketId(Integer ticketId) {
		return exchangeMapper.selectExchangeByTicketId(ticketId);
	}

	public List<ExchangeDao> getExchangeAll() {
		return exchangeMapper.selectExchangeAll();
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public Integer tempDeleteExchange() {
		return exchangeMapper.tempDeleteExchange();
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public Integer deleteExchange() {
		return exchangeMapper.deleteExchange();
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public Integer countExchangeByName(String pokemonName) {
		return exchangeMapper.countExchangeByName(pokemonName);
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public Integer deleteExchangeByTicketId(Integer ticketId) {
		return exchangeMapper.deleteExchangeByTicketId(ticketId);
	}

	/**
	 * ユニオン
	 */
	@Transactional(rollbackForClassName = { "Exception" })
	public void postUnion(UnionRequest request) {
		if (request.getTicketId() != null && StringUtils.isNotEmpty(request.getCreateUser())
				&& StringUtils.isNotEmpty(request.getExplain()) && StringUtils.isNotEmpty(request.getUnionName())) {
			unionMapper.insertUnion(request);
		}
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public List<UnionDao> getUnionByName(String username) {
		return unionMapper.selectUnion(username);
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public UnionDao getUnionByTicketId(Integer ticketId) {
		return unionMapper.selectUnionByTicketId(ticketId);
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public List<UnionDao> getUnionAll() {
		return unionMapper.selectUnionAll();
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public Integer tempDeleteUnion() {
		return unionMapper.tempDeleteUnion();
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public Integer deleteUnion() {
		return unionMapper.deleteUnion();
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public Integer countUnionByName(String unionName) {
		return unionMapper.countUnionByName(unionName);
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public Integer deleteUnionByTicketId(Integer ticketId) {
		return unionMapper.deleteUnionByTicketId(ticketId);
	}

	/**
	 * レイド
	 */
	@Transactional(rollbackForClassName = { "Exception" })
	public void postRaid(RaidRequest request) {
		if (request.getTicketId() != null && StringUtils.isNotEmpty(request.getCreateUser())
				&& StringUtils.isNotEmpty(request.getRaidLevel()) && StringUtils.isNotEmpty(request.getRaidName())) {
			raidMapper.insertRaid(request);
		}
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public List<RaidDao> getRaidByName(String username) {
		return raidMapper.selectRaid(username);
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public RaidDao getRaidByTicketId(Integer ticketId) {
		return raidMapper.selectRaidByTicketId(ticketId);
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public List<RaidDao> getRaidAll() {
		return raidMapper.selectRaidAll();
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public Integer tempDeleteRaid() {
		return raidMapper.tempDeleteRaid();
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public Integer deleteRaid() {
		return raidMapper.deleteRaid();
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public Integer countRaidByName(String raidName) {
		return raidMapper.countRaidByName(raidName);
	}

	@Transactional(rollbackForClassName = { "Exception" })
	public Integer deleteRaidByTicketId(Integer ticketId) {
		return raidMapper.deleteRaidByTicketId(ticketId);
	}

}
