package com.pokemon.app.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pokemon.app.model.db.ItemDao;
import com.pokemon.app.service.DbAccessService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(value = "/item")
@RequiredArgsConstructor
public class ItemController extends BaseController {
	private final DbAccessService dbAccessService;

	@GetMapping(value = "/all")
	public List<ItemDao> getItemAll() {
		return dbAccessService.getItemAll();
	}
}
