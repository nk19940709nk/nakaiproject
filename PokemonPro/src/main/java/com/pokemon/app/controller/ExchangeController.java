package com.pokemon.app.controller;

import java.util.Date;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pokemon.app.model.db.ExchangeDao;
import com.pokemon.app.model.db.request.ExchangeRequest;
import com.pokemon.app.model.db.request.TicketRequest;
import com.pokemon.app.service.DbAccessService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(value = "/exchange")
@RequiredArgsConstructor
public class ExchangeController extends BaseController {

	private final DbAccessService dbAccessService;

	@PostMapping(value = "/regist")
	public Long postExchange(@RequestBody ExchangeRequest request) {
		Long ticketId = dbAccessService.getNextTicketId();
		TicketRequest ticketRequest = new TicketRequest();
		if (ticketId != null && ticketId > 0) {
			ticketRequest.setLimitDate(new Date());
			ticketRequest.setTicketId(ticketId);
			ticketRequest.setCreateUser(request.getCreateUser());
			request.setTicketId(ticketId);
			if (dbAccessService.postTicket(ticketRequest)) {
				dbAccessService.postExchange(request);
			}
		}
		return ticketId;
	}

	@GetMapping(value = "/myself/{username}")
	public List<ExchangeDao> getExchangeByName(@PathVariable("username") String username) {
		return dbAccessService.getExchangeByName(username);
	}

	@GetMapping(value = "/all")
	public List<ExchangeDao> getExchangeAll() {
		return dbAccessService.getExchangeAll();
	}

	@GetMapping(value = "/{ticketId}")
	public ExchangeDao getExchangeByTicketId(@PathVariable("ticketId") Integer ticketId) {
		return dbAccessService.getExchangeByTicketId(ticketId);
	}

	@PutMapping(value = "/{ticketId}")
	public void deleteExchangeByTicketId(@PathVariable("ticketId") Integer ticketId) {
		Integer deleteTicketCount = dbAccessService.deleteTicketByTicketId(ticketId);
		if (deleteTicketCount > 0) {
			dbAccessService.deleteTicketByTicketId(ticketId);
		}
	}
}
