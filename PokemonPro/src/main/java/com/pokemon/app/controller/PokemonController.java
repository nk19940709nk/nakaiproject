package com.pokemon.app.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pokemon.app.model.db.PokemonDao;
import com.pokemon.app.model.db.PokemonEvoDao;
import com.pokemon.app.model.db.PokemonSkillDao;
import com.pokemon.app.model.db.PokemonTechDao;
import com.pokemon.app.service.DbAccessService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(value = "/pokemon")
@RequiredArgsConstructor
public class PokemonController extends BaseController {
	private final DbAccessService dbAccessService;

	@GetMapping(value = "/all")
	public List<PokemonDao> getPokemonAll() {
		return dbAccessService.getPokemonAll();
	}

	@GetMapping(value = "/{pokemonId}")
	public PokemonDao getPokemonByPokemonId(@PathVariable("pokemonId") Integer pokemonId) {
		return dbAccessService.getPokemonByPokemonId(pokemonId);
	}

	@GetMapping(value = "/skill/{pokemonId}")
	public List<PokemonSkillDao> getPokemonSkill(@PathVariable("pokemonId") Integer pokemonId) {
		return dbAccessService.getSkillByPokemonId(pokemonId);
	}

	@GetMapping(value = "/evo/{pokemonId}")
	public List<PokemonEvoDao> getPokemonEvo(@PathVariable("pokemonId") Integer pokemonId) {
		return dbAccessService.getEvoByPokemonId(pokemonId);
	}

	@GetMapping(value = "/tech/{pokemonId}")
	public List<PokemonTechDao> getPokemonTech(@PathVariable("pokemonId") Integer pokemonId) {
		return dbAccessService.getTechByPokemonId(pokemonId);
	}

}
