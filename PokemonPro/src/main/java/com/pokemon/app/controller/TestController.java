package com.pokemon.app.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pokemon.app.config.LineBean;
import com.pokemon.app.model.line.request.PushMessageRequest;
import com.pokemon.app.model.line.request.PushMessageRequest.MessagePartContent;
import com.pokemon.app.model.line.response.LineMessageResponse;
import com.pokemon.app.service.DbAccessService;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@Slf4j
public class TestController {

	private final LineBean lineBean;
	private final ObjectMapper objectMapper;
	private final DbAccessService dbAccessService;

	@GetMapping(value = "/test")
	public void test() {
		System.out.println("test");
		System.out.println(lineBean.getChannelAccessToken());
	}

	@PostMapping(value = "/test")
	public ResponseEntity<?> postTest(HttpServletRequest request, @RequestBody String requestBody)
			throws Exception, NoSuchAlgorithmException, UnsupportedEncodingException, InvalidKeyException,
			JsonMappingException, JsonProcessingException {

		try {
			// 送信元検証
			boolean inspectionResult = checkSignature(request.getHeader("x-line-signature"), requestBody);

			if (inspectionResult) {
				LineMessageResponse lineMessageResponse = objectMapper.readValue(requestBody,
						LineMessageResponse.class);
				String userId = lineMessageResponse.getEvents().get(0).getSource().getUserId();
				String messageText = lineMessageResponse.getEvents().get(0).getMessage().getText();
				String imagePath = "https://www.pokemon.co.jp/ex/sv/assets/img/pokemon/220227_01/img_01.jpg";

				List<MessagePartContent> messagePartContentList = new ArrayList<>();
				// ポケモン交換カード追加
				if (dbAccessService.countExchangeByName(messageText) > 0) {

					String actionUrl = "https://pokemonapp-bc196.web.app/exchange/" + messageText;
					MessagePartContent messagePartContent = new MessagePartContent(imagePath, "ポケモン交換", actionUrl,
							"OPEN");
					messagePartContentList.add(messagePartContent);
				}
				// ポケモンレイドカード追加
				if (dbAccessService.countRaidByName(messageText) > 0) {
					String actionUrl = "https://pokemonapp-bc196.web.app/raid/" + messageText;
					MessagePartContent messagePartContent = new MessagePartContent(imagePath, "ポケモンレイド", actionUrl,
							"OPEN");
					messagePartContentList.add(messagePartContent);
				}
				// ポケモンユニオンカード追加
				if (dbAccessService.countUnionByName(messageText) > 0) {
					String actionUrl = "https://pokemonapp-bc196.web.app/union/" + messageText;
					MessagePartContent messagePartContent = new MessagePartContent(imagePath, "ユニオンサークル", actionUrl,
							"OPEN");
					messagePartContentList.add(messagePartContent);
				}
				PushMessageRequest pushMessageRequest = new PushMessageRequest(userId, messagePartContentList);

				sendMessage(pushMessageRequest);
			} else {
				// ControllerAdviceでExceptonHandlerを設定する
				throw new Exception();
			}
		} catch (Exception e) {

		}

		return ResponseEntity.status(HttpStatus.OK).build();
	}

	/**
	 * LINEメッセージ送信元検証
	 * 
	 * @param xLineSignature リクエストヘッダー
	 * @param requestBody    リクエストボディ
	 * @return 検証結果
	 */
	public boolean checkSignature(String xLineSignature, String requestBody) {
		String channelSecret = "92f34cfda5bbbadd349d0e17e05b8d04";
		String signature = "";
		try {
			SecretKeySpec key = new SecretKeySpec(channelSecret.getBytes(), "HmacSHA256");
			Mac mac = Mac.getInstance("HmacSHA256");
			mac.init(key);
			byte[] source = requestBody.getBytes("UTF-8");
			signature = Base64.getEncoder().encodeToString(mac.doFinal(source));

		} catch (Exception e) {
			log.error(e.toString());
		}
		return signature.equals(xLineSignature);
	}

	public void sendMessage(PushMessageRequest pushMessageRequest) throws JsonProcessingException {
		try {

			String lineApiKey = lineBean.getChannelAccessToken();
			String jsonAsString = objectMapper.writeValueAsString(pushMessageRequest);

			// リクエスト先URLを設定
			HttpPost httpPost = new HttpPost("https://api.line.me/v2/bot/message/push");

			// ヘッダー設定
			httpPost.setHeader("Content-Type", "application/json");
			httpPost.setHeader("Authorization", "Bearer " + lineApiKey);

			// body設定
			StringEntity params = new StringEntity(jsonAsString, StandardCharsets.UTF_8);
			httpPost.setEntity(params);

			// リクエスト実行
			CloseableHttpClient client = HttpClients.createDefault();
			CloseableHttpResponse resp = client.execute(httpPost);
			int statusCode = resp.getStatusLine().getStatusCode();

			if (statusCode == 200) {
				System.out.println("OK");
			} else {
				System.out.println("NG");
			}
		} catch (IllegalArgumentException | IOException e) {
			log.error(e.toString());
		}
	}
}
