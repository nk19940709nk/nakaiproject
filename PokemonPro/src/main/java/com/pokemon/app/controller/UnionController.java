package com.pokemon.app.controller;

import java.util.Date;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pokemon.app.model.db.UnionDao;
import com.pokemon.app.model.db.request.TicketRequest;
import com.pokemon.app.model.db.request.UnionRequest;
import com.pokemon.app.service.DbAccessService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(value = "/union")
@RequiredArgsConstructor
public class UnionController extends BaseController {

	private final DbAccessService dbAccessService;

	@PostMapping(value = "/regist")
	public void postExchange(@RequestBody UnionRequest request) {
		Long ticketId = dbAccessService.getNextTicketId();
		if (ticketId != null && ticketId > 0) {
			TicketRequest ticketRequest = new TicketRequest() {
				{
					setLimitDate(new Date());
					setTicketId(ticketId);
					setCreateUser(request.getCreateUser());
				}
			};
			request.setTicketId(ticketId);
			if (dbAccessService.postTicket(ticketRequest)) {
				dbAccessService.postUnion(request);
			}
		}
	}

	@GetMapping(value = "/myself/{username}")
	public List<UnionDao> getUnionByName(@PathVariable("username") String username) {
		return dbAccessService.getUnionByName(username);
	}

	@GetMapping(value = "/all")
	public List<UnionDao> getUnionAll() {
		return dbAccessService.getUnionAll();
	}

	@GetMapping(value = "/{ticketId}")
	public UnionDao getUnionByTicketId(@PathVariable("ticketId") Integer ticketId) {
		return dbAccessService.getUnionByTicketId(ticketId);
	}

	@PutMapping(value = "/{ticketId}")
	public void deleteUnionByTicketId(@PathVariable("ticketId") Integer ticketId) {
		Integer deleteTicketCount = dbAccessService.deleteTicketByTicketId(ticketId);
		if (deleteTicketCount > 0) {
			dbAccessService.deleteUnionByTicketId(ticketId);
		}
	}
}
