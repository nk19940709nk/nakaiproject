package com.pokemon.app.controller;

import java.util.Date;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pokemon.app.model.db.RaidDao;
import com.pokemon.app.model.db.request.RaidRequest;
import com.pokemon.app.model.db.request.TicketRequest;
import com.pokemon.app.service.DbAccessService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(value = "/raid")
@RequiredArgsConstructor
public class RaidController extends BaseController {

	private final DbAccessService dbAccessService;

	@PostMapping(value = "/regist")
	public void postExchange(@RequestBody RaidRequest request) {
		Long ticketId = dbAccessService.getNextTicketId();
		TicketRequest ticketRequest = new TicketRequest();
		if (ticketId != null && ticketId > 0) {
			ticketRequest.setLimitDate(new Date());
			ticketRequest.setCreateUser(request.getCreateUser());
			ticketRequest.setTicketId(ticketId);
			request.setTicketId(ticketId);
			if (dbAccessService.postTicket(ticketRequest)) {
				dbAccessService.postRaid(request);
			}
		}
	}

	@GetMapping(value = "/myself/{username}")
	public List<RaidDao> getRaidByName(@PathVariable("username") String username) {
		return dbAccessService.getRaidByName(username);
	}

	@GetMapping(value = "/all")
	public List<RaidDao> getRaidAll() {
		return dbAccessService.getRaidAll();
	}

	@GetMapping(value = "/{ticketId}")
	public RaidDao getRaidByTicketId(@PathVariable("ticketId") Integer ticketId) {
		return dbAccessService.getRaidByTicketId(ticketId);
	}

	@PutMapping(value = "/{ticketId}")
	public void deleteRaidByTicketId(@PathVariable("ticketId") Integer ticketId) {
		Integer deleteTicketCount = dbAccessService.deleteTicketByTicketId(ticketId);
		if (deleteTicketCount > 0) {
			dbAccessService.deleteRaidByTicketId(ticketId);
		}
	}
}
