package com.pokemon.app.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pokemon.app.model.db.ChatDao;
import com.pokemon.app.model.db.request.ChatRequest;
import com.pokemon.app.service.DbAccessService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(value = "/chat")
@RequiredArgsConstructor
public class ChatController extends BaseController {
	private final DbAccessService dbAccessService;

	@GetMapping(value = "/{ticketId}")
	public List<ChatDao> getChatById(@PathVariable("ticketId") Integer ticketId) {
		return dbAccessService.getChatByTicketId(ticketId);
	}

	@PostMapping(value = "/")
	public void postChat(@RequestBody ChatRequest request) {
		dbAccessService.postChat(request);
	}

}
