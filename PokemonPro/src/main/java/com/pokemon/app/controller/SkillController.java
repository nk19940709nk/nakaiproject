package com.pokemon.app.controller;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pokemon.app.model.db.SkillDao;
import com.pokemon.app.service.DbAccessService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping(value = "/skill")
@RequiredArgsConstructor
public class SkillController extends BaseController {

	private final DbAccessService dbAccessService;

	@GetMapping(value = "/all")
	public Map<Integer, List<SkillDao>> getSkillAll() {
		List<SkillDao> skillList = dbAccessService.getSkillAll();
		Map<Integer, List<SkillDao>> skillMap = skillList.stream().collect(Collectors.groupingBy(SkillDao::getSkillId));

		return skillMap;
	}
}
