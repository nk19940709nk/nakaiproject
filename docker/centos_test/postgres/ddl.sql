--
-- PostgreSQL database dump
--

-- Dumped from database version 13.9
-- Dumped by pg_dump version 13.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'SJIS';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;


CREATE DATABASE testdb;
\c testdb
--
-- Name: nakai; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA nakai;


ALTER SCHEMA nakai OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: user; Type: TABLE; Schema: nakai; Owner: postgres
--

CREATE TABLE nakai."user" (
    user_id integer,
    user_name character varying(20)
);


ALTER TABLE nakai."user" OWNER TO postgres;

INSERT INTO nakai."user" VALUES (1, 'user1')

--
-- PostgreSQL database dump complete
--

