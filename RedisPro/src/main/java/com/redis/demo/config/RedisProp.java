package com.redis.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@ConfigurationProperties(prefix = "spring.redis")
@Data
public class RedisProp {
    private String host;
    private Integer port;

}
