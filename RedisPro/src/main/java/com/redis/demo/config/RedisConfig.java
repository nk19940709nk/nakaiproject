package com.redis.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisStaticMasterReplicaConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.redis.demo.model.RedisResponse;

import io.lettuce.core.ReadFrom;

@Configuration
public class RedisConfig {

	@Autowired
	private RedisProp redisProp;

	@Bean
	public RedisTemplate<String, RedisResponse> jsonRedisTemplate(RedisConnectionFactory connectionFactory) {
		RedisTemplate<String, RedisResponse> redisTemplate = new RedisTemplate<>();
		redisTemplate.setConnectionFactory(connectionFactory);
		redisTemplate.setKeySerializer(new StringRedisSerializer());
		redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer<>(RedisResponse.class));
		redisTemplate.setHashKeySerializer(redisTemplate.getKeySerializer());
		redisTemplate.setHashValueSerializer(redisTemplate.getValueSerializer());
		return redisTemplate;
	}

	@Bean
	public LettuceConnectionFactory redisConnectionFactory() {
		LettuceClientConfiguration clientConfig = LettuceClientConfiguration.builder()
				.readFrom(ReadFrom.REPLICA_PREFERRED).build();

		RedisStaticMasterReplicaConfiguration serverConfig = new RedisStaticMasterReplicaConfiguration(
				redisProp.getHost(), redisProp.getPort());
		serverConfig.addNode(redisProp.getHost(), redisProp.getPort());

		return new LettuceConnectionFactory(serverConfig, clientConfig);
	}
}
