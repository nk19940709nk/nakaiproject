package com.redis.demo.model;

import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class RedisRequest {

	private String string;
	private List<String> list;
	private Map<String, String> map;
}
