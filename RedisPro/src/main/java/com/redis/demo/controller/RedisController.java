package com.redis.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.redis.demo.model.RedisResponse;
import com.redis.demo.service.RedisService;

@RestController
public class RedisController {

	@Autowired
	private RedisService redisService;

	@GetMapping("/")
	public RedisResponse get() throws Exception {
		RedisResponse redisResponse = new RedisResponse();
		redisResponse.setData("data1");
		redisResponse.setName("name1");

		redisService.saveJsonRedis("testdataA", redisResponse);
		RedisResponse result = redisService.getJsonRedis("testdataA");
		System.out.println(result);

		redisService.saveStringRedis("testdataB", "dataB");
		System.out.println(redisService.getStringRedis("testdataB"));

		return result;
	}
}
