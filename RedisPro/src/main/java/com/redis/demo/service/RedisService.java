package com.redis.demo.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import com.redis.demo.model.RedisResponse;

@Service
public class RedisService {

	@Autowired
	@Qualifier("jsonRedisTemplate")
	private RedisTemplate<String, RedisResponse> jsonRedisTemplate;

	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	public boolean saveJsonRedis(String key, RedisResponse data) {
		try {
			jsonRedisTemplate.opsForValue().set(key, data);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public RedisResponse getJsonRedis(String key) {
		return jsonRedisTemplate.opsForValue().get(key);
	}

	public void deleteJsonRedis(String key) {
		jsonRedisTemplate.delete(key);
	}

	public boolean saveStringRedis(String key, String data) {
		try {
			stringRedisTemplate.opsForValue().set(key, data);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public String getStringRedis(String key) {
		return stringRedisTemplate.opsForValue().get(key);
	}

	public void deleteStringRedis(String key) {
		stringRedisTemplate.delete(key);
	}

	public boolean saveListRedis(String key, List<String> data) {
		try {
			stringRedisTemplate.opsForList().rightPushAll(key, data);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public List<String> getListRedis(String key) {
		return stringRedisTemplate.opsForList().range(key, 0, 0);
	}
	

	public boolean saveMapRedis(String key, Map<String, String> data) {
		try {
			stringRedisTemplate.opsForHash().putAll(key, data);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public Map<String, String> getMapRedis(String key) {
		return stringRedisTemplate.<String, String>opsForHash().entries(key);
	}

}
