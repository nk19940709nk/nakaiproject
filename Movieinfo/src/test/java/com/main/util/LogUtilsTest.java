package com.main.util;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class LogUtilsTest {
    
    @Autowired
    LogUtils logUtils;

    @Test
    public void infoTest() {
        logUtils.info("testInfo");
    }
}
