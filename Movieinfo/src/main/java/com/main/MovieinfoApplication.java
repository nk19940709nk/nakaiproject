package com.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;


@PropertySource("classpath:apiKey.properties")
@EnableScheduling
@SpringBootApplication
public class MovieinfoApplication {
    public static void main(String[] args) {
        SpringApplication.run(MovieinfoApplication.class, args);

    }
}
