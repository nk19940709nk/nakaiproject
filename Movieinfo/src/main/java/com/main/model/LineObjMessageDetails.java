package com.main.model;

import lombok.Data;

@Data
public class LineObjMessageDetails {
    
    /** メッセージ種別 */
    private String type;
    /** メッセージ */
    private String text;

}
