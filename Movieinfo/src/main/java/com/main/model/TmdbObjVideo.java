package com.main.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * 予告動画情報
 */
@Data
public class TmdbObjVideo {
    /** ID */
    @JsonProperty("id")
    private int id;
    
    /** Video詳細情報の多次元配列オブジェクト */
    @JsonProperty("results")
    private List<Object> results;

}
