package com.main.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * 公開間近映画詳細情報
 *
 */
@Data
public class TmdbObjUpcomingResults {
    /** 成人向け */
    @JsonProperty("adult")
    private boolean adult;
    
    /** 背景画像パス */
    @JsonProperty("backdrop_path")
    private String backdrop_path;
    
    /** ジャンルIDリスト */
    @JsonProperty("genre_ids")
    private int[] genre_ids;
    
    /** ID */
    @JsonProperty("id")
    private int id;
    
    /** オリジナル言語 */
    @JsonProperty("original_language")
    private String original_language;
    
    /** オリジナルタイトル */
    @JsonProperty("original_title")
    private String original_title;
    
    /** オーバービュー */
    @JsonProperty("overview")
    private String overview;
    
    /** 人気 */
    @JsonProperty("popularity")
    private double popularity;
    
    /** ポスター画像パス */
    @JsonProperty("poster_path")
    private String poster_path;
    
    /** 公開日 */
    @JsonProperty("release_date")
    private String release_date;
    
    /** タイトル */
    @JsonProperty("title")
    private String title;
    
    /** 予告動画有無 */
    @JsonProperty("video")
    private boolean video;
    
    /** 評価値 */
    @JsonProperty("vote_average")
    private int vote_average;
    
    /** 評価人数 */
    @JsonProperty("vote_count")
    private int vote_count;

}
