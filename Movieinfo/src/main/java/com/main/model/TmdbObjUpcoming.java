package com.main.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * 公開間近映画情報
 *
 */
@Data
public class TmdbObjUpcoming {

    /** 絞り込み日付 */
    @JsonProperty("dates")
    private Object dates;
    
    /** 検索対象ページ数 */
    @JsonProperty("page")
    private int page;
    
    /** 検索結果の多次元配列部オブジェクト */
    @JsonProperty("results")
    private List<Object> results;
    
    /** 検索エ結果合計ページ数 */
    @JsonProperty("total_pages")
    private int total_pages;
    
    /** 検索結果累計 */
    @JsonProperty("total_results")
    private int total_results;

}
