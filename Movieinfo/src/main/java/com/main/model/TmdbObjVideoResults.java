package com.main.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * 予告動画詳細情報
 */
@Data
public class TmdbObjVideoResults {

    /** 文字コード639 */
    @JsonProperty("iso_639_1")
    private String iso_639_1;

    /** 文字コード3166 */
    @JsonProperty("iso_3166_1")
    private String iso_3166_1;

    /** 作曲家 */
    @JsonProperty("name")
    private String name;

    /** youtubeキー */
    @JsonProperty("key")
    private String key;

    /** サイト名 */
    @JsonProperty("site")
    private String site;

    /** 動画サイズ */
    @JsonProperty("size")
    private int size;

    /** 動画タイプ */
    @JsonProperty("type")
    private String type;

    /** 公式有無 */
    @JsonProperty("official")
    private boolean official;
    
    /** 投稿日 */
    @JsonProperty("published_at")
    private String published_at;
    
    /** ID */
    @JsonProperty("id")
    private String id;

}
