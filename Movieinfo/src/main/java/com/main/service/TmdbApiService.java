package com.main.service;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.main.model.TmdbObjUpcoming;
import com.main.model.TmdbObjUpcomingResults;
import com.main.model.TmdbObjVideo;
import com.main.model.TmdbObjVideoResults;
import com.main.util.ApiUtils;
import com.main.util.Const;
import com.main.util.DateUtils;
import com.main.util.LogUtils;

@Service
public class TmdbApiService {
    
    /** オブジェクトマッパー */
    private final ObjectMapper objMapper = new ObjectMapper();
    
    @Autowired
    Environment env;
    @Autowired
    LogUtils logger;
    @Autowired
    DateUtils dateUtils;
    @Autowired
    ApiUtils apiUtils;
    
    /**
     * TMDBApiにアクセスして公開1週間前の映画のリストを取得
     * 
     * @return 映画リスト
     * @throws IOException
     * @throws ParseException
     */
    public List<TmdbObjUpcomingResults> getUpcomingMovie() throws IOException, ParseException {
        // TMDBのApiKeyを取得
        String apiKey = env.getProperty("api.key.tmdb");
        List<TmdbObjUpcomingResults> rtnTmdbObjUpcomingResultsList = new ArrayList<>();

        try {
            URL url = new URL(String.format(Const.TMDB_URL_UPCOMING, apiKey));
            // TMDBApiにアクセスして公開間近の映画情報を取得
            String json = apiUtils.getJson(url);
            // Json→オブジェクト変換（オブジェクトで扱える用にするため）
            TmdbObjUpcoming tmdbObjUpcoming = objMapper.readValue(json, TmdbObjUpcoming.class);
            for (Object TmdbObjUpcomingResultsObj : tmdbObjUpcoming.getResults()) {
                // オブジェクト→Json変換（不規則な多次元配列となっているためJsonに再変換）
                String TmdbObjUpcomingResultsJson = objMapper.writeValueAsString(TmdbObjUpcomingResultsObj);
                // Json→オブジェクト変換（多次元部をオブジェクトとして取り扱うため）
                TmdbObjUpcomingResults tmdbObjUpcomingResults = objMapper.readValue(TmdbObjUpcomingResultsJson,
                        TmdbObjUpcomingResults.class);

                // 公開日をフォーマット
                Date releaseDate = Const.DATE_FORMAT_YYYYMMDD
                        .parse(tmdbObjUpcomingResults.getRelease_date().replace("-", "/"));
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                // 一週間後の日付を取得
                calendar.add(Calendar.DAY_OF_MONTH, 7);
                Date nowDate = calendar.getTime();

                // 公開日が1週間前の場合
                if (dateUtils.compareDateDay(nowDate, releaseDate) == 0) {
                    rtnTmdbObjUpcomingResultsList.add(tmdbObjUpcomingResults);
                }
            }

            return rtnTmdbObjUpcomingResultsList;
        } catch (IOException | ParseException e) {
            logger.error(e.toString());
            throw e;
        }
    }

    /**
     * TMDBApiにアクセスして広告動画のURLを取得
     * 
     * @param movieId TMDBの映画ID
     * @return 広告動画URL
     * @throws IOException
     */
    public String getTMDBVideLink(String movieId) throws IOException {
        // TMDBのApiKeyを取得
        String apiKey = env.getProperty("api.key.tmdb");

        try {
            URL url = new URL(String.format(Const.TMDB_URL_VIDEO, movieId, apiKey));
            // TMDBApiにアクセスして予告動画のJsonデータを取得
            String json = apiUtils.getJson(url);

            // Json→オブジェクト変換（オブジェクトで扱えるように変換）
            TmdbObjVideo tmdbObjVideo = objMapper.readValue(json, TmdbObjVideo.class);
            // オブジェクト→Json変換（多次元配列となっているため、多次元部をJsonに再変換）

            String tmdbObjVideoResultsJson = null;
            // 予告動画が存在しない場合はnullを返却
            if (tmdbObjVideo.getResults().size() == 0) {
                return tmdbObjVideoResultsJson;
            } else {
                tmdbObjVideoResultsJson = objMapper.writeValueAsString(tmdbObjVideo.getResults().get(0));
            }
            // Json→オブジェクト変換（多次元部をオブジェクトに格納）
            TmdbObjVideoResults tmdbObjVideoResults = objMapper.readValue(tmdbObjVideoResultsJson,
                    TmdbObjVideoResults.class);

            // ユーチューブの動画URLを作成
            return String.format(Const.LINE_YOUTUBE_LINK, tmdbObjVideoResults.getKey());

        } catch (IOException e) {
            logger.error(e.toString());
            throw e;
        }
    }
}
