package com.main.service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.main.model.LineObjMessageDetails;
import com.main.model.LineObjMessages;
import com.main.util.Const;
import com.main.util.DateUtils;
import com.main.util.LogUtils;

@Service
public class LineApiService {
    
    /** オブジェクトマッパー */
    private final ObjectMapper objMapper = new ObjectMapper();
    
    @Autowired
    Environment env;
    @Autowired
    LogUtils logger;
    @Autowired
    DateUtils dateUtils;
    
    /**
     * LINEブロードキャストメッセージを配信する
     * @param messasge メッセージ
     * @return メッセージ送信OK：true メッセージ送信NG:false
     * @throws IllegalArgumentException
     * @throws IOException
     */
    public Boolean sendLineBroadcastMessage(String jsonForFlexMsg) throws IllegalArgumentException, IOException {

        // LineApiKeyを取得
        String lineApiKey = env.getProperty("api.key.line.messageapi");
        
        try {
            // リクエスト先URLを設定
            HttpPost httpPost = new HttpPost(Const.LINE_URL_MESSAGE_BROADCAST);
            
            // ヘッダー設定
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Authorization", "Bearer " + lineApiKey);

            // body設定
            StringEntity params = new StringEntity(jsonForFlexMsg, StandardCharsets.UTF_8);
            httpPost.setEntity(params);
            
            // リクエスト実行
            CloseableHttpClient client = HttpClients.createDefault();
            CloseableHttpResponse resp = client.execute(httpPost);
            int statusCode = resp.getStatusLine().getStatusCode();
            
            if(statusCode == 200) {
                return true;
            } else {
                return null;
            }
        } catch(IllegalArgumentException | IOException e) {
            logger.error(e.toString());
            throw e;
        }
    }
    
    

    /**
     * LINEメッセージを送りたい時に使用する 
     * @param messasge メッセージ
     * @return メッセージ送信OK：true メッセージ送信NG:false
     * @throws IllegalArgumentException
     * @throws IOException
     */
    public Boolean sendLineMessages(String message) throws IllegalArgumentException, IOException {

        // LineApiKeyを取得
        String lineApiKey = env.getProperty("api.key.line.messageapi");
        
        try {
            
            List<LineObjMessageDetails> lineObjMessageDetailsList = new ArrayList<>();
            
            // メッセージオブジェクト生成
            LineObjMessageDetails lineObjMessageDetails = new LineObjMessageDetails();
            lineObjMessageDetails.setType("text");
            lineObjMessageDetails.setText(message);
            
            // メッセージオブジェクトリストを生成
            lineObjMessageDetailsList.add(lineObjMessageDetails);
            LineObjMessages lineObjMessages = new LineObjMessages();
            lineObjMessages.setMessages(lineObjMessageDetailsList);
            
            // オブジェクト→Json変換
            String lineObjMessagesJson = objMapper.writeValueAsString(lineObjMessages);


            // リクエスト先URLを設定
            HttpPost httpPost = new HttpPost(Const.LINE_URL_MESSAGE_BROADCAST);
            
            // ヘッダー設定
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Authorization", "Bearer " + lineApiKey);

            // body設定
            StringEntity params = new StringEntity(lineObjMessagesJson, StandardCharsets.UTF_8);
            httpPost.setEntity(params);
            
            // リクエスト実行
            CloseableHttpClient client = HttpClients.createDefault();
            CloseableHttpResponse resp = client.execute(httpPost);
            int statusCode = resp.getStatusLine().getStatusCode();
            
            if(statusCode == 200) {
                return true;
            } else {
                return null;
            }
        } catch(IllegalArgumentException | IOException e) {
            logger.error(e.toString());
            throw e;
        }
    }
    
}
