package com.main.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.main.model.TmdbObjUpcomingResults;
import com.main.util.Const;
import com.main.util.LogUtils;

@Service
public class ApiAccessBatch {

    @Autowired
    LineApiService lineApiService;
    @Autowired
    TmdbApiService tmdbApiService;
    @Autowired
    LogUtils logger;

    // cronは秒、分、時、日、月、曜日 を指定することが可能。曜日は（0,1,2,3,4,5,6,7が日～日を表す。※日は2つある）
    @Scheduled(cron = "0 0 10 * * *", zone = "Asia/Tokyo")
//    @Scheduled(fixedRate = 10000)
    public void castBroadcaseMessage() {

        try {
            StringBuilder flexMsgContent = new StringBuilder();
            List<TmdbObjUpcomingResults> tmdbObjUpcomingResultsList = tmdbApiService.getUpcomingMovie();
            for (Iterator<TmdbObjUpcomingResults> iterator = tmdbObjUpcomingResultsList.iterator(); iterator
                    .hasNext();) {
                TmdbObjUpcomingResults tmdbObjUpcomingResults = iterator.next();
                // 広告画像URLを生成
                String imageUrl = String.format(Const.TMDB_URL_IMAGE, tmdbObjUpcomingResults.getPoster_path());

                flexMsgContent.append(String.format(Const.LINE_FLEXMSG_JSON_CONTENT, imageUrl,
                        tmdbObjUpcomingResults.getRelease_date(), tmdbObjUpcomingResults.getTitle()));
                if (iterator.hasNext()) {
                    flexMsgContent.append(",");
                }
            }
            String jsonStr = String.format(Const.LINE_FLEX_JSON_FRAME, flexMsgContent.toString());
            lineApiService.sendLineBroadcastMessage(jsonStr);

        } catch (IOException | ParseException e) {
            logger.error("エラー");
        }

    }
}
