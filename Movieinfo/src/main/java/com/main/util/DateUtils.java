package com.main.util;

import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class DateUtils {

    /**
     * 日付（時分秒をのぞく）を比較
     * @param date1 日付１
     * @param date2 日付２
     * @return 同日：0　日付１が大きい：1 日付２が大きい:-1
     */
    public int compareDateDay(Date date1, Date date2) {
       // 日付１の時分秒を0に設定
       Calendar cal1 = Calendar.getInstance();
       cal1.setTime(date1);
       cal1.set(Calendar.HOUR_OF_DAY, 0);
       cal1.set(Calendar.MINUTE, 0);
       cal1.set(Calendar.SECOND, 0);
       cal1.set(Calendar.MILLISECOND, 0);
       
       // 日付２の時分秒を0に設定
       Calendar cal2 = Calendar.getInstance();
       cal2.setTime(date2);
       cal2.set(Calendar.HOUR_OF_DAY, 0);
       cal2.set(Calendar.MINUTE, 0);
       cal2.set(Calendar.SECOND, 0);
       cal2.set(Calendar.MILLISECOND, 0);
       
       if(cal1.compareTo(cal2) == 0) {
           return 0;
       } else if(cal1.compareTo(cal2) > 0) {
           return 1;
       } else {
           return -1;
       }
    }
}
