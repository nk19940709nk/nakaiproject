package com.main.util;

import java.text.SimpleDateFormat;

/**
 * 定数クラス
 *
 */
public class Const {

    /**
     * プライベートコンストラクタ。
     * インスタンスを禁止する
     */
    private Const() {
    }
    
    /** TMDBApiURL:公開間近映画情報取得用 */
    public static final String TMDB_URL_UPCOMING = "https://api.themoviedb.org/3/movie/upcoming?api_key=%s&language=ja-JA&page=1";

    /** TMDBApiURL:予告動画取得用 */
    public static final String TMDB_URL_VIDEO = "https://api.themoviedb.org/3/movie/%s/videos?api_key=%s&language=ja-JA";
    
    /** TMDBApiURL:imageパス */
    public static final String TMDB_URL_IMAGE = "https://image.tmdb.org/t/p/original%s";
    
    /** LINEMessage送信用URL */
    public static final String LINE_URL_MESSAGE_BROADCAST = "https://api.line.me/v2/bot/message/broadcast";

    /** 予告動画作成用YoutubeURL */
    public static final String LINE_YOUTUBE_LINK = "https://www.youtube.com/watch?v=%s";
    
    /** LINEFLEXメッセージの枠組み */
    public static final String LINE_FLEX_JSON_FRAME = "{\"messages\":[{\"type\":\"flex\",\"altText\":\"公開間近の映画情報！\",\"contents\":{\"type\":\"carousel\",\"contents\":[%s]}}]}";
    
    /**
     * LINEFLEXメッセージ用JSON<br>
     * 設定要素<br>
     * 1:画像URL<br>
     * 2:公開日<br>
     * 3:映画タイトル<br>
     */
    public static final String LINE_FLEXMSG_JSON_CONTENT = "{\"type\":\"bubble\",\"body\":{\"type\":\"box\",\"layout\":\"vertical\",\"contents\":[{\"type\":\"image\",\"url\":\"%s\",\"size\":\"full\",\"aspectMode\":\"cover\",\"gravity\":\"top\",\"aspectRatio\":\"4:5\"},{\"type\":\"box\",\"layout\":\"vertical\",\"contents\":[{\"type\":\"box\",\"layout\":\"baseline\",\"contents\":[{\"type\":\"text\",\"text\":\"%s\",\"color\":\"#ebebeb\",\"size\":\"xs\",\"flex\":0}],\"spacing\":\"md\",\"paddingBottom\":\"4px\"},{\"type\":\"box\",\"layout\":\"vertical\",\"contents\":[{\"type\":\"text\",\"text\":\"%s\",\"size\":\"lg\",\"color\":\"#ffffff\",\"weight\":\"bold\"}]},{\"type\":\"box\",\"layout\":\"vertical\",\"contents\":[{\"type\":\"button\",\"action\":{\"type\":\"uri\",\"label\":\"Detail\",\"uri\":\"https://eigaland.com/cinema\"},\"color\":\"#FFFFFF\",\"style\":\"link\",\"height\":\"sm\"}],\"borderWidth\":\"1px\",\"cornerRadius\":\"4px\",\"spacing\":\"sm\",\"borderColor\":\"#ffffff\",\"margin\":\"xxl\",\"height\":\"40px\"}],\"position\":\"absolute\",\"offsetBottom\":\"0px\",\"offsetStart\":\"0px\",\"offsetEnd\":\"0px\",\"backgroundColor\":\"#051730cc\",\"paddingAll\":\"20px\",\"paddingTop\":\"18px\"}],\"paddingAll\":\"0px\"}}";

    /** 日付フォーマット：yyyy/MM/dd */
    public static final SimpleDateFormat DATE_FORMAT_YYYYMMDD = new SimpleDateFormat("yyyy/MM/dd");
}
