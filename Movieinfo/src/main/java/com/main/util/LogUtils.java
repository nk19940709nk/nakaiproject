package com.main.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * ログ出力クラス
 *
 */
@Component
public class LogUtils {
    protected final static Logger logger = LoggerFactory.getLogger(LogUtils.class);
    /**
     * エラーメッセージ：インフォ
     * @param msg
     */
    public void info(String msg) {
        logger.info(msg);
    }
    
    /**
     * エラーメッセージ：ワーニング
     * @param msg
     */
    public void warn(String msg) {
        logger.warn(msg);
    }
    
    /**
     * エラーメッセージ：エラー
     * @param msg
     */
    public void error(String msg) {
        logger.error(msg);
    }
    
    /**
     * エラーメッセージ：デバグ
     * @param msg
     */
    public void debug(String msg) {
        logger.debug(msg);
    }
    
    /**
     * エラーメッセージ：トレース
     * @param msg
     */
    public void trace(String msg) {
        logger.trace(msg);
    }
}
