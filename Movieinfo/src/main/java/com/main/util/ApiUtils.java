package com.main.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ApiUtils {
    
    @Autowired
    LogUtils logger;
    
    /**
     * URLにアクセスしてJsonデータを取得する
     * 
     * @param url 接続先URL
     * @return jsonデータ
     * @throws IOException
     */
    public String getJson(URL url) throws IOException {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer json = new StringBuffer();
            String line;
            while ((line = reader.readLine()) != null) {
                json.append(line);
            }
            reader.close();
            return json.toString();
        } catch (IOException e) {
            logger.error(e.toString());
            throw e;
        }
    }
}
