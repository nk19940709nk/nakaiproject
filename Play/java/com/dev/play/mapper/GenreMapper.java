package com.dev.play.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.dev.play.model.db.GenreDto;

/**
 * ジャンルマッパー
 * 
 * @author nakai.k
 *
 */
@Mapper
public interface GenreMapper {
    List<GenreDto> selectGenreAll();

    void insertGenre(GenreDto genre);

    Integer selectGenreId();
}
