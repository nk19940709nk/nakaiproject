package com.dev.play.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.dev.play.model.db.GenreDto;
import com.dev.play.model.db.PlayInfoDto;
import com.dev.play.model.db.PlayInfoForPlaceCountDto;
import com.dev.play.model.request.PlaceCountRequest;
import com.dev.play.model.request.PlayInfoDeleteRequest;
import com.dev.play.model.request.PlayInfoInsertRequest;
import com.dev.play.model.request.PlayInfoSearchRequest;
import com.dev.play.model.request.PlayInfoUpdateRequest;

/**
 * やること情報テーブルマッパー
 * 
 * @author nakai.k
 *
 */
@Mapper
public interface PlayInfoMapper {

    /**
     * あそび情報取得
     * 
     * @param request 検索情報リクエスト
     * @return
     */
    List<PlayInfoDto> selectPlayInfo(PlayInfoSearchRequest request);

    /**
     * あそび情報全取得
     * 
     * @return あそび情報
     */
    List<PlayInfoDto> selectPlayInfoAll();

    /**
     * あそび情報削除
     * 
     * @param playInfoDeleteRequest 削除対象リクエスト
     */
    void deletePlayInfo(PlayInfoDeleteRequest playInfoDeleteRequest);

    /**
     * あそび情報更新
     * 
     * @param playInfoUpdateRequest 更新対象リクエスト
     */
    void updatePlayInfo(PlayInfoUpdateRequest playInfoUpdateRequest);

    /**
     * あそび情報登録
     * 
     * @param playInfoInsertRequest 登録対象リクエスト
     */
    void insertPlayInfo(PlayInfoInsertRequest playInfoInsertRequest);

    /**
     * あそびテーブルシーケンス番号取得
     * 
     * @return
     */
    Integer selectPlayInfoId();

    /**
     * あそび情報場所種類取得
     * 
     * @return
     */
    List<PlayInfoForPlaceCountDto> selectPlayInfoWithPlace(PlaceCountRequest request);

    /**
     * あそび情報に含まれるジャンル取得
     * 
     * @return
     */
    List<GenreDto> selectPlayInfoWithGenre();

}
