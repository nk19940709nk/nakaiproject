package com.dev.play.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.dev.play.model.db.WeatherDto;

/**
 * 天気テーブルマッパー
 * 
 * @author nakai.k
 *
 */
@Mapper
public interface WeatherMapper {

    /**
     * 天気情報全取得
     * 
     * @return
     */
    List<WeatherDto> selectWeatherAll();

}
