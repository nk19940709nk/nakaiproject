package com.dev.play.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.dev.play.model.db.PlaceDto;

/**
 * 場所テーブルマッパー
 * 
 * @author nakai.k
 *
 */
@Mapper
public interface PlaceMapper {

    /**
     * 場所情報全取得
     * 
     * @return
     */
    List<PlaceDto> selectPlaceAll();

}
