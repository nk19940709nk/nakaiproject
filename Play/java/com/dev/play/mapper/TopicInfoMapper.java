package com.dev.play.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.dev.play.model.db.TopicInfoDto;
import com.dev.play.model.request.TopicInfoInsertRequest;

/**
 * トピック情報テーブルマッパー
 * 
 * @author nakai.k
 *
 */
@Mapper
public interface TopicInfoMapper {

    /**
     * トピックテーブル全取得
     * 
     * @return トピック情報リスト
     */
    List<TopicInfoDto> selectTopicInfoAll();

    /**
     * トピック情報登録
     * 
     * @param topicInfoDtoList 登録情報リスト
     */
    void insertTopicInfo(TopicInfoInsertRequest topicInfoDtoList);

    /**
     * トピックテーブル削除
     */
    void deleteTopicInfo();

    /**
     * トピックテーブルシーケンス番号取得
     * 
     * @return
     */
    Integer selectTopicInfoId();
}
