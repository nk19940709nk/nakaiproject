package com.dev.play.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.dev.play.model.db.UserInfoDto;
import com.dev.play.model.request.LoginRequest;

@Mapper
public interface UserInfoMapper {

    UserInfoDto selectUserInfo(LoginRequest request);

}
