package com.dev.play.model.response;

import java.util.List;

import com.dev.play.model.db.GenreDto;
import com.dev.play.model.db.PlaceDto;
import com.dev.play.model.db.WeatherDto;

import lombok.Data;

@Data
public class ConditionsDataResponse {

    private List<GenreDto> genreList;

    private List<PlaceDto> placeList;

    private List<WeatherDto> weatherList;

}
