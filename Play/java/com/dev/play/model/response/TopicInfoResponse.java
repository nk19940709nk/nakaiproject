package com.dev.play.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TopicInfoResponse {

    /** トピックID */
    @JsonProperty("topicId")
    private Integer topicId;

    /** トピックサイトタイトル */
    @JsonProperty("topicSiteTitle")
    private String topicSiteTitle;

    /** トピックサイトURL */
    @JsonProperty("topicSiteUrl")
    private String topicSiteUrl;

    /** トピックOGPURL */
    @JsonProperty("topicOgpUrl")
    private String topicOgpUrl;

    @JsonProperty("genreName")
    private String genreName;

    @JsonProperty("genreIcon")
    private String genreIcon;
}
