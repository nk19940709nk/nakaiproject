package com.dev.play.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class RegistedGenreResponse {
    
    /** ジャンルID */
    @JsonProperty("genreId")
    private Integer genreId;

    /** ジャンル名称 */
    @JsonProperty("genreName")
    private String genreName;
}
