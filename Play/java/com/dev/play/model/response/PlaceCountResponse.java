package com.dev.play.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class PlaceCountResponse {

    /** 地名ID */
    @JsonProperty("placeId")
    private Integer placeId;

    /** 地名 */
    @JsonProperty("placeName")
    private String placeName;

    /** 地名カウント */
    @JsonProperty("placeCount")
    private Integer placeCount;
}
