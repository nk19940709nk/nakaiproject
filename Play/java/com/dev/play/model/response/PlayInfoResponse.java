package com.dev.play.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class PlayInfoResponse {

    @JsonProperty("playInfoId")
    private Integer playInfoId;

    @JsonProperty("playInfoName")
    private String playInfoName;

    @JsonProperty("playInfoSiteUrl")
    private String playInfoSiteUrl;

    @JsonProperty("playInfoOgpUrl")
    private String playInfoOgpUrl;

    @JsonProperty("playInfoSnsUrl")
    private String playInfoSnsUrl;

    @JsonProperty("playInfoWeather")
    private Integer playInfoWeather;

    @JsonProperty("playInfoPlace")
    private Integer playInfoPlace;

    @JsonProperty("playInfoGenre")
    private Integer playInfoGenre;

    @JsonProperty("playInfoDescription")
    private String playInfoDescription;

    @JsonProperty("playInfoTag")
    private String playInfoTag;

    @JsonProperty("genreName")
    private String genreName;

    @JsonProperty("genreIcon")
    private String genreIcon;
}
