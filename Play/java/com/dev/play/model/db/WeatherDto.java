package com.dev.play.model.db;

import lombok.Data;

/**
 * 天気テーブルDtoクラス
 * 
 * @author nakai.k
 *
 */
@Data
public class WeatherDto {

    /** 天気ID */
    private Integer weatherId;

    /** 天気名称 */
    private String weatherName;
}
