package com.dev.play.model.db;

import lombok.Data;

/**
 * ジャンルテーブルDto
 * 
 * @author nakai.k
 *
 */
@Data
public class GenreDto {

    /** ジャンルID */
    private Integer genreId;

    /** ジャンル名称 */
    private String genreName;

    /** アイコン */
    private String genreIcon;

}
