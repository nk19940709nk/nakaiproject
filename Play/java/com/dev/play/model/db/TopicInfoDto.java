package com.dev.play.model.db;

import lombok.Data;

/**
 * トピック情報Dto
 * 
 * @author nakai.k
 *
 */
@Data
public class TopicInfoDto {

    /** トピックID */
    private Integer topicId;

    /** トピックサイトタイトル */
    private String topicSiteTitle;

    /** トピックサイトURL */
    private String topicSiteUrl;

    /** トピックOGPURL */
    private String topicOgpUrl;

    /** ジャンル */
    private Integer genreId;
}
