package com.dev.play.model.db;

import lombok.Data;

/**
 * 遊び情報Dto
 * 
 * @author nakai.k
 *
 */
@Data
public class PlayInfoDto {

    /** あそび情報ID */
    private Integer playInfoId;

    /** あそび情報 */
    private String playInfoName;

    /** あそび情報サイトURL */
    private String playInfoSiteUrl;

    /** あそび情報OGPURL */
    private String playInfoOgpUrl;

    /** あそび情報SnsURL */
    private String playInfoSnsUrl;

    /** あそび情報人数 */
    private Integer playInfoPeople;

    /** あそび情報天気 */
    private Integer playInfoWeather;

    /** あそび情報場所 */
    private Integer playInfoPlace;

    /** あそび情報ジャンル */
    private Integer playInfoGenre;

    /** あそび情報説明 */
    private String playInfoDescription;

    /** あそびタグ情報 */
    private String playInfoTag;

    /** ジャンル名称 */
    private String genreName;

    /** ジャンルアイコン */
    private String genreIcon;

}
