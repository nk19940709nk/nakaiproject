package com.dev.play.model.db;

import lombok.Data;

/**
 * 場所テーブルDto
 * 
 * @author nakai.k
 *
 */
@Data
public class PlaceDto {

    /** 場所ID */
    private Integer placeId;
    
    /** 場所名称 */
    private String placeName;
}
