package com.dev.play.model.db;

import lombok.Data;

@Data
public class UserInfoDto {
    /** ユーザーネーム */
    private String userName;

    /** パスワード */
    private String userPassword;
}
