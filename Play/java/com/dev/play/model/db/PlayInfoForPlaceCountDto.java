package com.dev.play.model.db;

import lombok.Data;

/**
 * 場所情報Dto
 * 
 * @author nakai.k
 *
 */
@Data
public class PlayInfoForPlaceCountDto {

    /** 地名ID */
    private Integer placeId;

    /** 地名 */
    private String placeName;

    /** 地名カウント */
    private Integer placeCount;
}
