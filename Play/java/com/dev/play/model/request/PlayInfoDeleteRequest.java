package com.dev.play.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class PlayInfoDeleteRequest {
    @JsonProperty("playInfoId")
    private Integer playInfoId;
}
