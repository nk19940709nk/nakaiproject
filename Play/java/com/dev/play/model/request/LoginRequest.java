package com.dev.play.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class LoginRequest {
    /** ユーザーネーム */
    @JsonProperty("username")
    private String username;

    /** パスワード */
    @JsonProperty("password")
    private String password;
}
