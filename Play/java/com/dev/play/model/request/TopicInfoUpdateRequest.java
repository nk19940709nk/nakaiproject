package com.dev.play.model.request;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class TopicInfoUpdateRequest {
    /** 更新対象ジャンル */
    @JsonProperty("genreList")
    private List<Integer> genreList;
}
