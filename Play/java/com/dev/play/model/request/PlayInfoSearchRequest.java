package com.dev.play.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class PlayInfoSearchRequest {

    @JsonProperty("genreVal")
    private Integer genreVal;

    @JsonProperty("placeVal")
    private Integer placeVal;

    @JsonProperty("weatherVal")
    private Integer weatherVal;
}
