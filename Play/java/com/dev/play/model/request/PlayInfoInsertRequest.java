package com.dev.play.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class PlayInfoInsertRequest {

    /** あそび情報ID */
    @JsonProperty("playInfoId")
    private Integer playInfoId;

    /** あそび情報 */
    @JsonProperty("playInfoName")
    private String playInfoName;

    /** あそび情報サイトURL */
    @JsonProperty("playInfoSiteUrl")
    private String playInfoSiteUrl;

    /** あそび情報OGPURL */
    @JsonProperty("playInfoOgpUrl")
    private String playInfoOgpUrl;
    
    /** あそび情報SNSURL */
    @JsonProperty("playInfoSnsUrl")
    private String playInfoSnsUrl;

    /** あそび情報天気 */
    @JsonProperty("playInfoWeather")
    private Integer playInfoWeather;

    /** あそび情報場所 */
    @JsonProperty("playInfoPlace")
    private Integer playInfoPlace;

    /** あそび情報ジャンル */
    @JsonProperty("playInfoGenre")
    private Integer playInfoGenre;

    /** あそび情報説明 */
    @JsonProperty("playInfoDescription")
    private String playInfoDescription;

    /** あそびタグ情報 */
    @JsonProperty("playInfoTag")
    private String playInfoTag;
}
