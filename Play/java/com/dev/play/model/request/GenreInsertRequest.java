package com.dev.play.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * ジャンル登録リクエスト
 * 
 * @author nakai.k
 *
 */
@Data
public class GenreInsertRequest {
    @JsonProperty("genreName")
    private String genreName;

    @JsonProperty("genreIcon")
    private String genreIcon;

}
