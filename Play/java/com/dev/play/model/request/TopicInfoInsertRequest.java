package com.dev.play.model.request;

import java.util.List;

import com.dev.play.model.db.TopicInfoDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class TopicInfoInsertRequest {

    /** トピックDtoリスト */
    @JsonProperty("topicInfoDtoList")
    private List<TopicInfoDto> topicInfoDtoList;
}
