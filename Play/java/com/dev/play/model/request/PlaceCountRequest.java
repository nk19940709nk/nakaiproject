package com.dev.play.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class PlaceCountRequest {
    @JsonProperty("genreId")
    private Integer genreId;
}
