package com.dev.play.model.util;

import lombok.Data;

@Data
public class JsoupOgpDto {

    /** サイトURL */
    private String siteUrl;

    /** OGPイメージURL */
    private String ogpImageUrl;

    /** OGPタイトル */
    private String ogpTitle;

}
