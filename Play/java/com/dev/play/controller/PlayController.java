package com.dev.play.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dev.play.model.db.GenreDto;
import com.dev.play.model.db.PlayInfoDto;
import com.dev.play.model.db.PlayInfoForPlaceCountDto;
import com.dev.play.model.db.TopicInfoDto;
import com.dev.play.model.db.UserInfoDto;
import com.dev.play.model.request.GenreInsertRequest;
import com.dev.play.model.request.LoginRequest;
import com.dev.play.model.request.PlaceCountRequest;
import com.dev.play.model.request.PlayInfoDeleteRequest;
import com.dev.play.model.request.PlayInfoInsertRequest;
import com.dev.play.model.request.PlayInfoSearchRequest;
import com.dev.play.model.request.PlayInfoUpdateRequest;
import com.dev.play.model.request.TopicInfoInsertRequest;
import com.dev.play.model.request.TopicInfoUpdateRequest;
import com.dev.play.model.response.ConditionsDataResponse;
import com.dev.play.model.response.PlaceCountResponse;
import com.dev.play.model.response.PlayInfoResponse;
import com.dev.play.model.response.RegistedGenreResponse;
import com.dev.play.model.response.TopicInfoResponse;
import com.dev.play.model.util.JsoupOgpDto;
import com.dev.play.service.db.GenreService;
import com.dev.play.service.db.PlaceService;
import com.dev.play.service.db.PlayInfoService;
import com.dev.play.service.db.TopicInfoService;
import com.dev.play.service.db.UserInfoService;
import com.dev.play.service.db.WeatherService;
import com.dev.play.service.util.EncryptService;
import com.dev.play.service.util.JsoupService;
import com.dev.play.util.StringUtil;

/**
 * コントローラークラス
 * 
 * @author nakai.k
 *
 */
@RestController
@EnableScheduling
// テスト用クロスオリジン設定
//@CrossOrigin(origins = "http://localhost:3000")
@CrossOrigin
public class PlayController {

    @Autowired
    TopicInfoService topicInfoService;
    @Autowired
    PlayInfoService playInfoService;
    @Autowired
    GenreService genreService;
    @Autowired
    PlaceService placeService;
    @Autowired
    WeatherService weatherService;
    @Autowired
    UserInfoService userInfoService;
    @Autowired
    JsoupService jsoupUtil;
    @Autowired
    EncryptService encryptService;

    @Value("${target.location}")
    private String locationAry;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * トピック情報取得
     * 
     * @return トピック情報リスト
     */
    @GetMapping(value = "/topicinfo")
    public Map<String, List<TopicInfoResponse>> getTopicInfoList() {

        Map<String, List<TopicInfoResponse>> response = new HashMap<>();
        try {
            // トピック情報を取得
            List<TopicInfoDto> topicInfoList = topicInfoService.getTopicInfoList();
            List<GenreDto> genreList = genreService.getGenreAll();
            Map<Integer, GenreDto> genreMap = genreList.stream().collect(Collectors.toMap(s -> s.getGenreId(), s -> s));

            // トピック情報をレスポンスに格納
            List<TopicInfoResponse> topicInfoResponseList = new ArrayList<>();
            for (TopicInfoDto topicInfo : topicInfoList) {
                TopicInfoResponse topicInfoResponse = new TopicInfoResponse() {
                    {
                        setTopicId(topicInfo.getTopicId());
                        setTopicSiteTitle(topicInfo.getTopicSiteTitle());
                        setTopicSiteUrl(topicInfo.getTopicSiteUrl());
                        setTopicOgpUrl(topicInfo.getTopicOgpUrl());
                        setGenreName(genreMap.get(topicInfo.getGenreId()).getGenreName());
                        setGenreIcon(genreMap.get(topicInfo.getGenreId()).getGenreIcon());
                    }
                };
                topicInfoResponseList.add(topicInfoResponse);
            }
            if (!ObjectUtils.isEmpty(topicInfoResponseList)) {
                response = topicInfoResponseList.stream()
                        .collect(Collectors.groupingBy(TopicInfoResponse::getGenreName));
            }

        } catch (Exception e) {
            logger.error(e.toString());
        }
        return response;
    }

    /**
     * あそび情報検索
     * 
     * @param request あそび情報検索リクエスト
     * 
     * @return
     */
    @PostMapping(value = "/playinfo")
    public List<PlayInfoResponse> getPlayInfoList(@RequestBody PlayInfoSearchRequest request) {
        List<PlayInfoDto> playInfoDtoList = new ArrayList<>();
        try {
            // リクエストチェック
            if (request.getGenreVal() != null && request.getPlaceVal() != null && request.getWeatherVal() != null) {
                playInfoDtoList = playInfoService.getPlayInfo(request);
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }

        if (!CollectionUtils.isEmpty(playInfoDtoList)) {
            return setPlayInfo(playInfoDtoList);
        } else {
            return null;
        }

    }

    /**
     * あそび情報追加
     * 
     * @param request 追加するあそび情報
     * @return
     */
    @PostMapping(value = "/addplayinfo")
    private boolean addPlayInfo(@RequestBody PlayInfoInsertRequest request) {
        // リクエストチェック
        if (!StringUtil.isEmpty(request.getPlayInfoName()) && !StringUtil.isEmpty(request.getPlayInfoSiteUrl())
                && !StringUtil.isEmpty(request.getPlayInfoTag()) && request.getPlayInfoGenre() != null
                && request.getPlayInfoPlace() != null && request.getPlayInfoWeather() != null) {
            try {
                request.setPlayInfoId(playInfoService.getPlayInfoId());
                if (StringUtil.isEmpty(request.getPlayInfoOgpUrl())) {
                    request.setPlayInfoOgpUrl(jsoupUtil.getImageUrl(request.getPlayInfoSiteUrl()));
                }
                if (request.getPlayInfoTag().contains("、")) {
                    request.setPlayInfoTag(request.getPlayInfoTag().replace("、", ","));
                }
                playInfoService.addPlayInfo(request);
            } catch (Exception e) {
                logger.error(e.toString());
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * あそび情報削除
     * 
     * @param request 削除するあそび情報
     * @return
     */
    @PostMapping(value = "/deleteplayinfo")
    private boolean deletePlayInfo(@RequestBody PlayInfoDeleteRequest request) {
        if (request.getPlayInfoId() != null) {
            try {
                playInfoService.deletePlayInfo(request);
            } catch (Exception e) {
                logger.error(e.toString());
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * あそび情報更新
     * 
     * @param request 更新するあそび情報
     * @return
     */
    @PostMapping(value = "/updateplayinfo")
    private boolean updatePlayInfo(@RequestBody PlayInfoUpdateRequest request) {
        if (!StringUtil.isEmpty(request.getPlayInfoName()) && !StringUtil.isEmpty(request.getPlayInfoSiteUrl())
                && !StringUtil.isEmpty(request.getPlayInfoTag()) && request.getPlayInfoGenre() != null
                && request.getPlayInfoPlace() != null && request.getPlayInfoWeather() != null) {
            try {
                if (StringUtil.isEmpty(request.getPlayInfoOgpUrl())) {
                    request.setPlayInfoOgpUrl(jsoupUtil.getImageUrl(request.getPlayInfoSiteUrl()));
                }
                if (request.getPlayInfoTag().contains("、")) {
                    request.setPlayInfoTag(request.getPlayInfoTag().replace("、", ","));
                }
                playInfoService.updatePlayInfo(request);
            } catch (Exception e) {
                logger.error(e.toString());
                return false;
            }
        }
        return true;
    }

    /**
     * あそび情報全取得
     * 
     * @return
     */
    @GetMapping(value = "/playinfoall")
    public Map<String, List<PlayInfoResponse>> getPlayInfoAllList() {
        Map<String, List<PlayInfoResponse>> response = new HashMap<>();
        try {
            List<PlayInfoResponse> playInfoResponseList = new ArrayList<>();
            List<PlayInfoDto> playInfoDtoList = playInfoService.getPlayInfoAll();
            for (PlayInfoDto playInfoDto : playInfoDtoList) {
                PlayInfoResponse playInfoResponse = new PlayInfoResponse() {
                    {
                        setPlayInfoId(playInfoDto.getPlayInfoId());
                        setPlayInfoName(playInfoDto.getPlayInfoName());
                        setPlayInfoSiteUrl(playInfoDto.getPlayInfoSiteUrl());
                        setPlayInfoOgpUrl(playInfoDto.getPlayInfoOgpUrl());
                        setPlayInfoSnsUrl(playInfoDto.getPlayInfoSnsUrl());
                        setPlayInfoWeather(playInfoDto.getPlayInfoWeather());
                        setPlayInfoPlace(playInfoDto.getPlayInfoPlace());
                        setPlayInfoGenre(playInfoDto.getPlayInfoGenre());
                        setPlayInfoDescription(playInfoDto.getPlayInfoDescription());
                        setPlayInfoTag(playInfoDto.getPlayInfoTag());
                        setGenreName(playInfoDto.getGenreName());
                        setGenreIcon(playInfoDto.getGenreIcon());
                    }
                };
                playInfoResponseList.add(playInfoResponse);
            }
            if (!ObjectUtils.isEmpty(playInfoResponseList)) {
                response = playInfoResponseList.stream().collect(Collectors.groupingBy(PlayInfoResponse::getGenreName));
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }

        return response;
    }

    /**
     * 場所種類情報取得
     * 
     * @return
     */
    @PostMapping(value = "/placecount")
    public List<PlaceCountResponse> getPlaceCountData(@RequestBody PlaceCountRequest request) {
        List<PlaceCountResponse> response = new ArrayList<>();
        List<PlayInfoForPlaceCountDto> playInfoForPlaceCountDtoList = new ArrayList<>();
        try {
            playInfoForPlaceCountDtoList = playInfoService.getPlayInfoPlaceCount(request);
            for (PlayInfoForPlaceCountDto playInfoForPlaceCountDto : playInfoForPlaceCountDtoList) {
                PlaceCountResponse placeCountResponse = new PlaceCountResponse() {
                    {
                        setPlaceId(playInfoForPlaceCountDto.getPlaceId());
                        setPlaceName(playInfoForPlaceCountDto.getPlaceName());
                        setPlaceCount(playInfoForPlaceCountDto.getPlaceCount());
                    }
                };
                response.add(placeCountResponse);
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return response;
    }

    @GetMapping(value = "/registedgenre")
    public List<RegistedGenreResponse> getRegistedGenre() {
        List<RegistedGenreResponse> response = new ArrayList<>();
        List<GenreDto> genreDtoList = new ArrayList<>();
        try {
            genreDtoList = playInfoService.getPlayInfoForGenre();
            for (GenreDto genreDto : genreDtoList) {
                RegistedGenreResponse registedGenreResponse = new RegistedGenreResponse() {
                    {
                        setGenreId(genreDto.getGenreId());
                        setGenreName(genreDto.getGenreName());
                    }
                };
                response.add(registedGenreResponse);
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
        return response;
    }

    /**
     * セレクトボックス選択値取得
     * 
     * @return
     */
    @GetMapping(value = "/conditionsdata")
    public ConditionsDataResponse getConditionsData() {
        return setConditionsData();
    }

    /**
     * トピック情報更新
     * 
     * @param request
     */
    @PostMapping(value = "/topicinfoupdate")
    public void updateTopicInfo(@RequestBody TopicInfoUpdateRequest request) {
        System.out.println("バッチ実行");
        try {

            // トピック情報を削除
            topicInfoService.deleteTopicInfo();
            // 取得対象地名
            String[] locations = locationAry.split(",");
            // ジャンル取得
            List<GenreDto> genreList = genreService.getGenreAll();
            for (int i = 0; i < genreList.size(); i++) {
                if (genreList.get(i).getGenreId() == 99) {
                    genreList.remove(i);
                }
            }

            // トピックDtoリスト
            List<TopicInfoDto> topicInfoDtoList = new ArrayList<>();

            for (String location : locations) {
                for (GenreDto genre : genreList) {
                    if (request.getGenreList().contains(genre.getGenreId())) {
                        // トピック情報取得
                        List<JsoupOgpDto> jsoupOgpDtoList = jsoupUtil.getOgpDataList(location, genre.getGenreName());
                        for (JsoupOgpDto jsoupOgpDto : jsoupOgpDtoList) {
                            TopicInfoDto topicInfoDto = new TopicInfoDto() {
                                {
                                    setTopicId(topicInfoService.getTopicInfoId());
                                    setTopicSiteTitle(jsoupOgpDto.getOgpTitle());
                                    setTopicSiteUrl(jsoupOgpDto.getSiteUrl());
                                    setTopicOgpUrl(jsoupOgpDto.getOgpImageUrl());
                                    setGenreId(genre.getGenreId());
                                }
                            };
                            topicInfoDtoList.add(topicInfoDto);
                        }
                    }

                }
            }

            TopicInfoInsertRequest opicInfoInsertRequest = new TopicInfoInsertRequest() {
                {
                    setTopicInfoDtoList(topicInfoDtoList);
                }
            };
            // トピック情報を追加
            topicInfoService.insertTopicInfo(opicInfoInsertRequest);

            System.out.println("バッチ終了");
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    /**
     * ジャンル登録
     * 
     * @param request 登録するジャンル
     * @return
     */
    @PostMapping(value = "/insertgenre")
    private boolean insertGenre(@RequestBody GenreInsertRequest request) {
        if (!StringUtil.isEmpty(request.getGenreName()) && !StringUtil.isEmpty(request.getGenreIcon())) {
            GenreDto genreDto = new GenreDto() {
                {
                    setGenreName(request.getGenreName());
                    setGenreIcon(request.getGenreIcon());
                }
            };
            try {
                genreDto.setGenreId(genreService.selectGenreId());
                genreService.insertGenre(genreDto);
            } catch (Exception e) {
                logger.error(e.toString());
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    /**
     * ログイン
     * 
     * @param request 更新するあそび情報
     * @return
     */
    @PostMapping(value = "/login")
    private boolean login(@RequestBody LoginRequest request) {
        if (!StringUtil.isEmpty(request.getUsername()) && !StringUtil.isEmpty(request.getPassword())) {
            try {
                UserInfoDto response = userInfoService.getUser(request);
                if (encryptService.decrypt(response.getUserPassword()).equals(request.getPassword())) {
                    return true;
                }
            } catch (Exception e) {
                logger.error(e.toString());
                return false;
            }
        }
        return false;
    }

    /**
     * あそび情報リストからランダムに要素を取得する
     * 
     * @param playInfoDtoList あそび情報リスト
     * @return
     */
    private List<PlayInfoResponse> setPlayInfo(List<PlayInfoDto> playInfoDtoList) {

        // 乱数取得
        List<Integer> rndIndexList = new ArrayList<>();
        for (int i = 0; i < playInfoDtoList.size(); i++) {
            rndIndexList.add(i);
        }
        Collections.shuffle(rndIndexList);

        List<PlayInfoResponse> response = new ArrayList<>();
        int randomLen = 5;
        if (playInfoDtoList.size() < 5) {
            randomLen = playInfoDtoList.size();
        }
        for (int i = 0; i < randomLen; i++) {
            PlayInfoDto playInfoDto = playInfoDtoList.get(rndIndexList.get(i));
            PlayInfoResponse playInfoResponse = new PlayInfoResponse() {
                {
                    setPlayInfoName(playInfoDto.getPlayInfoName());
                    setPlayInfoSiteUrl(playInfoDto.getPlayInfoSiteUrl());
                    setPlayInfoOgpUrl(playInfoDto.getPlayInfoOgpUrl());
                    setPlayInfoSnsUrl(playInfoDto.getPlayInfoSnsUrl());
                    setPlayInfoWeather(playInfoDto.getPlayInfoWeather());
                    setPlayInfoPlace(playInfoDto.getPlayInfoPlace());
                    setPlayInfoGenre(playInfoDto.getPlayInfoGenre());
                    setPlayInfoDescription(playInfoDto.getPlayInfoDescription());
                    setPlayInfoTag(playInfoDto.getPlayInfoTag());
                }
            };
            response.add(playInfoResponse);
        }

        return response;
    }

    /**
     * セレクトボックス選択値取得コントローラー
     * 
     * @return
     */
    private ConditionsDataResponse setConditionsData() {

        ConditionsDataResponse response = new ConditionsDataResponse() {
            {
                setGenreList(genreService.getGenreAll());
                setPlaceList(placeService.getPlaceAll());
                setWeatherList(weatherService.getWeatherAll());
            }
        };
        return response;
    }
}
