package com.dev.play.service.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.play.mapper.GenreMapper;
import com.dev.play.model.db.GenreDto;

@Service
public class GenreService {
    @Autowired
    GenreMapper mapper;

    /**
     * あそび情報全取得
     * 
     * @return 遊び情報リスト
     */
    public List<GenreDto> getGenreAll() {
        return mapper.selectGenreAll();
    }

    /**
     * ジャンル登録
     * 
     * @param genre 登録するジャンル
     */
    public void insertGenre(GenreDto genre) {
        mapper.insertGenre(genre);
    }

    /**
     * ジャンルID取得
     * 
     * @return
     */
    public Integer selectGenreId() {
        return mapper.selectGenreId();
    }
}
