package com.dev.play.service.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.play.mapper.PlayInfoMapper;
import com.dev.play.model.db.GenreDto;
import com.dev.play.model.db.PlayInfoDto;
import com.dev.play.model.db.PlayInfoForPlaceCountDto;
import com.dev.play.model.request.PlaceCountRequest;
import com.dev.play.model.request.PlayInfoDeleteRequest;
import com.dev.play.model.request.PlayInfoInsertRequest;
import com.dev.play.model.request.PlayInfoSearchRequest;
import com.dev.play.model.request.PlayInfoUpdateRequest;

/**
 * やること情報テーブルサービスクラス
 * 
 * @author nakai.k
 *
 */
@Service
public class PlayInfoService {

    @Autowired
    PlayInfoMapper mapper;

    /**
     * あそび情報検索
     * 
     * @return 遊び情報リスト
     */
    public List<PlayInfoDto> getPlayInfo(PlayInfoSearchRequest request) {
        return mapper.selectPlayInfo(request);
    }

    /**
     * 遊び情報全取得
     * 
     * @return あそび情報リスト
     */
    public List<PlayInfoDto> getPlayInfoAll() {
        return mapper.selectPlayInfoAll();
    }

    /**
     * あそび情報削除
     * 
     * @param playInfoDeleteRequest 削除対象あそび情報
     */
    public void deletePlayInfo(PlayInfoDeleteRequest playInfoDeleteRequest) {
        mapper.deletePlayInfo(playInfoDeleteRequest);
    }

    /**
     * あそび情報追加
     * 
     * @param playInfoInsertRequest 追加対象あそび情報
     */
    public void addPlayInfo(PlayInfoInsertRequest playInfoInsertRequest) {
        mapper.insertPlayInfo(playInfoInsertRequest);
    }

    /**
     * あそび情報更新
     * 
     * @param playInfoUpdateRequest 更新対応あそび情報
     */
    public void updatePlayInfo(PlayInfoUpdateRequest playInfoUpdateRequest) {
        mapper.updatePlayInfo(playInfoUpdateRequest);
    }

    /**
     * あそび情報テーブルシーケンス取得
     * 
     * @return
     */
    public Integer getPlayInfoId() {
        return mapper.selectPlayInfoId();
    }

    /**
     * あそび情報の場所種類を取得
     * 
     * @return
     */
    public List<PlayInfoForPlaceCountDto> getPlayInfoPlaceCount(PlaceCountRequest request) {
        return mapper.selectPlayInfoWithPlace(request);
    }

    /**
     * あそび情報のジャンルを取得
     * 
     * @return
     */
    public List<GenreDto> getPlayInfoForGenre() {
        return mapper.selectPlayInfoWithGenre();
    }
}
