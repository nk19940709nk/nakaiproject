package com.dev.play.service.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.play.mapper.WeatherMapper;
import com.dev.play.model.db.WeatherDto;

/**
 * 天気情報サービス
 * 
 * @author nakai.k
 *
 */
@Service
public class WeatherService {

    @Autowired
    WeatherMapper mapper;

    /**
     * 天気情報全取得
     * 
     * @return
     */
    public List<WeatherDto> getWeatherAll() {
        return mapper.selectWeatherAll();
    }

}
