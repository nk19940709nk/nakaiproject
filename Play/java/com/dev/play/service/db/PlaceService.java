package com.dev.play.service.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.play.mapper.PlaceMapper;
import com.dev.play.model.db.PlaceDto;

/**
 * 場所情報サービス
 * 
 * @author nakai.k
 *
 */
@Service
public class PlaceService {

    @Autowired
    PlaceMapper mapper;

    /**
     * 場所情報全取得
     * 
     * @return
     */
    public List<PlaceDto> getPlaceAll() {
        return mapper.selectPlaceAll();
    }

}
