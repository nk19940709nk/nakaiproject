package com.dev.play.service.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.play.mapper.TopicInfoMapper;
import com.dev.play.model.db.TopicInfoDto;
import com.dev.play.model.request.TopicInfoInsertRequest;

/**
 * トピック情報サービスクラス
 * 
 * @author nakai.k
 *
 */
@Service
public class TopicInfoService {

    @Autowired
    TopicInfoMapper mapper;

    /**
     * トピック情報全取得
     * 
     * @return
     */
    public List<TopicInfoDto> getTopicInfoList() {
        return mapper.selectTopicInfoAll();
    }

    /**
     * トピックテーブルシーケンス番号取得
     * 
     * @return
     */
    public Integer getTopicInfoId() {
        return mapper.selectTopicInfoId();
    }

    /**
     * トピックテーブル全削除
     */
    public void deleteTopicInfo() {
        mapper.deleteTopicInfo();
    }

    /**
     * トピック情報登録
     * 
     * @param topicInfoDtoList 登録情報リスト
     */
    public void insertTopicInfo(TopicInfoInsertRequest topicInfoDtoList) {
        mapper.insertTopicInfo(topicInfoDtoList);
    }

}
