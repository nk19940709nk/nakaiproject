package com.dev.play.service.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dev.play.mapper.UserInfoMapper;
import com.dev.play.model.db.UserInfoDto;
import com.dev.play.model.request.LoginRequest;

@Service
public class UserInfoService {
    @Autowired
    UserInfoMapper mapper;

    public UserInfoDto getUser(LoginRequest request) {
        return mapper.selectUserInfo(request);
    }
}
