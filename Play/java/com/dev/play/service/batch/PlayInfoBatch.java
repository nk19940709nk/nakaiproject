package com.dev.play.service.batch;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.dev.play.model.db.GenreDto;
import com.dev.play.model.db.TopicInfoDto;
import com.dev.play.model.request.TopicInfoInsertRequest;
import com.dev.play.model.util.JsoupOgpDto;
import com.dev.play.service.db.GenreService;
import com.dev.play.service.db.TopicInfoService;
import com.dev.play.service.util.JsoupService;

@Service
public class PlayInfoBatch {

    @Autowired
    JsoupService jsoupUtil;
    @Autowired
    TopicInfoService topicInfoService;
    @Autowired
    GenreService genreService;

    @Value("${target.location}")
    private String locationAry;

    // cronは秒、分、時、日、月、曜日 を指定することが可能。曜日は（0,1,2,3,4,5,6,7が日～日を表す。※日は2つある）
//    @Scheduled(cron = "0 15 * * * *", zone = "Asia/Tokyo")
//    @Scheduled(fixedRate = 1000)
    public void managePlayInfoTable() {
        System.out.println("バッチ実行");

        // トピック情報を削除
        topicInfoService.deleteTopicInfo();
        // 取得対象地名
        String[] locations = locationAry.split(",");
        // ジャンル取得
        List<GenreDto> genreList = genreService.getGenreAll();
        for (int i = 0; i < genreList.size(); i++) {
            if (genreList.get(i).getGenreId() == 99) {
                genreList.remove(i);
            }
        }

        // トピックDtoリスト
        List<TopicInfoDto> topicInfoDtoList = new ArrayList<>();

        for (String location : locations) {
            for (GenreDto genre : genreList) {
                // トピック情報取得
                List<JsoupOgpDto> jsoupOgpDtoList = jsoupUtil.getOgpDataList(location, genre.getGenreName());
                for (JsoupOgpDto jsoupOgpDto : jsoupOgpDtoList) {
                    TopicInfoDto topicInfoDto = new TopicInfoDto() {
                        {
                            setTopicId(topicInfoService.getTopicInfoId());
                            setTopicSiteTitle(jsoupOgpDto.getOgpTitle());
                            setTopicSiteUrl(jsoupOgpDto.getSiteUrl());
                            setTopicOgpUrl(jsoupOgpDto.getOgpImageUrl());
                            setGenreId(genre.getGenreId());
                        }
                    };
                    topicInfoDtoList.add(topicInfoDto);
                }
            }
        }

        TopicInfoInsertRequest opicInfoInsertRequest = new TopicInfoInsertRequest() {
            {
                setTopicInfoDtoList(topicInfoDtoList);
            }
        };
        // トピック情報を追加
        topicInfoService.insertTopicInfo(opicInfoInsertRequest);
    }

}
