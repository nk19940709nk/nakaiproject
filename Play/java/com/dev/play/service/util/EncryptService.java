package com.dev.play.service.util;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.stereotype.Service;

import com.dev.play.util.StringUtil;

/**
 * 暗号・復号処理クラス
 *
 */
@Service
public class EncryptService {

    // アルゴリズム/ブロックモード/パディング方式
    private static final String ALGORITHM = "AES/CBC/PKCS5Padding";
    // 暗号キー文字列
    private static final String ENCRYPT_KEY = "DEMytBzxJaV4VgSg";
    private final SecretKeySpec secretKey = new SecretKeySpec(ENCRYPT_KEY.getBytes(), "AES");
    // 初期ベクトル
    private static final String INIT_VECTOR = "KBHFG46ZlrV7zA95";
    private final IvParameterSpec iv = new IvParameterSpec(INIT_VECTOR.getBytes());

    /**
     * 暗号化処理
     * 
     * @param text 暗号対象
     * @return 暗号処理後文字列
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidAlgorithmParameterException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public String encrypt(String text) throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

        Cipher encrypter = Cipher.getInstance(ALGORITHM);
        // 暗号化初期設定
        encrypter.init(Cipher.ENCRYPT_MODE, secretKey, iv);
        // 暗号化
        byte[] byteToken = null;
        if (!StringUtil.isEmpty(text)) {
            byteToken = encrypter.doFinal(text.getBytes());
            return new String(Base64.getEncoder().encode(byteToken));
        } else {
            return null;
        }

    }

    /**
     * 復号化処理
     * 
     * @param encryptedText 復号対象
     * @return 復号処理後文字列
     * @throws NoSuchAlgorithmException
     * @throws NoSuchPaddingException
     * @throws InvalidAlgorithmParameterException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public String decrypt(String encryptedText) throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {

        Cipher decrypter = Cipher.getInstance(ALGORITHM);
        // 復号化初期設定
        decrypter.init(Cipher.DECRYPT_MODE, secretKey, iv);
        // 復号化
        byte[] byteToken = Base64.getDecoder().decode(encryptedText);

        return new String(decrypter.doFinal(byteToken));
    }
}
