package com.dev.play.service.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.dev.play.model.util.JsoupOgpDto;
import com.dev.play.util.StringUtil;

@Service
public class JsoupService {

    @Value("${google.query.url}")
    private String googleUrl;

    private static final String META_OGP_IMAGE = "meta[property~=og:image]";
    private static final String META_OGP_TITLE = "meta[property~=og:title]";

    private static final String TAG_HREF = "a[href]";
    private static final String TAG_H3 = "<h3";

    private static final String EMPTY_TEXT = "";

    /**
     * OGPデータリストを取得する
     * 
     * @param location 検索地方
     * @param genre    検索ジャンル
     * @return
     */
    public List<JsoupOgpDto> getOgpDataList(String location, String genre) {
        List<JsoupOgpDto> jsoupOgpDtoList = new ArrayList<>();

        String query = location + "+" + genre;
        String request = String.format(googleUrl, query);
        try {
            Document reqDoc = Jsoup.connect(request)
                    .userAgent("Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)").get();
            Elements elements = reqDoc.select(TAG_HREF);
            for (Element element : elements) {
                String html = element.toString();
                try {
                    if (html.contains(TAG_H3)) {
                        Document targetDoc = Jsoup.parse(html);
                        String targetElement = targetDoc.select(TAG_HREF).get(0).attr("href");
                        if (targetElement.startsWith("/url?q=")) {
                            if (targetElement.contains("&") && !targetElement.contains("%")) {
                                String siteUrl = targetElement.replace("/url?q=", "").split("&")[0];
                                JsoupOgpDto jsoupOgpDto = new JsoupOgpDto();
                                String imageUrl = getImageUrl(siteUrl);
                                String siteTitle = getTitle(siteUrl);
                                if (StringUtil.isEmpty(imageUrl) || StringUtil.isEmpty(siteTitle)) {
                                    continue;
                                } else {
                                    jsoupOgpDto.setOgpImageUrl(getImageUrl(siteUrl));
                                    jsoupOgpDto.setOgpTitle(getTitle(siteUrl));
                                }
                                jsoupOgpDto.setSiteUrl(siteUrl);

                                jsoupOgpDtoList.add(jsoupOgpDto);
                            }
                        }
                    }
                } catch (Exception e) {
                    continue;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsoupOgpDtoList;
    }

    /**
     * OGPイメージ情報取得
     * 
     * @param url OGP取得対象URL
     * @return OGPイメージURL
     * @throws IOException
     */
    public String getImageUrl(String url) throws IOException {
        String imageUrl = null;
        Document doc = Jsoup.connect(url).get();
        Elements elements = doc.select(META_OGP_IMAGE);
        if (ObjectUtils.isEmpty(elements)) {
            imageUrl = EMPTY_TEXT;
        } else {
            if (elements.get(0).toString().contains("http")) {
                imageUrl = elements.get(0).attr("content");
            }
        }
        return imageUrl;
    }

    /**
     * OGPタイトル情報取得
     * 
     * @param url OGP取得対象URL
     * @return OGPタイトル
     * @throws IOException
     */
    private String getTitle(String url) throws IOException {
        String siteTitle = null;
        Document doc = Jsoup.connect(url).get();
        Elements elements = doc.select(META_OGP_TITLE);
        if (ObjectUtils.isEmpty(elements)) {
            siteTitle = EMPTY_TEXT;
        } else {
            siteTitle = elements.get(0).attr("content");
        }
        return siteTitle;
    }
}
