package com.dev.play.util;

public class StringUtil {

    /**
     * コンストラクタ<br>
     * インスタンス禁止
     */
    private StringUtil() {
    }

    /**
     * 空文字チェック
     * 
     * @param value
     * @return
     */
    public static boolean isEmpty(String value) {
        if (value == null || value == "") {
            return true;
        } else {
            return false;
        }
    }
}
