package com.dev.play.mapper;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.dev.play.model.db.TopicInfoDto;
import com.dev.play.model.request.TopicInfoInsertRequest;

/**
 * トピック情報マッパーテストクラス
 * 
 * @author nakai.k
 *
 */
@SpringBootTest
public class TopicInfoMapperTest {

    @Autowired
    TopicInfoMapper mapper;
//
//    /**
//     * トピック情報全取得テスト
//     */
//    @Test
//    @Ignore
//    public void testSelectTopicInfoAll() {
//        var topicInfoDtoList = mapper.selectTopicInfoAll();
//        for (var topicInfoDto : topicInfoDtoList) {
//            System.out.println(topicInfoDto);
//        }
//    }
//
//    @Test
//    @Ignore
//    public void testSelectTopicInfoId() {
//        var id = mapper.selectTopicInfoId();
//        System.out.println(id);
//    }
//
//    @Test
//    @Ignore
//    public void testDeleteTopicInfo() {
//        mapper.deleteTopicInfo();
//    }
//
//    @Test
//    @Ignore
//    public void testInsertTopicInfo() {
//        List<TopicInfoDto> topicInfoDtoList = new ArrayList<>();
//        TopicInfoDto topicInfoDto = new TopicInfoDto() {
//            {
//                setTopicId(1);
//                setTopicSiteTitle("2022年 福岡のおすすめ遊び・観光スポットランキングTOP20");
//                setTopicSiteUrl("https://haveagood.holiday/areas/55/spots/attraction");
//                setTopicOgpUrl(
//                        "https://image.hldy-cdn.com/c/w=1200,h=630,g=5,a=2,r=auto,f=webp:auto/holiday_area_images/55/55.jpg?1496726329");
//            }
//        };
//        topicInfoDtoList.add(topicInfoDto);
//
//        TopicInfoInsertRequest opicInfoInsertRequest = new TopicInfoInsertRequest() {
//            {
//                setTopicInfoDtoList(topicInfoDtoList);
//            }
//        };
//        mapper.insertTopicInfo(opicInfoInsertRequest);
//    }

}
