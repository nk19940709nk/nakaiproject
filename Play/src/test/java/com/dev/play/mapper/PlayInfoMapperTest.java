package com.dev.play.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PlayInfoMapperTest {

    @Autowired
    PlayInfoMapper mapper;

//
//    @Test
//    public void testSelectPlayInfoAll() {
//        List<PlayInfoDto> playInfoList = mapper.selectPlayInfoAll();
//
//        for (PlayInfoDto playInfo : playInfoList) {
//            System.out.println(playInfo);
//        }
//    }
//
//    @Test
//    @Ignore
//    public void testInsertPlayInfo() {
//        PlayInfoInsertRequest playInfoInsertRequest = new PlayInfoInsertRequest();
//        playInfoInsertRequest.setPlayInfoId(3);
//        playInfoInsertRequest.setPlayInfoName("テスト名称1");
//        playInfoInsertRequest.setPlayInfoSiteUrl("https://www.travel.co.jp/guide/article/33587/");
//        playInfoInsertRequest.setPlayInfoOgpUrl(
//                "https://img-cdn.guide.travel.co.jp/article/855/33587/2AC51C5E1D024750909F87BD4100FD36_LL.jpg");
//        playInfoInsertRequest.setPlayInfoPeople(1);
//        playInfoInsertRequest.setPlayInfoWeather(1);
//        playInfoInsertRequest.setPlayInfoPlace(1);
//        playInfoInsertRequest.setPlayInfoGenre(3);
//        playInfoInsertRequest.setPlayInfoDescription("テスト説明");
//        playInfoInsertRequest.setPlayInfoTag("testTag");
//
//        mapper.insertPlayInfo(playInfoInsertRequest);
//    }
//
//    @Test
//    @Ignore
//    public void testDeletePlayInfo() {
//        PlayInfoDeleteRequest playInfoDeleteRequest = new PlayInfoDeleteRequest();
//        playInfoDeleteRequest.setPlayInfoId(3);
//        mapper.deletePlayInfo(playInfoDeleteRequest);
//    }
//
//    @Test
//    @Ignore
//    public void testSelectPlayInfoId() {
//        var testId = mapper.selectPlayInfoId();
//        System.out.println(testId);
//    }
//
//    @Test
//    @Ignore
//    public void testUpdatePlayInfo() {
//        PlayInfoUpdateRequest playInfoUpdateRequest = new PlayInfoUpdateRequest();
//        playInfoUpdateRequest.setPlayInfoId(3);
//        playInfoUpdateRequest.setPlayInfoName("テスト名称更新");
//        playInfoUpdateRequest.setPlayInfoSiteUrl("https://www.travel.co.jp/guide/article/33587/");
//        playInfoUpdateRequest.setPlayInfoOgpUrl("https://img-cdn.guide.travel.co.jp/article/855/33587/2AC51C5E1D024750909F87BD4100FD36_LL.jpg");
//        playInfoUpdateRequest.setPlayInfoPeople(2);
//        playInfoUpdateRequest.setPlayInfoPlace(2);
//        playInfoUpdateRequest.setPlayInfoGenre(3);
//        playInfoUpdateRequest.setPlayInfoWeather(2);
//        playInfoUpdateRequest.setPlayInfoDescription("テスト説明22");
//        playInfoUpdateRequest.setPlayInfoTag("テストタグ！");
//        
//        mapper.updatePlayInfo(playInfoUpdateRequest);
//    }

}
