package com.dev.play.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.dev.play.service.batch.PlayInfoBatch;
import com.dev.play.service.util.EncryptService;

@SpringBootTest
public class GenreMapperTest {
    @Autowired
    GenreMapper mapper;
    @Autowired
    PlayInfoBatch playInfoBatch;
    @Autowired
    EncryptService EncryptService;

    /**
     * トピック情報全取得テスト
     */
//    @Test
//    @Ignore
//    public void testSelectGenreAll() {
//        var genreDtoList = mapper.selectGenreAll();
//        for (var genreDto : genreDtoList) {
//            System.out.println(genreDto);
//        }
//    }
//
//    Random rnd = new Random();
//
//    @Test
//    @Ignore
//    public void test() {
////        playInfoBatch.managePlayInfoTable();
//        List<Integer> rndIndexList = new ArrayList<>();
//        for (int i = 0; i < 5; i++) {
//            rndIndexList.add(rnd.nextInt(5));
//        }
//        System.out.println(rndIndexList);
//    }
//    $2a$10$st9JosEqcQdTl7YYfYJxrughdqrMT6jxWdPokOCxB37exPnJIDicG
//    $2a$10$pU5j9bM7vW3L.4w2eILGt.JodSZvXKP8i28QL8pv7CizU4Lm2XEiu
}
