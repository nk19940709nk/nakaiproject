package com.dev.play.service.batch;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.dev.play.service.util.JsoupService;

@SpringBootTest
public class PlayInfoBatchTest {

    @Autowired
    PlayInfoBatch playInfoBatch;
    @Autowired
    JsoupService JsoupService;
//    @Test
//    @Ignore
//    public void batchTest() {
//        playInfoBatch.managePlayInfoTable();
//    }

    @Test
    public void test() throws IOException {
        String imageUrl = JsoupService.getImageUrl("https://www.instagram.com/p/Cgg7uT6JS-u/?utm_source=ig_web_copy_link");
        System.out.println(imageUrl);
    }

}
