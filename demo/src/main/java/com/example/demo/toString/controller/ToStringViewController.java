package com.example.demo.toString.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.toString.model.ToStringTest;

@RestController
public class ToStringViewController {

    @GetMapping("/toString")
    public String view() {
        ToStringTest test = new ToStringTest("testName", "testAge","testAddress");
        System.out.println("################### start test view ###################");
        System.out.println(test);
        return "nakaitest";
    }
}