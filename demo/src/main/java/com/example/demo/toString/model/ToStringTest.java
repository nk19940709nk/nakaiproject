package com.example.demo.toString.model;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@ToString(exclude = { "name", "address" })
@Data
@RequiredArgsConstructor
public class ToStringTest {
    private final String name;
    private final String age;
    private final String address;
}
