package com.example.demo.logger.appender;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.spi.ILoggingEvent;

public class MaskLayout extends PatternLayout {

    @Override
    public String doLayout(ILoggingEvent event) {
        return maskPersonalInfo(super.doLayout(event));
    }

    private String maskPersonalInfo(String message) {
        // StringBuffer sb = new StringBuffer();

        // Pattern regex = Pattern.compile("(password=)(.*)");
        // Matcher matcher = regex.matcher(message);

        // if (matcher.find()) {
        // matcher.appendReplacement(sb, matcher.group(1) + "*****");
        // }
        // matcher.appendTail(sb);
        // return sb.toString();

        return message.replaceAll("(?i)(password=)(\\S+)", "$1****");
    }
}
