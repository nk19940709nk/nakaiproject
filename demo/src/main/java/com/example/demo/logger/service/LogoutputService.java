package com.example.demo.logger.service;

import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class LogoutputService {
    public void outputLog() {
        log.error("debug message. password=ttttttt");
        log.error("info message. password=xxxxxxx");
        log.error("warn message. password=xxxxxxx");
        log.error("error message");
        log.error("password=nakai");
    }
}
