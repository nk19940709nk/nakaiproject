package com.example.demo.logger.controller;

import org.slf4j.MDC;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.logger.service.LogoutputService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class LoggerViewController {

	private final LogoutputService logoutputService;

	@GetMapping("/logtest")
	public void loggerView() {
		// Mapped Diagnostic Contexts
		MDC.put("requestId", "12345");
		MDC.put("action", "this is test message");
		logoutputService.outputLog();
		MDC.clear();
	}
}
