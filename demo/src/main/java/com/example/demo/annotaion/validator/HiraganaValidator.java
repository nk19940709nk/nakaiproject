package com.example.demo.annotaion.validator;

import com.example.demo.annotaion.customAnnotation.Hiragana;

import io.micrometer.common.util.StringUtils;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class HiraganaValidator implements ConstraintValidator<Hiragana, String> {
    @Override
    public void initialize(Hiragana annotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (StringUtils.isEmpty(value)) {
            return true;
        }
        String hiraganaRegex = "^[ぁ-ゞ]+$";

        return value.matches(hiraganaRegex);
    }
}
