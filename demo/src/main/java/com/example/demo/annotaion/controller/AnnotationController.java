package com.example.demo.annotaion.controller;

import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.annotaion.customAnnotation.ClassAnnotation;
import com.example.demo.annotaion.model.AnnotationModel;
import com.example.demo.annotaion.service.MethodAnnotationService;

import lombok.RequiredArgsConstructor;

@RestController
@ClassAnnotation(version = "2.0")
@RequiredArgsConstructor
public class AnnotationController {

    private final MethodAnnotationService methodAnnotationService;

    /**
     * curl --location 'http://localhost:8080/fieldAnnotation' \
     * --header 'Content-Type: application/json' \
     * --data '{
     * "name":"ひら",
     * "data":"test data"
     * }'
     * 
     * @param model
     * @param result
     * @return
     */
    @PostMapping("/fieldAnnotation")
    public AnnotationModel fieldTest(@RequestBody @Validated AnnotationModel model, BindingResult result) {
        System.out.println(result);
        if (result.hasErrors()) {
            System.out.println("ERROR検知:" + result);
        }
        return model;
    }

    @GetMapping("/methodAnnotation")
    public void methodAnnotation() {
        methodAnnotationService.print();
    }

    @GetMapping("/classAnnotation")
    public void classAnnotation() {
        System.out.println("Called class annotation");
    }
}
