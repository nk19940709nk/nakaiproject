package com.example.demo.annotaion.annotationAop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import com.example.demo.annotaion.customAnnotation.ClassAnnotation;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Aspect
@Component
@RequiredArgsConstructor
@Slf4j
public class ClassAnnotationAop {

    @Before("com.example.demo.annotaion.annotationAop.pointcut.Pointcuts.controllerPointcut()")
    public void before(JoinPoint joinPoint) {
        log.info("++++++++++++++++++++ Before class annotation: " + joinPoint.getSignature().getName());

        String signature = joinPoint.getSignature().toString();
        log.info("Signature : " + signature);

        Class<?> classObject = joinPoint.getTarget().getClass();
        ClassAnnotation classAnnotation = classObject.getAnnotation(ClassAnnotation.class);
        if (classAnnotation != null) {
            log.info("class version : " + classAnnotation.version());
        }
    }

    @After("com.example.demo.annotaion.annotationAop.pointcut.Pointcuts.controllerPointcut()")
    public void after(JoinPoint joinPoint) {
        log.info("++++++++++++++++++++ After class annotation: " + joinPoint.getSignature().getName());
    }
}
