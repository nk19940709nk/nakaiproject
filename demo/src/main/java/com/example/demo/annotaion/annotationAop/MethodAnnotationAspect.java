package com.example.demo.annotaion.annotationAop;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import com.example.demo.annotaion.customAnnotation.MethodAnnotation;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Aspect
@Component
@RequiredArgsConstructor
@Slf4j
public class MethodAnnotationAspect {

    @Before("@annotation(com.example.demo.annotaion.customAnnotation.MethodAnnotation)")
    public void before(JoinPoint joinPoint) {
        log.info("++++++++++++++++++++ Before method annotation: " + joinPoint.getSignature().getName());

        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        log.info("Signature : " + methodSignature.toString());
        log.info("Method name : " + method.getName());
    }

    @After("@annotation(com.example.demo.annotaion.customAnnotation.MethodAnnotation)")
    public void afterMethodExecution(JoinPoint joinPoint) {
        log.info("++++++++++++++++++++ After method annotation:" + joinPoint.getSignature().getName());
    }
}
