package com.example.demo.annotaion.annotationAop;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import com.example.demo.annotaion.customAnnotation.FieldAnnotation;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Aspect
@Component
@RequiredArgsConstructor
@Slf4j
public class FieldAnnotationAop {
    @Before("com.example.demo.annotaion.annotationAop.pointcut.Pointcuts.controllerPointcut()")
    public void before(JoinPoint joinPoint) {
        final MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        final Method method = signature.getMethod();
        final String className = method.getDeclaringClass().getName();
        final String methodName = method.getName();

        try {
            final Object[] args = joinPoint.getArgs();
            Map<String, String> keyValues = new HashMap<>();
            for (Object obj : args) {
                for (Field field : obj.getClass().getDeclaredFields()) {
                    if (field.isAnnotationPresent(FieldAnnotation.class)) {
                        field.setAccessible(true);
                        String name = field.getName();
                        keyValues.put(name, obj.toString());
                    }
                }
            }
            if (keyValues.isEmpty()) {
                log.info("++++++++++++++++ Class : {}, Method : {}", className, methodName);
            } else {
                log.info("++++++++++++++++ Class : {}, Method : {}, Value : {}", className, methodName, keyValues);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @After("com.example.demo.annotaion.annotationAop.pointcut.Pointcuts.controllerPointcut()")
    public void after(JoinPoint joinPoint) {

    }
}
