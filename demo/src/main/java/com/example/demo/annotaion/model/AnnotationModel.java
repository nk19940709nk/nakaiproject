package com.example.demo.annotaion.model;

import com.example.demo.annotaion.customAnnotation.FieldAnnotation;
import com.example.demo.annotaion.customAnnotation.Hiragana;

import lombok.Data;

@Data
public class AnnotationModel {

	@Hiragana
	private String name;

	@FieldAnnotation
	private String data;

}
