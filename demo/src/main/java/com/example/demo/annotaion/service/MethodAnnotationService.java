package com.example.demo.annotaion.service;

import org.springframework.stereotype.Service;

import com.example.demo.annotaion.customAnnotation.MethodAnnotation;

@Service
public class MethodAnnotationService {
    @MethodAnnotation
    public void print() {
        System.out.println("this is method annotation test.");
    }
}
