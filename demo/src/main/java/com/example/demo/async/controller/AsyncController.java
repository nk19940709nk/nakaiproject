package com.example.demo.async.controller;

import java.util.concurrent.CompletableFuture;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.async.service.AsyncService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@Slf4j
public class AsyncController {

    private final AsyncService asyncService;

    @GetMapping("/async")
    public void async() {
        try {
            long startTime = System.currentTimeMillis();
            CompletableFuture<String> future1 = asyncService.asyncMethod("3");
            CompletableFuture<String> future2 = asyncService.asyncMethod("2");
            CompletableFuture<String> future3 = asyncService.asyncMethod("1");

            CompletableFuture.allOf(future1, future2, future3).join();

            System.out.println("--> " + future1.get());
            System.out.println("--> " + future2.get());
            System.out.println("--> " + future3.get());
            long endTime = System.currentTimeMillis();
            System.out.println("処理時間：" + (endTime - startTime) + " ms");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
