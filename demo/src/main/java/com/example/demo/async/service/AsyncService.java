package com.example.demo.async.service;

import java.util.concurrent.CompletableFuture;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AsyncService {

    @Async
    public CompletableFuture<String> asyncMethod(String seconds) throws InterruptedException {
        Thread.sleep(Long.valueOf(seconds) * 1000L);
        return CompletableFuture.completedFuture(seconds + "秒かかる処理");
    }
}
