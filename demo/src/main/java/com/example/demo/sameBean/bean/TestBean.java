package com.example.demo.sameBean.bean;

public class TestBean {
    private String beanName;

    public TestBean(String beanName) {
        this.beanName = beanName;
    }

    public void test() {
        System.out.println("bean test : " + beanName);
    }
}
