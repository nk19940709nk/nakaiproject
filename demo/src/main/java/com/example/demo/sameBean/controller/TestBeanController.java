package com.example.demo.sameBean.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.sameBean.bean.TestBean;

@RestController
public class TestBeanController {

    @Autowired
    @Qualifier("testBean1")
    private TestBean testBean1;

    @Autowired
    @Qualifier("testBean2")
    private TestBean testBean2;

    @GetMapping("/testbean")
    public void testController() {
        System.out.println("### testBean1");
        testBean1.test();
        System.out.println("### testBean2");
        testBean2.test();
    }
}
