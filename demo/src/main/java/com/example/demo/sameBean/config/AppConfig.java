package com.example.demo.sameBean.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.demo.sameBean.bean.TestBean;

@Configuration
public class AppConfig {

    @Bean
    public TestBean testBean2() {
        return new TestBean("testbean2");
    }

    @Bean
    public TestBean testBean1() {
        return new TestBean("testbean1");
    }

}
