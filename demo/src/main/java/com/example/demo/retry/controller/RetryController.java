package com.example.demo.retry.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.retry.service.RetrySerice;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class RetryController {

    private final RetrySerice retrySerice;

    @GetMapping("/retry")
    public void retry() {
        retrySerice.print();
    }
}
