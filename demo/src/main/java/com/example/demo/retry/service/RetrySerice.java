package com.example.demo.retry.service;

import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RetrySerice {

    @Retryable(value = { RuntimeException.class })
    public void print() {
        System.out.println("called print method");
        try {
            throw new RuntimeException("throw runtime Exception!");
        } catch (RuntimeException e) {
            log.error("retry it");
            throw e;
        }
    }

    @Recover
    public void recover(RuntimeException e) {
        log.info("The number of retries has reached the limit.");
    }

}
