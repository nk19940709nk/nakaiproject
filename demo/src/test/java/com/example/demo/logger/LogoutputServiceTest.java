package com.example.demo.logger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;

import com.example.demo.logger.service.LogoutputService;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.Appender;

@SpringBootTest
@SpringJUnitWebConfig
public class LogoutputServiceTest {
    @Mock
    private Appender<ILoggingEvent> mockAppender;

    @InjectMocks
    @Autowired
    LogoutputService logoutputService;

    @Test
    public void appenderMockTest() {
        Logger logger = (Logger) LoggerFactory.getLogger(LogoutputService.class);
        logger.addAppender(mockAppender);

        logoutputService.outputLog();

        ArgumentCaptor<LoggingEvent> captor = ArgumentCaptor.forClass(LoggingEvent.class);
        verify(mockAppender, times(5)).doAppend(captor.capture());
        assertEquals("ERROR", captor.getValue().getLevel().toString());

        captor.getAllValues().forEach(value -> {
            System.out.println("Level : " + value.getLevel());
            System.out.println("Message : " + value.getMessage());
        });
    }
}
