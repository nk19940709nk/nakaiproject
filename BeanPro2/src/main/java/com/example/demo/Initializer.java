package com.example.demo;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.core.Ordered;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MapPropertySource;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.stereotype.Component;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class Initializer implements BeanPostProcessor, Ordered {
	private final ApplicationContext applicationContext;
	private final Config config;

	@PostConstruct
	public void initialize() {
		Map<String, Object> pathap = new HashMap<>();
		// プロパティファイルから取得したデータに手を加える（データの復号化やでコードなど）
		pathap.put("testkey", "encoded:" + config.getMessage());
		ConfigurableEnvironment configurableEnvironment = (ConfigurableEnvironment) applicationContext.getEnvironment();
		MutablePropertySources propertySources = configurableEnvironment.getPropertySources();
		propertySources.addLast(new MapPropertySource("testdata", pathap));

	}

	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) {
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) {
		return bean;
	}

	@Override
	public int getOrder() {
		return Ordered.HIGHEST_PRECEDENCE;
	}

}
