package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeanPro2Application {

	public static void main(String[] args) {
		SpringApplication.run(BeanPro2Application.class, args);
	}

}
