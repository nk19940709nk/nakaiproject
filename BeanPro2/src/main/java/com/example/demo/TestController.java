package com.example.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {

	@Value("${encryptdata}")
	private String encryptdata;

	@GetMapping(value = "/")
	@ResponseBody
	public void test() {
		System.out.println(encryptdata);
	}

}
