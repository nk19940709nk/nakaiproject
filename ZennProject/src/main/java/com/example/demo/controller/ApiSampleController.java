package com.example.demo.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Logger;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class ApiSampleController {
    @PostMapping(value = "/test")
    public void test(@RequestParam String obj) {
        System.out.println(obj);
    }

    @RequestMapping(value = "/logger", method = RequestMethod.POST)
    public void loggerPost(
            // リクエストボディに渡されたJSONを引数にマッピングする
            @RequestBody Logger logger) {

        // listをloggerテーブルにINSERTする⇒INSERTしたデータのリストが返ってくる
        // JSON文字列に変換されたレスポンスが送られる
        System.out.println(logger.getIdtoken());
        System.out.println(logger.getMenuid());
        System.out.println(logger.getId());
    }
}
