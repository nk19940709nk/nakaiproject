package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.util.Utils;

@Service
public class MockitoSample {

    @Autowired
    Utils utils;

    public void testMethod(String value) {
        // メソッド実行1回目
        System.out.println(utils.checkLenngh(value));
    }

    public int getData() {

        return 1;
    }
}
