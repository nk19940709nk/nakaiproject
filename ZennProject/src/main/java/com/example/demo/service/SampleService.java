package com.example.demo.service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.example.demo.model.LineObjMessageDetails;
import com.example.demo.model.LineObjMessages;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SampleService {
    

    /**
     * LINEブロードキャストメッセージを配信する
     * @param messasge メッセージ
     * @return メッセージ送信OK：true メッセージ送信NG:false
     * @throws IllegalArgumentException
     * @throws IOException
     */
    public Boolean sendLineBroadcastMessage() throws IllegalArgumentException, IOException {

        // LineApiKeyを取得
        String lineApiKey = "LINEで取得したAPIキー";
        
        try {
            // リクエスト先URLを設定
            HttpPost httpPost = new HttpPost("https://api.line.me/v2/bot/message/broadcast");
            // ヘッダー設定
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Authorization", "Bearer " + lineApiKey);
            
            String jsonStr = "{\"messages\":[{\"type\":\"flex\",\"altText\":\"通知に表示されるテキスト\",\"contents\":"
                    + "{\"type\":\"bubble\",\"body\":{\"type\":\"box\",\"layout\":\"horizontal\",\"contents\":"
                    + "[{\"type\":\"text\",\"text\":\"Hello,\"},{\"type\":\"text\",\"text\":\"%s\"}]}}}]}";
            String json = String.format(jsonStr, "テストメッセージ");

            // body設定
            StringEntity params = new StringEntity(json, StandardCharsets.UTF_8);
            httpPost.setEntity(params);
            
            // リクエスト実行
            CloseableHttpClient client = HttpClients.createDefault();
            CloseableHttpResponse resp = client.execute(httpPost);
            int statusCode = resp.getStatusLine().getStatusCode();
            
            if(statusCode == 200) {
                return true;
            } else {
                return null;
            }
        } catch(IllegalArgumentException | IOException e) {
            throw e;
        }
    }
    

    /**
     * LINEメッセージを送りたい時に使用する 
     * @param messasge メッセージ
     * @return メッセージ送信OK：true メッセージ送信NG:false
     * @throws IllegalArgumentException
     * @throws IOException
     */
    public Boolean sendLineMessages(String message) throws IllegalArgumentException, IOException {

        // LineApiKeyを取得
        String lineApiKey = "LINEで取得したAPIキー";
        ObjectMapper objMapper = new ObjectMapper();
        
        try {
            
            List<LineObjMessageDetails> lineObjMessageDetailsList = new ArrayList<>();
            
            // メッセージオブジェクト生成
            LineObjMessageDetails lineObjMessageDetails = new LineObjMessageDetails();
            lineObjMessageDetails.setType("text");
            lineObjMessageDetails.setText(message);
            
            // メッセージオブジェクトリストを生成
            lineObjMessageDetailsList.add(lineObjMessageDetails);
            LineObjMessages lineObjMessages = new LineObjMessages();
            lineObjMessages.setMessages(lineObjMessageDetailsList);
            
            // オブジェクト→Json変換
            String lineObjMessagesJson = objMapper.writeValueAsString(lineObjMessages);

            // リクエスト先URLを設定
            HttpPost httpPost = new HttpPost("https://api.line.me/v2/bot/message/broadcast");
            
            // ヘッダー設定
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Authorization", "Bearer " + lineApiKey);

            // body設定
            StringEntity params = new StringEntity(lineObjMessagesJson, StandardCharsets.UTF_8);
            httpPost.setEntity(params);
            
            // リクエスト実行
            CloseableHttpClient client = HttpClients.createDefault();
            CloseableHttpResponse resp = client.execute(httpPost);
            int statusCode = resp.getStatusLine().getStatusCode();
            
            if(statusCode == 200) {
                return true;
            } else {
                return null;
            }
        } catch(IllegalArgumentException | IOException e) {
            throw e;
        }
    }
}
