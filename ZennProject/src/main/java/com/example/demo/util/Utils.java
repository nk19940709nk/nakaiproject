package com.example.demo.util;

import java.io.File;
import org.springframework.stereotype.Service;

@Service
public class Utils {

    /**
     * ディレクトリの存在をチェックしてなければ作成する。
     * 
     * @param path ディレクトリパス
     * @return 存在有無 true：存在している false:存在していない
     */
    public boolean checkDir(String path) {
        File stopDir = new File(path);
        // ディレクトリ存在チェック
        if (!stopDir.exists()) {
            stopDir.mkdirs();
            return false;
        } else {
            return true;
        }
    }

    /**
     * テキストの文字数を数える処理
     * 
     * @param msg チェック対象文字列
     * @return 文字数
     */
    public int checkLenngh(String msg) {
        return msg.length();
    }
}
