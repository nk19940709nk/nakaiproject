package com.example.demo.model;

import java.util.List;

import lombok.Data;

@Data
public class LineObjMessages {
    /** メッセージリスト */
    private List<LineObjMessageDetails> messages;

}
