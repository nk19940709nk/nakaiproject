package com.example.demo.model;

import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class LineObjMessageDetails {
    
    /** メッセージ種別 */
    private String type;
    /** メッセージ */
    private String text;
    
    public String mockTest() {
        return "mmmmm";
    }

}
