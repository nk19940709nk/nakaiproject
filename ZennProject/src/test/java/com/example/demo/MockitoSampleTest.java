package com.example.demo;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.demo.service.MockitoSample;
import com.example.demo.util.Utils;

@ExtendWith(MockitoExtension.class)
public class MockitoSampleTest {

    @Mock(lenient = true)
    private Utils utils;

    @InjectMocks
    MockitoSample mockitoSample;

    /**
     * whenの確認テスト
     */
    @Test
    public void whenTest() {
        // モックインスタンスの戻り値を設定
        doReturn(5).when(utils).checkLenngh("t");
        doReturn(6).when(utils).checkLenngh("te");
        doReturn(7).when(utils).checkLenngh("tes");
        // 以下方法でも設定可能
        when(utils.checkLenngh("test")).thenReturn(8);

        // テスト対象メソッド実行
        mockitoSample.testMethod("test");

        // ========コンソール出力内容========
        // 5
        // 6
        // 7
        // 8
    }

    /**
     * verifyの確認テスト
     */
    @Test
    public void verifyTest() {
        // テスト対象メソッド実行
        mockitoSample.testMethod("");

        // モックインスタンスの呼び出し回数を検証
        verify(utils, times(1)).checkLenngh("t");
        verify(utils, times(1)).checkLenngh("te");
        verify(utils, times(1)).checkLenngh("tes");
        verify(utils, times(2)).checkLenngh("test");

        // ========JUnitエラー内容========
        // utils.checkLenngh("test");
    }

    /**
     * argumentCaptorの確認テスト
     */
    @Test
    public void argumentCaptorTest() {
        // テスト対象メソッド実行
        mockitoSample.testMethod("");

        // キャプチャー対象の引数を設定
        ArgumentCaptor<String> strCaptor = ArgumentCaptor.forClass(String.class);

        // モックインスタンスのメソッドを実行
        verify(utils, times(4)).checkLenngh(strCaptor.capture());
        // キャプチャー内容を出力
        System.out.println(strCaptor.getAllValues());

        // ========コンソール出力内容========
        // 0
        // 0
        // 0
        // 0
        // [t, te, tes, test]
    }

    @ParameterizedTest
    @CsvSource(delimiterString = "|", value = { "test1 | user1", "test2 | user2" })
    public void getDataTest(String arg1, String arg2) {

        // ============ パラメータ設定 ============
        String value = arg1;
        String user = arg2;

        // ============ 偽装 ============
        when(utils.checkLenngh(value)).thenReturn(8);
        when(utils.checkLenngh(user)).thenReturn(0);

        // ============ 検証 ============
        mockitoSample.testMethod(value);
        mockitoSample.testMethod(user);
    }
}
