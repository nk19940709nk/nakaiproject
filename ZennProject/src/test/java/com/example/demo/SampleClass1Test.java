package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.demo.util.SampleClass1;


//@SpringJUnitConfig
//@SpringBootTest
//@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class })
@ExtendWith(MockitoExtension.class)
public class SampleClass1Test {

    @Test
    public void callProtecTest() {
        SampleClass1 sampleClass1 = new SampleClass1();
        Method method;
        try {
            method = SampleClass1.class.getDeclaredMethod("callProtec", String.class);
            method.setAccessible(true);
            boolean flg = (boolean)method.invoke(sampleClass1, "testStr!");
            assertEquals(flg,true);
        } catch (NoSuchMethodException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (SecurityException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch(InvocationTargetException e) {
            
        } catch(IllegalAccessException e) {
            
        }
    }
    
    @Test
    public void callPriTest() {
        SampleClass1 sampleClass1 = new SampleClass1();
        Method method;
        try {
            method = SampleClass1.class.getDeclaredMethod("callPri", String.class);
            method.setAccessible(true);
            boolean flg = (boolean)method.invoke(sampleClass1, "testStr2!");
            assertEquals(flg,true);
        } catch (NoSuchMethodException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (SecurityException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch(InvocationTargetException e) {
            
        } catch(IllegalAccessException e) {
            
        }
    }
    
    @Test
    public void test() {
        List<String> testList = new ArrayList<>();
        testList.add("a");
        testList.add("b");
        testList.add("c");
        testList.add("d");
        testList.stream().forEach(a -> {
            if(a.equals("b")) {
                System.out.println("testseikou!");
            }
        });
    }
}
