package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.example.demo.util.Utils;

@SpringJUnitConfig
@SpringBootTest
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class })
public class UtilsTest {

	@Autowired
	private Utils utils;
	
	@Test
	public void createDirTest() {
		utils.checkDir("/test");
	}

}
